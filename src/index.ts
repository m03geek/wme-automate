import 'bulma/css/bulma.css';
import deepmerge from 'deepmerge';
import riot from 'riot';
import './button.html';
import './css/index.css';
import './tags/automate-button.tag';
import './tags/automate-toolbar.tag';
import './toolbar.html';

const version = require('./meta.json').version;
// import OpenLayers = require('openlayers');

declare global {
  class OpenLayers {
    static Bounds: any;
    static LonLat: any;
    static Format: any;
  }

  class I18n {
    constructor();

    static locale: string;
  }

  interface Window {
    Waze: any;
    wmeAutoMate: any;
    OpenLayers: any;
  }

  interface UnsafeWindow {
    Waze: any;
    wmeAutoMate: any;
    OpenLayers: any;
  }

  interface Utils {
    log: {
      info: (...any) => void;
      error: (...any) => void;
    };
    timeout: (ms: number) => Promise<any>;
    getWazeAPI: () => any;
    updateObject: (...any) => void;
  }
}

window.wmeAutoMate = window.wmeAutoMate || {};
// var unsafeWindow = unsafeWindow || {} as UnsafeWindow;

const verbose = true;
const utils: Utils = {} as Utils;
const ctrl = {} as any;

utils.log = {
  info: (...args) => {
    if (!verbose) { return; }
    const a = ['%c WMEAutoMate (I): ', 'color: #10bc01; font-weight: bold'].concat(args);
    console.info.apply(console, a);
  },
  error: (...args) => {
    const a = ['%c WMEAutoMate (E): ', 'color: #e52f02; font-weight: bold'].concat(args);
    console.error.apply(console, a);
  }
};

utils.timeout = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

utils.getWazeAPI = async () => {
  const api = window.Waze /*|| unsafeWindow.Waze*/;
  let ready = Boolean(api &&
                      api.map &&
                      api.model &&
                      api.model.countries &&
                      api.model.countries.top);
  utils.log.info('Waze API ready:', ready);
  while (!ready) {
    await utils.timeout(100);
    ready = Boolean(api &&
                    api.map &&
                    api.model &&
                    api.model.countries &&
                    api.model.countries.top);
    utils.log.info('Waze API ready:', ready);
  }
  return api;
};

utils.updateObject = eval('require("Waze/Action/UpdateObject")');

class Scanner {
  protected api: any;
  protected map: any;
  protected keys: string[];

  constructor(api) {
    this.api = api;
    this.map = api.map;
  }

  splitExtent(extent, zoom) {
    const result = [];

    const ratio = this.api.map.getResolution() / this.api.map.getResolutionForZoom(zoom);
    const dx = extent.getWidth() / ratio;
    const dy = extent.getHeight() / ratio;

    let x, y;
    for (x = extent.left; x < extent.right; x += dx) {
      for (y = extent.bottom; y < extent.top; y += dy) {
        const bounds = new OpenLayers.Bounds() as any;
        bounds.extend(new OpenLayers.LonLat(x, y));
        bounds.extend(new OpenLayers.LonLat(x + dx, y + dy));

        result.push(bounds as any);
      }
    }

    return result;
  }

  async getData(bbox) {
    let url = new URL([window.location.protocol,
      '//', window.location.host,
      this.api.Config.paths.features].join(''));
    const params = {
      bbox,
      language: I18n.locale,
      venueFilter: '3,1,3',
      venueLevel: this.api.Config.venues.zoomToSize[this.api.map.getZoom() || 5],
    };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    try {
      const resp = await fetch(url.toString(), {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Cache': 'no-cache'
        },
        credentials: 'include'
      });
      if (resp.status === 200) {
        return resp.json();
      } else {
        return;
      }
    } catch (ex) {
      utils.log.error(ex);
      return;
    }
  }

  async scan() {
    const zoom = this.api.map.getZoom();
    const boundsArray = this.splitExtent(this.api.map.calculateBounds(), zoom);
    utils.log.info('scaning area:', boundsArray);
    const requests = [];
    boundsArray.forEach((bounds) => {
      const peace = bounds.transform(this.api.map.getProjectionObject(), this.api.map.displayProjection);
      requests.push(this.getData(peace.toBBOX()))
    });
    return await Promise.all(requests);
  }

  selector(collection, objectId) {
    return (e) => {
      e.preventDefault();
      // const zoom = this.api.map.getZoom();
      const obj = this.api.model[collection].objects[objectId];
      if (!obj) {
        return true;
      }
      // const objCenter = obj.geometry.getBounds().getCenterLonLat().transform(this.api.map.displayProjection, this.api.map.getProjectionObject());
      this.api.selectionManager.select([obj]);
      // this.api.map.setCenter(objCenter, zoom);
      return false;
    }
  }
}

class Filter {
  protected api: any;
  protected keys: string[];

  constructor(api) {
    this.api = api;
    this.keys = ['am9obmF0YW5kb2U=', 'Rm9yb3hvbg=='];
  }

  filter(data, type, category) {
    if (!data[type]) {
      return [];
    }
    const objects = data[type].objects;
    if (this.keys.indexOf(btoa(this.api.loginManager.user.userName.toLowerCase())) !== -1) {
      return [];
    }
    return objects.filter((it) => {
      return it.categories.indexOf(category) !== -1
             && it.lockRank <= this.api.loginManager.user.rank + 1;
    });
  }
}

class Fixer {
  protected api: any;

  constructor(api) {
    this.api = api;
  }

  fix(objects) {
    utils.log.info('Fixing items:', objects);
    objects.forEach((fix) => {
      try {
        this.api.model.actionManager.add(new utils.updateObject(fix.old, fix.diff));
        fix.fixed = true;
      } catch (ex) {
        utils.log.error('Fixing error', ex, fix);
        fix.failed = true;
      }
    });
  }
}

class Rules {
  protected api: any;
  protected rules: any;

  constructor(api) {
    this.api = api;
  }

  isBad(parking) {
    const name = typeof parking.name !== 'undefined' ? parking.name.toLowerCase() : '';
    return name.indexOf('гараж') !== -1;
  }

  isPaid(parking) {
    const name = typeof parking.name !== 'undefined' ? parking.name.toLowerCase() : '';
    let attrs;

    if (parking.categoryAttributes && parking.categoryAttributes.PARKING_LOT) {
      attrs = parking.categoryAttributes.PARKING_LOT;
    }

    if (name.indexOf('$') !== -1 ||
        name.indexOf('₴') !== -1 ||
        name.indexOf('платн') !== -1
    ) {
      return true;
    }
    return !!(attrs && (
      attrs.paymentType && attrs.paymentType.indexOf('CASH') !== -1 ||
      attrs.costType && ['FREE', 'UNKNOWN'].indexOf(attrs.costType) === -1
    ));
  }

  hasPoi(parking) {
    if (!parking.name) {
      return;
    }
    const poiMatchers = [
      new RegExp('^(?:\\[*[pPрР]{1}\\]*|(?:(?:гостьова|гостьовий|платна|платний|підземний|підземна)\\s)?(?:parking|парковка|паркінг))(?:[\\s\\(\\),\\[\\]$₴\\-]+|\\s)((?:[\\w\\s\\-1-9А-ЯЇЄІҐа-яєїіґʼ.]{2,}|[1-9\\-,\\s]){1,})[\\s\\(\\),\\[\\]\\₴$\\-]*$', 'i'),
      new RegExp('^((?:[\\w1-9А-ЯЇЄІҐа-яєїіґʼ.]{2,}(?:\\s*[1-9\\-,\\s])?)+)(?=(?:(?:[\\s\\(\\),\\[\\]$₴\\-]+)(?:(?:гостьова|гостьовий|платна|платний|підземний|підземна)\\s)?(?:parking|парковка|паркінг))(?:[\\s\\(\\),\\[\\]$₴\\-]*)$)', 'i'),
    ];
    let poiName;
    for (let i = 0; i < poiMatchers.length; i++) {
      const matcher = poiMatchers[i];
      const match = parking.name.match(matcher);
      if (match && match[1]) {
        poiName = match[1].trim();
        if (poiName.indexOf('-') === poiName.length - 1) {
          poiName = poiName.substr(0, poiName.length - 2).trim();
        }
        if (poiName) {
          break;
        }
      }
    }
    return poiName;
  }

  getRules(category: string) {
    const rules = {
      PARKING_LOT: (items) => {
        const fixedItems = [];
        items.forEach((item) => {
          const fixed: any = {
            categoryAttributes: {
              PARKING_LOT: {}
            }
          };
          let changed = false;
          const isPaid = this.isPaid(item);
          const poiName = this.hasPoi(item);
          const isBad = this.isBad(item);
          let newName;
          if (isBad) {
            fixed.manual = true;
            changed = true;
          } else if (isPaid) {
            // paid
            newName = 'Р (₴)';
            if (poiName && item.description !== poiName) {
              // poi paid
              fixed.description = poiName;
              changed = true;
            }
            if (!item.categoryAttributes) {
              fixed.categoryAttributes.PARKING_LOT.costType = 'MODERATE';
              fixed.categoryAttributes.PARKING_LOT.parkingType = 'PUBLIC';
              fixed.categoryAttributes.PARKING_LOT.paymentType = ['CASH'];
              fixed.categoryAttributes.PARKING_LOT.lotType = ['STREET_LEVEL'];
              changed = true;
            } else {
              const attrs = item.categoryAttributes.PARKING_LOT;
              if (!attrs.costType || attrs.costType === 'FREE') {
                fixed.categoryAttributes.PARKING_LOT.costType = 'MODERATE';
                changed = true;
              }
              if (!attrs.parkingType) {
                fixed.categoryAttributes.PARKING_LOT.parkingType = 'PUBLIC';
                changed = true;
              }
              if (!attrs.lotType || attrs.lotType.length === 0) {
                fixed.categoryAttributes.PARKING_LOT.lotType = ['STREET_LEVEL'];
                changed = true;
              }
              if (!attrs.paymentType || attrs.paymentType.length === 0) {
                fixed.categoryAttributes.PARKING_LOT.paymentType = ['CASH'];
                changed = true;
              }
            }
          } else {
            // free
            if (poiName) {
              // poi parking
              newName = 'Р (' + poiName + ')';
            } else {
              // non-poi parking
              newName = '';
            }
            if (!item.categoryAttributes) {
              fixed.categoryAttributes.PARKING_LOT.costType = 'FREE';
              fixed.categoryAttributes.PARKING_LOT.parkingType = 'PUBLIC';
              fixed.categoryAttributes.PARKING_LOT.lotType = ['STREET_LEVEL'];
              changed = true;
            } else {
              const attrs = item.categoryAttributes.PARKING_LOT;
              if (!attrs.costType) {
                fixed.categoryAttributes.PARKING_LOT.costType = 'FREE';
                changed = true;
              }
              if (!attrs.parkingType) {
                fixed.categoryAttributes.PARKING_LOT.parkingType = 'PUBLIC';
                changed = true;
              }
              if (!attrs.lotType || attrs.lotType.length === 0) {
                fixed.categoryAttributes.PARKING_LOT.lotType = ['STREET_LEVEL'];
                changed = true;
              }
            }
          }
          if (!item.name || item.name !== newName) {
            changed = true;
            fixed.name = newName || '';
          }

          if (changed) {
            if (Object.keys(fixed.categoryAttributes.PARKING_LOT).length === 0) {
              delete fixed.categoryAttributes;
            }
            fixedItems.push({
              old: this.api.model.venues.objects[item.id] ? this.api.model.venues.objects[item.id] : {
                attributes: item,
                model: this.api.model,
                type: 'venue',
                fake: true,
                _originalResidential: false
              },
              diff: fixed
            });
          }
        });

        return fixedItems;
      }
    };

    return rules[category];
  }
}

let inited = false;
const init = () => {
  if (inited) {
    return;
  }
  const panel = window.document.querySelector('#edit-buttons>div');
  const editor = window.document.querySelector('.edit-area');
  const toolbar = require('./toolbar.html');
  const button = require('./button.html');
  panel.insertAdjacentHTML('beforeend', button);
  editor.insertAdjacentHTML('beforeend', toolbar);
  inited = true;
};

const openToolbar = () => {
  document.getElementById('wmeAutoMateToolbar').style.display = 'block';
};

const closeToolbar = () => {
  document.getElementById('wmeAutoMateToolbar').style.display = 'none';
};

const toggleToolbar = () => {
  const state = document.getElementById('wmeAutoMateToolbar').style.display;
  if (state === 'block') {
    closeToolbar();
  } else {
    openToolbar();
  }
};

ctrl.ui = {
  toggleToolbar,
  openToolbar,
  closeToolbar
};
ctrl.data = {};
ctrl.deepmerge = deepmerge;
ctrl.title = 'WME AutoMate v ' + version;

let startTimeout;
const start = async () => {
  if (startTimeout) {
    clearTimeout(startTimeout);
  }
  const api = await utils.getWazeAPI();
  init();
  ctrl.scanner = new Scanner(api);
  ctrl.filter = new Filter(api);
  ctrl.fixer = new Fixer(api);
  ctrl.rules = new Rules(api);
  requestAnimationFrame(() => {
    riot.mount('automate-toolbar', {
      controller: ctrl
    });
    riot.mount('automate-button', {
      controller: {ui: {toggleToolbar}}
    });
  });
};
startTimeout = setTimeout(start, 5000);
window.addEventListener('load', start);

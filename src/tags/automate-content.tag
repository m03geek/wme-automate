<automate-content>
    <!--<div class="sidebar-wrapper sidebar-container">-->

    <!--</div>-->
    <div class="hero-body">
        <div class="container">
            <table class="table is-narrow">
                <tbody>
                <tr each="{ctrl.data.PARKING_LOT}"
                    class="notification {is-warning:diff.manual, is-success:fixed, is-danger:failed}">
                    <td><label class="checkbox"><input type="checkbox" checked={selected} onclick={toggleFix}/></label>
                    </td>
                    <td><a onclick={selectVenue} href={getHref(old.attributes.id)}>#</a></td>
                    <td>{old.attributes.name}</td>
                    <td>{typeof diff.name !== 'undefined' ? (diff.name === '' ? '""' : diff.name) : 'Attrs'}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script>
      this.ctrl = this.opts.ctrl;

      this.getHref = (id) => {
        const l = window.location;
        return l.protocol + '//' + l.host + l.pathname + '?venues=' + id;
      };

      this.toggleFix = (e) => {
        e.item.selected = !e.item.selected;
      };
    </script>
</automate-content>

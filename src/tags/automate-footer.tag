<automate-footer>
    <div class="hero-foot">
        <footer class="is-fullwidth">
            <div class="container" style="width: auto;">
                <div class="columns">
                    <div class="column">
                        <a class="button is-info is-fullwidth" onclick={scan}>Scan</a>
                    </div>
                    <div class="column">
                        <a class="button is-success is-fullwidth" onclick={fix}>Fix</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script>
      const ctrl = this.opts.ctrl;
      this.scan = async (e) => {
        e.preventDefault();
        const data = await ctrl.scanner.scan();

        let merged;
        if (data.length > 1) {
          merged = ctrl.deepmerge.all(data);
        } else {
          merged = data[0];
        }
        const parkings = ctrl.filter.filter(merged, 'venues', 'PARKING_LOT');
        const parkingsToFix = ctrl.rules.getRules('PARKING_LOT')(parkings);
        ctrl.data.PARKING_LOT = parkingsToFix.map((it) => {
          it.selected = !it.diff.manual;
          it.selectVenue = ctrl.scanner.selector('venues', it.old.attributes.id);
          return it;
        });
        this.opts.parent.update();
      };

      this.fix = async (e) => {
        e.preventDefault();
        this.opts.parent.update();
        const toFix = ctrl.data.PARKING_LOT.filter((it) => {
          return it.selected
        });
        ctrl.fixer.fix(toFix, ctrl.rules.getRules('PARKING_LOT'));
        this.opts.parent.update();
      }
    </script>
</automate-footer>

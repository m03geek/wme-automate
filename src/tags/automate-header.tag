<automate-header>
    <!--<div class="sidebar-wrapper sidebar-top">-->
    <!--<h6 class="title is-5">{ this.opts.ctrl.title }-->
    <!--<button class="delete is-small is-pulled-right" onclick={ close }></button>-->
    <!--</h6>-->
    <!--</div>-->
    <div class="hero-head">
        <header class="nav">
            <div class="container is-fluid">
                <div class="nav-left nav-item" style="flex-basis: auto;">
                    <h4 class="title is-4">{ this.opts.ctrl.title }</h4>
                </div>
                <div class="nav-right nav-item">
                    <button class="delete is-small is-danger" onclick={close}></button>
                </div>
            </div>
        </header>
    </div>

    <script>
      this.title = this.opts.ctrl.title;
      this.close = (e) => {
        e.preventDefault();
        this.opts.ctrl.ui.closeToolbar();
      }
    </script>
</automate-header>

<automate-button>
    <div class="icon-toolbar" onclick={toggle}></div>
    <script>
      this.toggle = (e) => {
        e.preventDefault();
        this.opts.controller.ui.toggleToolbar();
      }
    </script>
</automate-button>
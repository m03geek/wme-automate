// ==UserScript==
// @name        wme-automate
// @namespace   https://waze.com
// @description This script automates correction of various names
// @version     1.1.4
// @include     https://editor-beta.waze.com/*
// @include     https://*.waze.com/editor/editor/*
// @include     https://*.waze.com/*/editor/*
// @match       https://world.waze.com/editor/*
// @match       https://*.waze.com/editor/*
// @match       https://*.waze.com/*/editor/*
// @match       https://world.waze.com/map-editor/*
// @match       https://world.waze.com/beta_editor/*
// @match       https://www.waze.com/map-editor/*
// @grant       none
// @updateURL   https://gitlab.com/m03geek/wme-automate/raw/master/dist/wme-automate.user.js
// @downloadURL https://gitlab.com/m03geek/wme-automate/raw/master/dist/wme-automate.user.js
// @supportURL  https://gitlab.com/m03geek/wme-automate/issues
// ==/UserScript==

/******/
(function (modules) { // webpackBootstrap
  /******/ 	// The module cache
  /******/
  var installedModules = {};
  /******/
  /******/ 	// The require function
  /******/
  function __webpack_require__(moduleId) {
    /******/
    /******/ 		// Check if module is in cache
    /******/
    if (installedModules[moduleId]) {
      /******/
      return installedModules[moduleId].exports;
      /******/
    }
    /******/ 		// Create a new module (and put it into the cache)
    /******/
    var module = installedModules[moduleId] = {
      /******/      i: moduleId,
      /******/      l: false,
      /******/      exports: {}
      /******/
    };
    /******/
    /******/ 		// Execute the module function
    /******/
    modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
    /******/
    /******/ 		// Flag the module as loaded
    /******/
    module.l = true;
    /******/
    /******/ 		// Return the exports of the module
    /******/
    return module.exports;
    /******/
  }

  /******/
  /******/
  /******/ 	// expose the modules object (__webpack_modules__)
  /******/
  __webpack_require__.m = modules;
  /******/
  /******/ 	// expose the module cache
  /******/
  __webpack_require__.c = installedModules;
  /******/
  /******/ 	// identity function for calling harmony imports with the correct context
  /******/
  __webpack_require__.i = function (value) {
    return value;
  };
  /******/
  /******/ 	// define getter function for harmony exports
  /******/
  __webpack_require__.d = function (exports, name, getter) {
    /******/
    if (!__webpack_require__.o(exports, name)) {
      /******/
      Object.defineProperty(exports, name, {
        /******/        configurable: false,
        /******/        enumerable: true,
        /******/        get: getter
        /******/
      });
      /******/
    }
    /******/
  };
  /******/
  /******/ 	// getDefaultExport function for compatibility with non-harmony modules
  /******/
  __webpack_require__.n = function (module) {
    /******/
    var getter = module && module.__esModule ?
      /******/      function getDefault() {
        return module['default'];
      } :
      /******/      function getModuleExports() {
        return module;
      };
    /******/
    __webpack_require__.d(getter, 'a', getter);
    /******/
    return getter;
    /******/
  };
  /******/
  /******/ 	// Object.prototype.hasOwnProperty.call
  /******/
  __webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  };
  /******/
  /******/ 	// __webpack_public_path__
  /******/
  __webpack_require__.p = "";
  /******/
  /******/ 	// Load entry module and return exports
  /******/
  return __webpack_require__(__webpack_require__.s = 17);
  /******/
})
/************************************************************************/
/******/([
  /* 0 */
  /***/ (function (module, exports, __webpack_require__) {

    /* Riot v3.6.1, @license MIT */
    (function (global, factory) {
      true ? factory(exports) :
        typeof define === 'function' && define.amd ? define(['exports'], factory) :
          (factory((global.riot = global.riot || {})));
    }(this, (function (exports) {
      'use strict';

      var __TAGS_CACHE = [];
      var __TAG_IMPL = {};
      var GLOBAL_MIXIN = '__global_mixin';
      var ATTRS_PREFIX = 'riot-';
      var REF_DIRECTIVES = ['ref', 'data-ref'];
      var IS_DIRECTIVE = 'data-is';
      var CONDITIONAL_DIRECTIVE = 'if';
      var LOOP_DIRECTIVE = 'each';
      var LOOP_NO_REORDER_DIRECTIVE = 'no-reorder';
      var SHOW_DIRECTIVE = 'show';
      var HIDE_DIRECTIVE = 'hide';
      var RIOT_EVENTS_KEY = '__riot-events__';
      var T_STRING = 'string';
      var T_OBJECT = 'object';
      var T_UNDEF = 'undefined';
      var T_FUNCTION = 'function';
      var XLINK_NS = 'http://www.w3.org/1999/xlink';
      var SVG_NS = 'http://www.w3.org/2000/svg';
      var XLINK_REGEX = /^xlink:(\w+)/;
      var WIN = typeof window === T_UNDEF ? undefined : window;
      var RE_SPECIAL_TAGS = /^(?:t(?:body|head|foot|[rhd])|caption|col(?:group)?|opt(?:ion|group))$/;
      var RE_SPECIAL_TAGS_NO_OPTION = /^(?:t(?:body|head|foot|[rhd])|caption|col(?:group)?)$/;
      var RE_EVENTS_PREFIX = /^on/;
      var RE_RESERVED_NAMES = /^(?:_(?:item|id|parent)|update|root|(?:un)?mount|mixin|is(?:Mounted|Loop)|tags|refs|parent|opts|trigger|o(?:n|ff|ne))$/;
      var RE_HTML_ATTRS = /([-\w]+) ?= ?(?:"([^"]*)|'([^']*)|({[^}]*}))/g;
      var CASE_SENSITIVE_ATTRIBUTES = {'viewbox': 'viewBox'};
      var RE_BOOL_ATTRS = /^(?:disabled|checked|readonly|required|allowfullscreen|auto(?:focus|play)|compact|controls|default|formnovalidate|hidden|ismap|itemscope|loop|multiple|muted|no(?:resize|shade|validate|wrap)?|open|reversed|seamless|selected|sortable|truespeed|typemustmatch)$/;
      var IE_VERSION = (WIN && WIN.document || {}).documentMode | 0;

      /**
       * Check Check if the passed argument is undefined
       * @param   { String } value -
       * @returns { Boolean } -
       */
      function isBoolAttr(value) {
        return RE_BOOL_ATTRS.test(value)
      }

      /**
       * Check if passed argument is a function
       * @param   { * } value -
       * @returns { Boolean } -
       */
      function isFunction(value) {
        return typeof value === T_FUNCTION
      }

      /**
       * Check if passed argument is an object, exclude null
       * NOTE: use isObject(x) && !isArray(x) to excludes arrays.
       * @param   { * } value -
       * @returns { Boolean } -
       */
      function isObject(value) {
        return value && typeof value === T_OBJECT // typeof null is 'object'
      }

      /**
       * Check if passed argument is undefined
       * @param   { * } value -
       * @returns { Boolean } -
       */
      function isUndefined(value) {
        return typeof value === T_UNDEF
      }

      /**
       * Check if passed argument is a string
       * @param   { * } value -
       * @returns { Boolean } -
       */
      function isString(value) {
        return typeof value === T_STRING
      }

      /**
       * Check if passed argument is empty. Different from falsy, because we dont consider 0 or false to be blank
       * @param { * } value -
       * @returns { Boolean } -
       */
      function isBlank(value) {
        return isUndefined(value) || value === null || value === ''
      }

      /**
       * Check if passed argument is a kind of array
       * @param   { * } value -
       * @returns { Boolean } -
       */
      function isArray(value) {
        return Array.isArray(value) || value instanceof Array
      }

      /**
       * Check whether object's property could be overridden
       * @param   { Object }  obj - source object
       * @param   { String }  key - object property
       * @returns { Boolean } -
       */
      function isWritable(obj, key) {
        var descriptor = Object.getOwnPropertyDescriptor(obj, key);
        return isUndefined(obj[key]) || descriptor && descriptor.writable
      }

      /**
       * Check if passed argument is a reserved name
       * @param   { String } value -
       * @returns { Boolean } -
       */
      function isReservedName(value) {
        return RE_RESERVED_NAMES.test(value)
      }

      var check = Object.freeze({
        isBoolAttr: isBoolAttr,
        isFunction: isFunction,
        isObject: isObject,
        isUndefined: isUndefined,
        isString: isString,
        isBlank: isBlank,
        isArray: isArray,
        isWritable: isWritable,
        isReservedName: isReservedName
      });

      /**
       * Shorter and fast way to select multiple nodes in the DOM
       * @param   { String } selector - DOM selector
       * @param   { Object } ctx - DOM node where the targets of our search will is located
       * @returns { Object } dom nodes found
       */
      function $$(selector, ctx) {
        return Array.prototype.slice.call((ctx || document).querySelectorAll(selector))
      }

      /**
       * Shorter and fast way to select a single node in the DOM
       * @param   { String } selector - unique dom selector
       * @param   { Object } ctx - DOM node where the target of our search will is located
       * @returns { Object } dom node found
       */
      function $(selector, ctx) {
        return (ctx || document).querySelector(selector)
      }

      /**
       * Create a document fragment
       * @returns { Object } document fragment
       */
      function createFrag() {
        return document.createDocumentFragment()
      }

      /**
       * Create a document text node
       * @returns { Object } create a text node to use as placeholder
       */
      function createDOMPlaceholder() {
        return document.createTextNode('')
      }

      /**
       * Check if a DOM node is an svg tag
       * @param   { HTMLElement }  el - node we want to test
       * @returns {Boolean} true if it's an svg node
       */
      function isSvg(el) {
        return !!el.ownerSVGElement
      }

      /**
       * Create a generic DOM node
       * @param   { String } name - name of the DOM node we want to create
       * @param   { Boolean } isSvg - true if we need to use an svg node
       * @returns { Object } DOM node just created
       */
      function mkEl(name) {
        return name === 'svg' ? document.createElementNS(SVG_NS, name) : document.createElement(name)
      }

      /**
       * Set the inner html of any DOM node SVGs included
       * @param { Object } container - DOM node where we'll inject new html
       * @param { String } html - html to inject
       */

      /* istanbul ignore next */
      function setInnerHTML(container, html) {
        if (!isUndefined(container.innerHTML)) { container.innerHTML = html; }
        // some browsers do not support innerHTML on the SVGs tags
        else {
          var doc = new DOMParser().parseFromString(html, 'application/xml');
          var node = container.ownerDocument.importNode(doc.documentElement, true);
          container.appendChild(node);
        }
      }

      /**
       * Toggle the visibility of any DOM node
       * @param   { Object }  dom - DOM node we want to hide
       * @param   { Boolean } show - do we want to show it?
       */

      function toggleVisibility(dom, show) {
        dom.style.display = show ? '' : 'none';
        dom['hidden'] = show ? false : true;
      }

      /**
       * Remove any DOM attribute from a node
       * @param   { Object } dom - DOM node we want to update
       * @param   { String } name - name of the property we want to remove
       */
      function remAttr(dom, name) {
        dom.removeAttribute(name);
      }

      /**
       * Convert a style object to a string
       * @param   { Object } style - style object we need to parse
       * @returns { String } resulting css string
       * @example
       * styleObjectToString({ color: 'red', height: '10px'}) // => 'color: red; height: 10px'
       */
      function styleObjectToString(style) {
        return Object.keys(style).reduce(function (acc, prop) {
          return (acc + " " + prop + ": " + (style[prop]) + ";")
        }, '')
      }

      /**
       * Get the value of any DOM attribute on a node
       * @param   { Object } dom - DOM node we want to parse
       * @param   { String } name - name of the attribute we want to get
       * @returns { String | undefined } name of the node attribute whether it exists
       */
      function getAttr(dom, name) {
        return dom.getAttribute(name)
      }

      /**
       * Set any DOM attribute
       * @param { Object } dom - DOM node we want to update
       * @param { String } name - name of the property we want to set
       * @param { String } val - value of the property we want to set
       */
      function setAttr(dom, name, val) {
        var xlink = XLINK_REGEX.exec(name);
        if (xlink && xlink[1]) { dom.setAttributeNS(XLINK_NS, xlink[1], val); }
        else { dom.setAttribute(name, val); }
      }

      /**
       * Insert safely a tag to fix #1962 #1649
       * @param   { HTMLElement } root - children container
       * @param   { HTMLElement } curr - node to insert
       * @param   { HTMLElement } next - node that should preceed the current node inserted
       */
      function safeInsert(root, curr, next) {
        root.insertBefore(curr, next.parentNode && next);
      }

      /**
       * Minimize risk: only zero or one _space_ between attr & value
       * @param   { String }   html - html string we want to parse
       * @param   { Function } fn - callback function to apply on any attribute found
       */
      function walkAttrs(html, fn) {
        if (!html) { return }
        var m;
        while (m = RE_HTML_ATTRS.exec(html)) { fn(m[1].toLowerCase(), m[2] || m[3] || m[4]); }
      }

      /**
       * Walk down recursively all the children tags starting dom node
       * @param   { Object }   dom - starting node where we will start the recursion
       * @param   { Function } fn - callback to transform the child node just found
       * @param   { Object }   context - fn can optionally return an object, which is passed to children
       */
      function walkNodes(dom, fn, context) {
        if (dom) {
          var res = fn(dom, context);
          var next;
          // stop the recursion
          if (res === false) { return }

          dom = dom.firstChild;

          while (dom) {
            next = dom.nextSibling;
            walkNodes(dom, fn, res);
            dom = next;
          }
        }
      }

      var dom = Object.freeze({
        $$: $$,
        $: $,
        createFrag: createFrag,
        createDOMPlaceholder: createDOMPlaceholder,
        isSvg: isSvg,
        mkEl: mkEl,
        setInnerHTML: setInnerHTML,
        toggleVisibility: toggleVisibility,
        remAttr: remAttr,
        styleObjectToString: styleObjectToString,
        getAttr: getAttr,
        setAttr: setAttr,
        safeInsert: safeInsert,
        walkAttrs: walkAttrs,
        walkNodes: walkNodes
      });

      var styleNode;
      var cssTextProp;
      var byName = {};
      var remainder = [];
      var needsInject = false;

// skip the following code on the server
      if (WIN) {
        styleNode = (function () {
          // create a new style element with the correct type
          var newNode = mkEl('style');
          setAttr(newNode, 'type', 'text/css');

          // replace any user node or insert the new one into the head
          var userNode = $('style[type=riot]');
          /* istanbul ignore next */
          if (userNode) {
            if (userNode.id) { newNode.id = userNode.id; }
            userNode.parentNode.replaceChild(newNode, userNode);
    }
          else { document.getElementsByTagName('head')[0].appendChild(newNode); }

          return newNode
        })();
        cssTextProp = styleNode.styleSheet;
      }

      /**
       * Object that will be used to inject and manage the css of every tag instance
       */
      var styleManager = {
        styleNode: styleNode,
        /**
         * Save a tag style to be later injected into DOM
         * @param { String } css - css string
         * @param { String } name - if it's passed we will map the css to a tagname
         */
        add: function add(css, name) {
          if (name) { byName[name] = css; }
          else { remainder.push(css); }
          needsInject = true;
        },
        /**
         * Inject all previously saved tag styles into DOM
         * innerHTML seems slow: http://jsperf.com/riot-insert-style
         */
        inject: function inject() {
          if (!WIN || !needsInject) { return }
          needsInject = false;
          var style = Object.keys(byName)
            .map(function (k) {
              return byName[k]
            })
            .concat(remainder).join('\n');
          /* istanbul ignore next */
          if (cssTextProp) { cssTextProp.cssText = style; }
          else { styleNode.innerHTML = style; }
        }
      };

      /**
       * The riot template engine
       * @version v3.0.8
       */

      var skipRegex = (function () { //eslint-disable-line no-unused-vars

        var beforeReChars = '[{(,;:?=|&!^~>%*/';

        var beforeReWords = [
          'case',
          'default',
          'do',
          'else',
          'in',
          'instanceof',
          'prefix',
          'return',
          'typeof',
          'void',
          'yield'
        ];

        var wordsLastChar = beforeReWords.reduce(function (s, w) {
          return s + w.slice(-1)
        }, '');

        var RE_REGEX = /^\/(?=[^*>/])[^[/\\]*(?:(?:\\.|\[(?:\\.|[^\]\\]*)*\])[^[\\/]*)*?\/[gimuy]*/;
        var RE_VN_CHAR = /[$\w]/;

        function prev(code, pos) {
          while (--pos >= 0 && /\s/.test(code[pos])) { }
          return pos
        }

        function _skipRegex(code, start) {

          var re = /.*/g;
          var pos = re.lastIndex = start++;
          var match = re.exec(code)[0].match(RE_REGEX);

          if (match) {
            var next = pos + match[0].length;

            pos = prev(code, pos);
            var c = code[pos];

            if (pos < 0 || ~beforeReChars.indexOf(c)) {
              return next
            }

            if (c === '.') {

              if (code[pos - 1] === '.') {
                start = next;
              }

            } else if (c === '+' || c === '-') {

              if (code[--pos] !== c ||
                (pos = prev(code, pos)) < 0 ||
                !RE_VN_CHAR.test(code[pos])) {
                start = next;
              }

            } else if (~wordsLastChar.indexOf(c)) {

              var end = pos + 1;

              while (--pos >= 0 && RE_VN_CHAR.test(code[pos])) { }
              if (~beforeReWords.indexOf(code.slice(pos + 1, end))) {
                start = next;
              }
            }
          }

          return start
        }

        return _skipRegex

      })();

      /**
       * riot.util.brackets
       *
       * - `brackets    ` - Returns a string or regex based on its parameter
       * - `brackets.set` - Change the current riot brackets
       *
       * @module
       */

      /* global riot */

      /* istanbul ignore next */
      var brackets = (function (UNDEF) {

        var
          REGLOB = 'g',

          R_MLCOMMS = /\/\*[^*]*\*+(?:[^*\/][^*]*\*+)*\//g,

          R_STRINGS = /"[^"\\]*(?:\\[\S\s][^"\\]*)*"|'[^'\\]*(?:\\[\S\s][^'\\]*)*'|`[^`\\]*(?:\\[\S\s][^`\\]*)*`/g,

          S_QBLOCKS = R_STRINGS.source + '|' +
            /(?:\breturn\s+|(?:[$\w\)\]]|\+\+|--)\s*(\/)(?![*\/]))/.source + '|' +
            /\/(?=[^*\/])[^[\/\\]*(?:(?:\[(?:\\.|[^\]\\]*)*\]|\\.)[^[\/\\]*)*?([^<]\/)[gim]*/.source,

          UNSUPPORTED = RegExp('[\\' + 'x00-\\x1F<>a-zA-Z0-9\'",;\\\\]'),

          NEED_ESCAPE = /(?=[[\]()*+?.^$|])/g,

          S_QBLOCK2 = R_STRINGS.source + '|' + /(\/)(?![*\/])/.source,

          FINDBRACES = {
            '(': RegExp('([()])|' + S_QBLOCK2, REGLOB),
            '[': RegExp('([[\\]])|' + S_QBLOCK2, REGLOB),
            '{': RegExp('([{}])|' + S_QBLOCK2, REGLOB)
          },

          DEFAULT = '{ }';

        var _pairs = [
          '{', '}',
          '{', '}',
          /{[^}]*}/,
          /\\([{}])/g,
          /\\({)|{/g,
          RegExp('\\\\(})|([[({])|(})|' + S_QBLOCK2, REGLOB),
          DEFAULT,
          /^\s*{\^?\s*([$\w]+)(?:\s*,\s*(\S+))?\s+in\s+(\S.*)\s*}/,
          /(^|[^\\]){=[\S\s]*?}/
        ];

        var
          cachedBrackets = UNDEF,
          _regex,
          _cache = [],
          _settings;

        function _loopback(re) {
          return re
        }

        function _rewrite(re, bp) {
          if (!bp) { bp = _cache; }
          return new RegExp(
            re.source.replace(/{/g, bp[2]).replace(/}/g, bp[3]), re.global ? REGLOB : ''
          )
        }

        function _create(pair) {
          if (pair === DEFAULT) { return _pairs }

          var arr = pair.split(' ');

          if (arr.length !== 2 || UNSUPPORTED.test(pair)) {
            throw new Error('Unsupported brackets "' + pair + '"')
          }
          arr = arr.concat(pair.replace(NEED_ESCAPE, '\\').split(' '));

          arr[4] = _rewrite(arr[1].length > 1 ? /{[\S\s]*?}/ : _pairs[4], arr);
          arr[5] = _rewrite(pair.length > 3 ? /\\({|})/g : _pairs[5], arr);
          arr[6] = _rewrite(_pairs[6], arr);
          arr[7] = RegExp('\\\\(' + arr[3] + ')|([[({])|(' + arr[3] + ')|' + S_QBLOCK2, REGLOB);
          arr[8] = pair;
          return arr
        }

        function _brackets(reOrIdx) {
          return reOrIdx instanceof RegExp ? _regex(reOrIdx) : _cache[reOrIdx]
        }

        _brackets.split = function split(str, tmpl, _bp) {
          // istanbul ignore next: _bp is for the compiler
          if (!_bp) { _bp = _cache; }

          var
            parts = [],
            match,
            isexpr,
            start,
            pos,
            re = _bp[6];

          var qblocks = [];
          var prevStr = '';
          var mark, lastIndex;

          isexpr = start = re.lastIndex = 0;

          while ((match = re.exec(str))) {

            lastIndex = re.lastIndex;
            pos = match.index;

            if (isexpr) {

              if (match[2]) {

                var ch = match[2];
                var rech = FINDBRACES[ch];
                var ix = 1;

                rech.lastIndex = lastIndex;
                while ((match = rech.exec(str))) {
                  if (match[1]) {
                    if (match[1] === ch) { ++ix; }
                    else if (!--ix) { break }
                  } else {
                    rech.lastIndex = pushQBlock(match.index, rech.lastIndex, match[2]);
                  }
                }
                re.lastIndex = ix ? str.length : rech.lastIndex;
                continue
              }

              if (!match[3]) {
                re.lastIndex = pushQBlock(pos, lastIndex, match[4]);
                continue
              }
            }

            if (!match[1]) {
              unescapeStr(str.slice(start, pos));
              start = re.lastIndex;
              re = _bp[6 + (isexpr ^= 1)];
              re.lastIndex = start;
            }
          }

          if (str && start < str.length) {
            unescapeStr(str.slice(start));
          }

          parts.qblocks = qblocks;

          return parts;

          function unescapeStr(s) {
            if (prevStr) {
              s = prevStr + s;
              prevStr = '';
            }
            if (tmpl || isexpr) {
              parts.push(s && s.replace(_bp[5], '$1'));
            } else {
              parts.push(s);
            }
          }

          function pushQBlock(_pos, _lastIndex, slash) { //eslint-disable-line
            if (slash) {
              _lastIndex = skipRegex(str, _pos);
            }

            if (tmpl && _lastIndex > _pos + 2) {
              mark = '\u2057' + qblocks.length + '~';
              qblocks.push(str.slice(_pos, _lastIndex));
              prevStr += str.slice(start, _pos) + mark;
              start = _lastIndex;
            }
            return _lastIndex
          }
        };

        _brackets.hasExpr = function hasExpr(str) {
          return _cache[4].test(str)
        };

        _brackets.loopKeys = function loopKeys(expr) {
          var m = expr.match(_cache[9]);

          return m
            ? {key: m[1], pos: m[2], val: _cache[0] + m[3].trim() + _cache[1]}
            : {val: expr.trim()}
        };

        _brackets.array = function array(pair) {
          return pair ? _create(pair) : _cache
        };

        function _reset(pair) {
          if ((pair || (pair = DEFAULT)) !== _cache[8]) {
            _cache = _create(pair);
            _regex = pair === DEFAULT ? _loopback : _rewrite;
            _cache[9] = _regex(_pairs[9]);
          }
          cachedBrackets = pair;
        }

        function _setSettings(o) {
          var b;

          o = o || {};
          b = o.brackets;
          Object.defineProperty(o, 'brackets', {
            set: _reset,
            get: function () {
              return cachedBrackets
            },
            enumerable: true
          });
          _settings = o;
          _reset(b);
        }

        Object.defineProperty(_brackets, 'settings', {
          set: _setSettings,
          get: function () {
            return _settings
          }
        });

        /* istanbul ignore next: in the browser riot is always in the scope */
        _brackets.settings = typeof riot !== 'undefined' && riot.settings || {};
        _brackets.set = _reset;
        _brackets.skipRegex = skipRegex;

        _brackets.R_STRINGS = R_STRINGS;
        _brackets.R_MLCOMMS = R_MLCOMMS;
        _brackets.S_QBLOCKS = S_QBLOCKS;
        _brackets.S_QBLOCK2 = S_QBLOCK2;

        return _brackets

      })();

      /**
       * @module tmpl
       *
       * tmpl          - Root function, returns the template value, render with data
       * tmpl.hasExpr  - Test the existence of a expression inside a string
       * tmpl.loopKeys - Get the keys for an 'each' loop (used by `_each`)
       */

      /* istanbul ignore next */
      var tmpl = (function () {

        var _cache = {};

        function _tmpl(str, data) {
          if (!str) { return str }

          return (_cache[str] || (_cache[str] = _create(str))).call(
            data, _logErr.bind({
              data: data,
              tmpl: str
            })
          )
        }

        _tmpl.hasExpr = brackets.hasExpr;

        _tmpl.loopKeys = brackets.loopKeys;

        // istanbul ignore next
        _tmpl.clearCache = function () {
          _cache = {};
        };

        _tmpl.errorHandler = null;

        function _logErr(err, ctx) {

          err.riotData = {
            tagName: ctx && ctx.__ && ctx.__.tagName,
            _riot_id: ctx && ctx._riot_id  //eslint-disable-line camelcase
          };

          if (_tmpl.errorHandler) { _tmpl.errorHandler(err); }
          else if (
            typeof console !== 'undefined' &&
            typeof console.error === 'function'
          ) {
            console.error(err.message);
            console.log('<%s> %s', err.riotData.tagName || 'Unknown tag', this.tmpl); // eslint-disable-line
            console.log(this.data); // eslint-disable-line
          }
        }

        function _create(str) {
          var expr = _getTmpl(str);

          if (expr.slice(0, 11) !== 'try{return ') { expr = 'return ' + expr; }

          return new Function('E', expr + ';')    // eslint-disable-line no-new-func
        }

        var RE_DQUOTE = /\u2057/g;
        var RE_QBMARK = /\u2057(\d+)~/g;

        function _getTmpl(str) {
          var parts = brackets.split(str.replace(RE_DQUOTE, '"'), 1);
          var qstr = parts.qblocks;
          var expr;

          if (parts.length > 2 || parts[0]) {
            var i, j, list = [];

            for (i = j = 0; i < parts.length; ++i) {

              expr = parts[i];

              if (expr && (expr = i & 1

                    ? _parseExpr(expr, 1, qstr)

                    : '"' + expr
                      .replace(/\\/g, '\\\\')
                      .replace(/\r\n?|\n/g, '\\n')
                      .replace(/"/g, '\\"') +
                    '"'

                )) { list[j++] = expr; }

            }

            expr = j < 2 ? list[0]
              : '[' + list.join(',') + '].join("")';

          } else {

            expr = _parseExpr(parts[1], 0, qstr);
          }

          if (qstr.length) {
            expr = expr.replace(RE_QBMARK, function (_, pos) {
              return qstr[pos]
                .replace(/\r/g, '\\r')
                .replace(/\n/g, '\\n')
            });
          }
          return expr
        }

        var RE_CSNAME = /^(?:(-?[_A-Za-z\xA0-\xFF][-\w\xA0-\xFF]*)|\u2057(\d+)~):/;
        var
          RE_BREND = {
            '(': /[()]/g,
            '[': /[[\]]/g,
            '{': /[{}]/g
          };

        function _parseExpr(expr, asText, qstr) {

          expr = expr
            .replace(/\s+/g, ' ').trim()
            .replace(/\ ?([[\({},?\.:])\ ?/g, '$1');

          if (expr) {
            var
              list = [],
              cnt = 0,
              match;

            while (expr &&
              (match = expr.match(RE_CSNAME)) &&
              !match.index
              ) {
              var
                key,
                jsb,
                re = /,|([[{(])|$/g;

              expr = RegExp.rightContext;
              key = match[2] ? qstr[match[2]].slice(1, -1).trim().replace(/\s+/g, ' ') : match[1];

              while (jsb = (match = re.exec(expr))[1]) { skipBraces(jsb, re); }

              jsb = expr.slice(0, match.index);
              expr = RegExp.rightContext;

              list[cnt++] = _wrapExpr(jsb, 1, key);
            }

            expr = !cnt ? _wrapExpr(expr, asText)
              : cnt > 1 ? '[' + list.join(',') + '].join(" ").trim()' : list[0];
          }
          return expr;

          function skipBraces(ch, re) {
            var
              mm,
              lv = 1,
              ir = RE_BREND[ch];

            ir.lastIndex = re.lastIndex;
            while (mm = ir.exec(expr)) {
              if (mm[0] === ch) { ++lv; }
              else if (!--lv) { break }
            }
            re.lastIndex = lv ? expr.length : ir.lastIndex;
          }
        }

        // istanbul ignore next: not both
        var // eslint-disable-next-line max-len
          JS_CONTEXT = '"in this?this:' + (typeof window !== 'object' ? 'global' : 'window') + ').',
          JS_VARNAME = /[,{][\$\w]+(?=:)|(^ *|[^$\w\.{])(?!(?:typeof|true|false|null|undefined|in|instanceof|is(?:Finite|NaN)|void|NaN|new|Date|RegExp|Math)(?![$\w]))([$_A-Za-z][$\w]*)/g,
          JS_NOPROPS = /^(?=(\.[$\w]+))\1(?:[^.[(]|$)/;

        function _wrapExpr(expr, asText, key) {
          var tb;

          expr = expr.replace(JS_VARNAME, function (match, p, mvar, pos, s) {
            if (mvar) {
              pos = tb ? 0 : pos + match.length;

              if (mvar !== 'this' && mvar !== 'global' && mvar !== 'window') {
                match = p + '("' + mvar + JS_CONTEXT + mvar;
                if (pos) { tb = (s = s[pos]) === '.' || s === '(' || s === '['; }
              } else if (pos) {
                tb = !JS_NOPROPS.test(s.slice(pos));
              }
            }
            return match
          });

          if (tb) {
            expr = 'try{return ' + expr + '}catch(e){E(e,this)}';
          }

          if (key) {

            expr = (tb
                ? 'function(){' + expr + '}.call(this)' : '(' + expr + ')'
            ) + '?"' + key + '":""';

          } else if (asText) {

            expr = 'function(v){' + (tb
                ? expr.replace('return ', 'v=') : 'v=(' + expr + ')'
            ) + ';return v||v===0?v:""}.call(this)';
          }

          return expr
        }

        _tmpl.version = brackets.version = 'v3.0.8';

        return _tmpl

      })();

      /* istanbul ignore next */
      var observable$1 = function (el) {

        /**
         * Extend the original object or create a new empty one
         * @type { Object }
         */

        el = el || {};

        /**
         * Private variables
         */
        var callbacks = {},
          slice = Array.prototype.slice;

        /**
         * Public Api
         */

        // extend the el object adding the observable methods
        Object.defineProperties(el, {
          /**
           * Listen to the given `event` ands
           * execute the `callback` each time an event is triggered.
           * @param  { String } event - event id
           * @param  { Function } fn - callback function
           * @returns { Object } el
           */
          on: {
            value: function (event, fn) {
              if (typeof fn == 'function') { (callbacks[event] = callbacks[event] || []).push(fn); }
              return el
            },
            enumerable: false,
            writable: false,
            configurable: false
          },

          /**
           * Removes the given `event` listeners
           * @param   { String } event - event id
           * @param   { Function } fn - callback function
           * @returns { Object } el
           */
          off: {
            value: function (event, fn) {
              if (event == '*' && !fn) { callbacks = {}; }
              else {
                if (fn) {
                  var arr = callbacks[event];
                  for (var i = 0, cb; cb = arr && arr[i]; ++i) {
                    if (cb == fn) { arr.splice(i--, 1); }
                  }
                } else { delete callbacks[event]; }
              }
              return el
            },
            enumerable: false,
            writable: false,
            configurable: false
          },

          /**
           * Listen to the given `event` and
           * execute the `callback` at most once
           * @param   { String } event - event id
           * @param   { Function } fn - callback function
           * @returns { Object } el
           */
          one: {
            value: function (event, fn) {
              function on() {
                el.off(event, on);
                fn.apply(el, arguments);
              }

              return el.on(event, on)
            },
            enumerable: false,
            writable: false,
            configurable: false
          },

          /**
           * Execute all callback functions that listen to
           * the given `event`
           * @param   { String } event - event id
           * @returns { Object } el
           */
          trigger: {
            value: function (event) {
              var arguments$1 = arguments;

              // getting the arguments
              var arglen = arguments.length - 1,
                args = new Array(arglen),
                fns,
                fn,
                i;

              for (i = 0; i < arglen; i++) {
                args[i] = arguments$1[i + 1]; // skip first argument
              }

              fns = slice.call(callbacks[event] || [], 0);

              for (i = 0; fn = fns[i]; ++i) {
                fn.apply(el, args);
              }

              if (callbacks['*'] && event != '*') { el.trigger.apply(el, ['*', event].concat(args)); }

              return el
            },
            enumerable: false,
            writable: false,
            configurable: false
          }
        });

        return el

      };

      /**
       * Specialized function for looping an array-like collection with `each={}`
       * @param   { Array } list - collection of items
       * @param   {Function} fn - callback function
       * @returns { Array } the array looped
       */
      function each(list, fn) {
        var len = list ? list.length : 0;
        var i = 0;
        for (; i < len; ++i) {
          fn(list[i], i);
        }
        return list
      }

      /**
       * Check whether an array contains an item
       * @param   { Array } array - target array
       * @param   { * } item - item to test
       * @returns { Boolean } -
       */
      function contains(array, item) {
        return array.indexOf(item) !== -1
      }

      /**
       * Convert a string containing dashes to camel case
       * @param   { String } str - input string
       * @returns { String } my-string -> myString
       */
      function toCamel(str) {
        return str.replace(/-(\w)/g, function (_, c) {
          return c.toUpperCase();
        })
      }

      /**
       * Faster String startsWith alternative
       * @param   { String } str - source string
       * @param   { String } value - test string
       * @returns { Boolean } -
       */
      function startsWith(str, value) {
        return str.slice(0, value.length) === value
      }

      /**
       * Helper function to set an immutable property
       * @param   { Object } el - object where the new property will be set
       * @param   { String } key - object key where the new property will be stored
       * @param   { * } value - value of the new property
       * @param   { Object } options - set the propery overriding the default options
       * @returns { Object } - the initial object
       */
      function defineProperty(el, key, value, options) {
        Object.defineProperty(el, key, extend({
          value: value,
          enumerable: false,
          writable: false,
          configurable: true
        }, options));
        return el
      }

      /**
       * Extend any object with other properties
       * @param   { Object } src - source object
       * @returns { Object } the resulting extended object
       *
       * var obj = { foo: 'baz' }
       * extend(obj, {bar: 'bar', foo: 'bar'})
       * console.log(obj) => {bar: 'bar', foo: 'bar'}
       *
       */
      function extend(src) {
        var obj, args = arguments;
        for (var i = 1; i < args.length; ++i) {
          if (obj = args[i]) {
            for (var key in obj) {
              // check if this property of the source object could be overridden
              if (isWritable(src, key)) { src[key] = obj[key]; }
            }
          }
        }
        return src
      }

      var misc = Object.freeze({
        each: each,
        contains: contains,
        toCamel: toCamel,
        startsWith: startsWith,
        defineProperty: defineProperty,
        extend: extend
      });

      var settings$1 = extend(Object.create(brackets.settings), {
        skipAnonymousTags: true,
        // handle the auto updates on any DOM event
        autoUpdate: true
      });

      /**
       * Trigger DOM events
       * @param   { HTMLElement } dom - dom element target of the event
       * @param   { Function } handler - user function
       * @param   { Object } e - event object
       */
      function handleEvent(dom, handler, e) {
        var ptag = this.__.parent,
          item = this.__.item;

        if (!item) {
          while (ptag && !item) {
            item = ptag.__.item;
            ptag = ptag.__.parent;
          }
        }

        // override the event properties
        /* istanbul ignore next */
        if (isWritable(e, 'currentTarget')) { e.currentTarget = dom; }
        /* istanbul ignore next */
        if (isWritable(e, 'target')) { e.target = e.srcElement; }
        /* istanbul ignore next */
        if (isWritable(e, 'which')) { e.which = e.charCode || e.keyCode; }

        e.item = item;

        handler.call(this, e);

        // avoid auto updates
        if (!settings$1.autoUpdate) { return }

        if (!e.preventUpdate) {
          var p = getImmediateCustomParentTag(this);
          // fixes #2083
          if (p.isMounted) { p.update(); }
        }
      }

      /**
       * Attach an event to a DOM node
       * @param { String } name - event name
       * @param { Function } handler - event callback
       * @param { Object } dom - dom node
       * @param { Tag } tag - tag instance
       */
      function setEventHandler(name, handler, dom, tag) {
        var eventName,
          cb = handleEvent.bind(tag, dom, handler);

        // avoid to bind twice the same event
        // possible fix for #2332
        dom[name] = null;

        // normalize event name
        eventName = name.replace(RE_EVENTS_PREFIX, '');

        // cache the listener into the listeners array
        if (!contains(tag.__.listeners, dom)) { tag.__.listeners.push(dom); }
        if (!dom[RIOT_EVENTS_KEY]) { dom[RIOT_EVENTS_KEY] = {}; }
        if (dom[RIOT_EVENTS_KEY][name]) { dom.removeEventListener(eventName, dom[RIOT_EVENTS_KEY][name]); }

        dom[RIOT_EVENTS_KEY][name] = cb;
        dom.addEventListener(eventName, cb, false);
      }

      /**
       * Update dynamically created data-is tags with changing expressions
       * @param { Object } expr - expression tag and expression info
       * @param { Tag }    parent - parent for tag creation
       * @param { String } tagName - tag implementation we want to use
       */
      function updateDataIs(expr, parent, tagName) {
        var conf, isVirtual, head, ref;

        if (expr.tag && expr.tagName === tagName) {
          expr.tag.update();
          return
        }

        isVirtual = expr.dom.tagName === 'VIRTUAL';
        // sync _parent to accommodate changing tagnames
        if (expr.tag) {
          // need placeholder before unmount
          if (isVirtual) {
            head = expr.tag.__.head;
            ref = createDOMPlaceholder();
            head.parentNode.insertBefore(ref, head);
          }

          expr.tag.unmount(true);
        }

        if (!isString(tagName)) { return }

        expr.impl = __TAG_IMPL[tagName];
        conf = {root: expr.dom, parent: parent, hasImpl: true, tagName: tagName};
        expr.tag = initChildTag(expr.impl, conf, expr.dom.innerHTML, parent);
        each(expr.attrs, function (a) {
          return setAttr(expr.tag.root, a.name, a.value);
        });
        expr.tagName = tagName;
        expr.tag.mount();
        if (isVirtual) { makeReplaceVirtual(expr.tag, ref || expr.tag.root); } // root exist first time, after use placeholder

        // parent is the placeholder tag, not the dynamic tag so clean up
        parent.__.onUnmount = function () {
          var delName = expr.tag.opts.dataIs,
            tags = expr.tag.parent.tags,
            _tags = expr.tag.__.parent.tags;
          arrayishRemove(tags, delName, expr.tag);
          arrayishRemove(_tags, delName, expr.tag);
          expr.tag.unmount();
        };
      }

      /**
       * Nomalize any attribute removing the "riot-" prefix
       * @param   { String } attrName - original attribute name
       * @returns { String } valid html attribute name
       */
      function normalizeAttrName(attrName) {
        if (!attrName) { return null }
        attrName = attrName.replace(ATTRS_PREFIX, '');
        if (CASE_SENSITIVE_ATTRIBUTES[attrName]) { attrName = CASE_SENSITIVE_ATTRIBUTES[attrName]; }
        return attrName
      }

      /**
       * Update on single tag expression
       * @this Tag
       * @param { Object } expr - expression logic
       * @returns { undefined }
       */
      function updateExpression(expr) {
        if (this.root && getAttr(this.root, 'virtualized')) { return }

        var dom = expr.dom,
          // remove the riot- prefix
          attrName = normalizeAttrName(expr.attr),
          isToggle = contains([SHOW_DIRECTIVE, HIDE_DIRECTIVE], attrName),
          isVirtual = expr.root && expr.root.tagName === 'VIRTUAL',
          parent = dom && (expr.parent || dom.parentNode),
          // detect the style attributes
          isStyleAttr = attrName === 'style',
          isClassAttr = attrName === 'class',
          hasValue,
          isObj,
          value;

        // if it's a tag we could totally skip the rest
        if (expr._riot_id) {
          if (expr.isMounted) {
            expr.update();
            // if it hasn't been mounted yet, do that now.
          } else {
            expr.mount();
            if (isVirtual) {
              makeReplaceVirtual(expr, expr.root);
            }
          }
          return
        }
        // if this expression has the update method it means it can handle the DOM changes by itself
        if (expr.update) { return expr.update() }

        // ...it seems to be a simple expression so we try to calculat its value
        value = tmpl(expr.expr, isToggle ? extend({}, Object.create(this.parent), this) : this);
        hasValue = !isBlank(value);
        isObj = isObject(value);

        // convert the style/class objects to strings
        if (isObj) {
          isObj = !isClassAttr && !isStyleAttr;
          if (isClassAttr) {
            value = tmpl(JSON.stringify(value), this);
          } else if (isStyleAttr) {
            value = styleObjectToString(value);
          }
        }

        // remove original attribute
        if (expr.attr && (!expr.isAttrRemoved || !hasValue || value === false)) {
          remAttr(dom, expr.attr);
          expr.isAttrRemoved = true;
        }

        // for the boolean attributes we don't need the value
        // we can convert it to checked=true to checked=checked
        if (expr.bool) { value = value ? attrName : false; }
        if (expr.isRtag) { return updateDataIs(expr, this, value) }
        if (expr.wasParsedOnce && expr.value === value) { return }

        // update the expression value
        expr.value = value;
        expr.wasParsedOnce = true;

        // if the value is an object we can not do much more with it
        if (isObj && !isToggle) { return }
        // avoid to render undefined/null values
        if (isBlank(value)) { value = ''; }

        // textarea and text nodes have no attribute name
        if (!attrName) {
          // about #815 w/o replace: the browser converts the value to a string,
          // the comparison by "==" does too, but not in the server
          value += '';
          // test for parent avoids error with invalid assignment to nodeValue
          if (parent) {
            // cache the parent node because somehow it will become null on IE
            // on the next iteration
            expr.parent = parent;
            if (parent.tagName === 'TEXTAREA') {
              parent.value = value;                    // #1113
              if (!IE_VERSION) { dom.nodeValue = value; }  // #1625 IE throws here, nodeValue
            }                                         // will be available on 'updated'
            else { dom.nodeValue = value; }
          }
          return
        }

        // event handler
        if (isFunction(value)) {
          setEventHandler(attrName, value, dom, this);
          // show / hide
        } else if (isToggle) {
          toggleVisibility(dom, attrName === HIDE_DIRECTIVE ? !value : value);
          // handle attributes
        } else {
          if (expr.bool) {
            dom[attrName] = value;
          }

          if (attrName === 'value' && dom.value !== value) {
            dom.value = value;
          }

          if (hasValue && value !== false) {
            setAttr(dom, attrName, value);
          }

          // make sure that in case of style changes
          // the element stays hidden
          if (isStyleAttr && dom.hidden) { toggleVisibility(dom, false); }
        }
      }

      /**
       * Update all the expressions in a Tag instance
       * @this Tag
       * @param { Array } expressions - expression that must be re evaluated
       */
      function updateAllExpressions(expressions) {
        each(expressions, updateExpression.bind(this));
      }

      var IfExpr = {
        init: function init(dom, tag, expr) {
          remAttr(dom, CONDITIONAL_DIRECTIVE);
          this.tag = tag;
          this.expr = expr;
          this.stub = createDOMPlaceholder();
          this.pristine = dom;

          var p = dom.parentNode;
          p.insertBefore(this.stub, dom);
          p.removeChild(dom);

          return this
        },
        update: function update() {
          this.value = tmpl(this.expr, this.tag);

          if (this.value && !this.current) { // insert
            this.current = this.pristine.cloneNode(true);
            this.stub.parentNode.insertBefore(this.current, this.stub);
            this.expressions = [];
            parseExpressions.apply(this.tag, [this.current, this.expressions, true]);
          } else if (!this.value && this.current) { // remove
            unmountAll(this.expressions);
            if (this.current._tag) {
              this.current._tag.unmount();
            } else if (this.current.parentNode) {
              this.current.parentNode.removeChild(this.current);
            }
            this.current = null;
            this.expressions = [];
          }

          if (this.value) { updateAllExpressions.call(this.tag, this.expressions); }
        },
        unmount: function unmount() {
          unmountAll(this.expressions || []);
        }
      };

      var RefExpr = {
        init: function init(dom, parent, attrName, attrValue) {
          this.dom = dom;
          this.attr = attrName;
          this.rawValue = attrValue;
          this.parent = parent;
          this.hasExp = tmpl.hasExpr(attrValue);
          return this
        },
        update: function update() {
          var old = this.value;
          var customParent = this.parent && getImmediateCustomParentTag(this.parent);
          // if the referenced element is a custom tag, then we set the tag itself, rather than DOM
          var tagOrDom = this.dom.__ref || this.tag || this.dom;

          this.value = this.hasExp ? tmpl(this.rawValue, this.parent) : this.rawValue;

          // the name changed, so we need to remove it from the old key (if present)
          if (!isBlank(old) && customParent) { arrayishRemove(customParent.refs, old, tagOrDom); }
          if (!isBlank(this.value) && isString(this.value)) {
            // add it to the refs of parent tag (this behavior was changed >=3.0)
            if (customParent) {
              arrayishAdd(
                customParent.refs,
                this.value,
                tagOrDom,
                // use an array if it's a looped node and the ref is not an expression
                null,
                this.parent.__.index
              );
            }

            if (this.value !== old) {
              setAttr(this.dom, this.attr, this.value);
            }
          } else {
            remAttr(this.dom, this.attr);
          }

          // cache the ref bound to this dom node
          // to reuse it in future (see also #2329)
          if (!this.dom.__ref) { this.dom.__ref = tagOrDom; }
        },
        unmount: function unmount() {
          var tagOrDom = this.tag || this.dom;
          var customParent = this.parent && getImmediateCustomParentTag(this.parent);
          if (!isBlank(this.value) && customParent) { arrayishRemove(customParent.refs, this.value, tagOrDom); }
        }
      };

      /**
       * Convert the item looped into an object used to extend the child tag properties
       * @param   { Object } expr - object containing the keys used to extend the children tags
       * @param   { * } key - value to assign to the new object returned
       * @param   { * } val - value containing the position of the item in the array
       * @param   { Object } base - prototype object for the new item
       * @returns { Object } - new object containing the values of the original item
       *
       * The variables 'key' and 'val' are arbitrary.
       * They depend on the collection type looped (Array, Object)
       * and on the expression used on the each tag
       *
       */
      function mkitem(expr, key, val, base) {
        var item = base ? Object.create(base) : {};
        item[expr.key] = key;
        if (expr.pos) { item[expr.pos] = val; }
        return item
      }

      /**
       * Unmount the redundant tags
       * @param   { Array } items - array containing the current items to loop
       * @param   { Array } tags - array containing all the children tags
       */
      function unmountRedundant(items, tags) {
        var i = tags.length,
          j = items.length;

        while (i > j) {
          i--;
          remove.apply(tags[i], [tags, i]);
        }
      }

      /**
       * Remove a child tag
       * @this Tag
       * @param   { Array } tags - tags collection
       * @param   { Number } i - index of the tag to remove
       */
      function remove(tags, i) {
        tags.splice(i, 1);
        this.unmount();
        arrayishRemove(this.parent, this, this.__.tagName, true);
      }

      /**
       * Move the nested custom tags in non custom loop tags
       * @this Tag
       * @param   { Number } i - current position of the loop tag
       */
      function moveNestedTags(i) {
        var this$1 = this;

        each(Object.keys(this.tags), function (tagName) {
          moveChildTag.apply(this$1.tags[tagName], [tagName, i]);
        });
      }

      /**
       * Move a child tag
       * @this Tag
       * @param   { HTMLElement } root - dom node containing all the loop children
       * @param   { Tag } nextTag - instance of the next tag preceding the one we want to move
       * @param   { Boolean } isVirtual - is it a virtual tag?
       */
      function move(root, nextTag, isVirtual) {
        if (isVirtual) { moveVirtual.apply(this, [root, nextTag]); }
        else { safeInsert(root, this.root, nextTag.root); }
      }

      /**
       * Insert and mount a child tag
       * @this Tag
       * @param   { HTMLElement } root - dom node containing all the loop children
       * @param   { Tag } nextTag - instance of the next tag preceding the one we want to insert
       * @param   { Boolean } isVirtual - is it a virtual tag?
       */
      function insert(root, nextTag, isVirtual) {
        if (isVirtual) { makeVirtual.apply(this, [root, nextTag]); }
        else { safeInsert(root, this.root, nextTag.root); }
      }

      /**
       * Append a new tag into the DOM
       * @this Tag
       * @param   { HTMLElement } root - dom node containing all the loop children
       * @param   { Boolean } isVirtual - is it a virtual tag?
       */
      function append(root, isVirtual) {
        if (isVirtual) { makeVirtual.call(this, root); }
        else { root.appendChild(this.root); }
      }

      /**
       * Manage tags having the 'each'
       * @param   { HTMLElement } dom - DOM node we need to loop
       * @param   { Tag } parent - parent tag instance where the dom node is contained
       * @param   { String } expr - string contained in the 'each' attribute
       * @returns { Object } expression object for this each loop
       */
      function _each(dom, parent, expr) {

        // remove the each property from the original tag
        remAttr(dom, LOOP_DIRECTIVE);

        var mustReorder = typeof getAttr(dom, LOOP_NO_REORDER_DIRECTIVE) !== T_STRING || remAttr(dom, LOOP_NO_REORDER_DIRECTIVE),
          tagName = getTagName(dom),
          impl = __TAG_IMPL[tagName],
          parentNode = dom.parentNode,
          placeholder = createDOMPlaceholder(),
          child = getTag(dom),
          ifExpr = getAttr(dom, CONDITIONAL_DIRECTIVE),
          tags = [],
          oldItems = [],
          hasKeys,
          isLoop = true,
          isAnonymous = !__TAG_IMPL[tagName],
          isVirtual = dom.tagName === 'VIRTUAL';

        // parse the each expression
        expr = tmpl.loopKeys(expr);
        expr.isLoop = true;

        if (ifExpr) { remAttr(dom, CONDITIONAL_DIRECTIVE); }

        // insert a marked where the loop tags will be injected
        parentNode.insertBefore(placeholder, dom);
        parentNode.removeChild(dom);

        expr.update = function updateEach() {
          // get the new items collection
          expr.value = tmpl(expr.val, parent);

          var frag = createFrag(),
            items = expr.value,
            isObject$$1 = !isArray(items) && !isString(items),
            root = placeholder.parentNode;

          // if this DOM was removed the update here is useless
          // this condition fixes also a weird async issue on IE in our unit test
          if (!root) { return }

          // object loop. any changes cause full redraw
          if (isObject$$1) {
            hasKeys = items || false;
            items = hasKeys ?
              Object.keys(items).map(function (key) {
                return mkitem(expr, items[key], key)
              }) : [];
          } else {
            hasKeys = false;
          }

          if (ifExpr) {
            items = items.filter(function (item, i) {
              if (expr.key && !isObject$$1) { return !!tmpl(ifExpr, mkitem(expr, item, i, parent)) }

              return !!tmpl(ifExpr, extend(Object.create(parent), item))
            });
          }

          // loop all the new items
          each(items, function (item, i) {
            // reorder only if the items are objects
            var
              doReorder = mustReorder && typeof item === T_OBJECT && !hasKeys,
              oldPos = oldItems.indexOf(item),
              isNew = oldPos === -1,
              pos = !isNew && doReorder ? oldPos : i,
              // does a tag exist in this position?
              tag = tags[pos],
              mustAppend = i >= oldItems.length,
              mustCreate = doReorder && isNew || !doReorder && !tag;

            item = !hasKeys && expr.key ? mkitem(expr, item, i) : item;

            // new tag
            if (mustCreate) {
              tag = new Tag$1(impl, {
                parent: parent,
                isLoop: isLoop,
                isAnonymous: isAnonymous,
                tagName: tagName,
                root: dom.cloneNode(isAnonymous),
                item: item,
                index: i,
              }, dom.innerHTML);

              // mount the tag
              tag.mount();

              if (mustAppend) { append.apply(tag, [frag || root, isVirtual]); }
              else { insert.apply(tag, [root, tags[i], isVirtual]); }

              if (!mustAppend) { oldItems.splice(i, 0, item); }
              tags.splice(i, 0, tag);
              if (child) { arrayishAdd(parent.tags, tagName, tag, true); }
            } else if (pos !== i && doReorder) {
              // move
              if (contains(items, oldItems[pos])) {
                move.apply(tag, [root, tags[i], isVirtual]);
                // move the old tag instance
                tags.splice(i, 0, tags.splice(pos, 1)[0]);
                // move the old item
                oldItems.splice(i, 0, oldItems.splice(pos, 1)[0]);
              }

              // update the position attribute if it exists
              if (expr.pos) { tag[expr.pos] = i; }

              // if the loop tags are not custom
              // we need to move all their custom tags into the right position
              if (!child && tag.tags) { moveNestedTags.call(tag, i); }
            }

            // cache the original item to use it in the events bound to this node
            // and its children
            tag.__.item = item;
            tag.__.index = i;
            tag.__.parent = parent;

            if (!mustCreate) { tag.update(item); }
          });

          // remove the redundant tags
          unmountRedundant(items, tags);

          // clone the items array
          oldItems = items.slice();

          // this condition is weird u
          root.insertBefore(frag, placeholder);
        };

        expr.unmount = function () {
          each(tags, function (t) {
            t.unmount();
          });
        };

        return expr
      }

      /**
       * Walk the tag DOM to detect the expressions to evaluate
       * @this Tag
       * @param   { HTMLElement } root - root tag where we will start digging the expressions
       * @param   { Array } expressions - empty array where the expressions will be added
       * @param   { Boolean } mustIncludeRoot - flag to decide whether the root must be parsed as well
       * @returns { Object } an object containing the root noode and the dom tree
       */
      function parseExpressions(root, expressions, mustIncludeRoot) {
        var this$1 = this;

        var tree = {parent: {children: expressions}};

        walkNodes(root, function (dom, ctx) {
          var type = dom.nodeType, parent = ctx.parent, attr, expr, tagImpl;
          if (!mustIncludeRoot && dom === root) { return {parent: parent} }

          // text node
          if (type === 3 && dom.parentNode.tagName !== 'STYLE' && tmpl.hasExpr(dom.nodeValue)) {
            parent.children.push({
              dom: dom,
              expr: dom.nodeValue
            });
          }

          if (type !== 1) { return ctx } // not an element

          var isVirtual = dom.tagName === 'VIRTUAL';

          // loop. each does it's own thing (for now)
          if (attr = getAttr(dom, LOOP_DIRECTIVE)) {
            if (isVirtual) { setAttr(dom, 'loopVirtual', true); } // ignore here, handled in _each
            parent.children.push(_each(dom, this$1, attr));
            return false
          }

          // if-attrs become the new parent. Any following expressions (either on the current
          // element, or below it) become children of this expression.
          if (attr = getAttr(dom, CONDITIONAL_DIRECTIVE)) {
            parent.children.push(Object.create(IfExpr).init(dom, this$1, attr));
            return false
          }

          if (expr = getAttr(dom, IS_DIRECTIVE)) {
            if (tmpl.hasExpr(expr)) {
              parent.children.push({isRtag: true, expr: expr, dom: dom, attrs: [].slice.call(dom.attributes)});
              return false
            }
          }

          // if this is a tag, stop traversing here.
          // we ignore the root, since parseExpressions is called while we're mounting that root
          tagImpl = getTag(dom);
          if (isVirtual) {
            if (getAttr(dom, 'virtualized')) {dom.parentElement.removeChild(dom); } // tag created, remove from dom
            if (!tagImpl && !getAttr(dom, 'virtualized') && !getAttr(dom, 'loopVirtual'))  // ok to create virtual tag
            { tagImpl = {tmpl: dom.outerHTML}; }
          }

          if (tagImpl && (dom !== root || mustIncludeRoot)) {
            if (isVirtual && !getAttr(dom, IS_DIRECTIVE)) { // handled in update
              // can not remove attribute like directives
              // so flag for removal after creation to prevent maximum stack error
              setAttr(dom, 'virtualized', true);

              var tag = new Tag$1({tmpl: dom.outerHTML},
                {root: dom, parent: this$1},
                dom.innerHTML);
              parent.children.push(tag); // no return, anonymous tag, keep parsing
            } else {
              var conf = {root: dom, parent: this$1, hasImpl: true};
              parent.children.push(initChildTag(tagImpl, conf, dom.innerHTML, this$1));
              return false
            }
          }

          // attribute expressions
          parseAttributes.apply(this$1, [dom, dom.attributes, function (attr, expr) {
            if (!expr) { return }
            parent.children.push(expr);
          }]);

          // whatever the parent is, all child elements get the same parent.
          // If this element had an if-attr, that's the parent for all child elements
          return {parent: parent}
        }, tree);
      }

      /**
       * Calls `fn` for every attribute on an element. If that attr has an expression,
       * it is also passed to fn.
       * @this Tag
       * @param   { HTMLElement } dom - dom node to parse
       * @param   { Array } attrs - array of attributes
       * @param   { Function } fn - callback to exec on any iteration
       */
      function parseAttributes(dom, attrs, fn) {
        var this$1 = this;

        each(attrs, function (attr) {
          if (!attr) { return false }

          var name = attr.name, bool = isBoolAttr(name), expr;

          if (contains(REF_DIRECTIVES, name)) {
            expr = Object.create(RefExpr).init(dom, this$1, name, attr.value);
          } else if (tmpl.hasExpr(attr.value)) {
            expr = {dom: dom, expr: attr.value, attr: name, bool: bool};
          }

          fn(attr, expr);
        });
      }

      /*
  Includes hacks needed for the Internet Explorer version 9 and below
  See: http://kangax.github.io/compat-table/es5/#ie8
       http://codeplanet.io/dropping-ie8/
*/

      var reHasYield = /<yield\b/i;
      var reYieldAll = /<yield\s*(?:\/>|>([\S\s]*?)<\/yield\s*>|>)/ig;
      var reYieldSrc = /<yield\s+to=['"]([^'">]*)['"]\s*>([\S\s]*?)<\/yield\s*>/ig;
      var reYieldDest = /<yield\s+from=['"]?([-\w]+)['"]?\s*(?:\/>|>([\S\s]*?)<\/yield\s*>)/ig;
      var rootEls = {tr: 'tbody', th: 'tr', td: 'tr', col: 'colgroup'};
      var tblTags = IE_VERSION && IE_VERSION < 10 ? RE_SPECIAL_TAGS : RE_SPECIAL_TAGS_NO_OPTION;
      var GENERIC = 'div';
      var SVG = 'svg';

      /*
  Creates the root element for table or select child elements:
  tr/th/td/thead/tfoot/tbody/caption/col/colgroup/option/optgroup
*/
      function specialTags(el, tmpl, tagName) {

        var
          select = tagName[0] === 'o',
          parent = select ? 'select>' : 'table>';

        // trim() is important here, this ensures we don't have artifacts,
        // so we can check if we have only one element inside the parent
        el.innerHTML = '<' + parent + tmpl.trim() + '</' + parent;
        parent = el.firstChild;

        // returns the immediate parent if tr/th/td/col is the only element, if not
        // returns the whole tree, as this can include additional elements
        /* istanbul ignore next */
        if (select) {
          parent.selectedIndex = -1;  // for IE9, compatible w/current riot behavior
        } else {
          // avoids insertion of cointainer inside container (ex: tbody inside tbody)
          var tname = rootEls[tagName];
          if (tname && parent.childElementCount === 1) { parent = $(tname, parent); }
        }
        return parent
      }

      /*
  Replace the yield tag from any tag template with the innerHTML of the
  original tag in the page
*/
      function replaceYield(tmpl, html) {
        // do nothing if no yield
        if (!reHasYield.test(tmpl)) { return tmpl }

        // be careful with #1343 - string on the source having `$1`
        var src = {};

        html = html && html.replace(reYieldSrc, function (_, ref, text) {
          src[ref] = src[ref] || text;   // preserve first definition
          return ''
        }).trim();

        return tmpl
          .replace(reYieldDest, function (_, ref, def) {  // yield with from - to attrs
            return src[ref] || def || ''
          })
          .replace(reYieldAll, function (_, def) {        // yield without any "from"
            return html || def || ''
          })
      }

      /**
       * Creates a DOM element to wrap the given content. Normally an `DIV`, but can be
       * also a `TABLE`, `SELECT`, `TBODY`, `TR`, or `COLGROUP` element.
       *
       * @param   { String } tmpl  - The template coming from the custom tag definition
       * @param   { String } html - HTML content that comes from the DOM element where you
       *           will mount the tag, mostly the original tag in the page
       * @param   { Boolean } isSvg - true if the root node is an svg
       * @returns { HTMLElement } DOM element with _tmpl_ merged through `YIELD` with the _html_.
       */
      function mkdom(tmpl, html, isSvg$$1) {
        var match = tmpl && tmpl.match(/^\s*<([-\w]+)/),
          tagName = match && match[1].toLowerCase(),
          el = mkEl(isSvg$$1 ? SVG : GENERIC);

        // replace all the yield tags with the tag inner html
        tmpl = replaceYield(tmpl, html);

        /* istanbul ignore next */
        if (tblTags.test(tagName)) { el = specialTags(el, tmpl, tagName); }
        else { setInnerHTML(el, tmpl); }

        return el
      }

      /**
       * Another way to create a riot tag a bit more es6 friendly
       * @param { HTMLElement } el - tag DOM selector or DOM node/s
       * @param { Object } opts - tag logic
       * @returns { Tag } new riot tag instance
       */
      function Tag$2(el, opts) {
        // get the tag properties from the class constructor
        var ref = this;
        var name = ref.name;
        var tmpl = ref.tmpl;
        var css = ref.css;
        var attrs = ref.attrs;
        var onCreate = ref.onCreate;
        // register a new tag and cache the class prototype
        if (!__TAG_IMPL[name]) {
          tag$1(name, tmpl, css, attrs, onCreate);
          // cache the class constructor
          __TAG_IMPL[name].class = this.constructor;
        }

        // mount the tag using the class instance
        mountTo(el, name, opts, this);
        // inject the component css
        if (css) { styleManager.inject(); }

        return this
      }

      /**
       * Create a new riot tag implementation
       * @param   { String }   name - name/id of the new riot tag
       * @param   { String }   tmpl - tag template
       * @param   { String }   css - custom tag css
       * @param   { String }   attrs - root tag attributes
       * @param   { Function } fn - user function
       * @returns { String } name/id of the tag just created
       */
      function tag$1(name, tmpl, css, attrs, fn) {
        if (isFunction(attrs)) {
          fn = attrs;

          if (/^[\w\-]+\s?=/.test(css)) {
            attrs = css;
            css = '';
          } else { attrs = ''; }
        }

        if (css) {
          if (isFunction(css)) { fn = css; }
          else { styleManager.add(css); }
        }

        name = name.toLowerCase();
        __TAG_IMPL[name] = {name: name, tmpl: tmpl, attrs: attrs, fn: fn};

        return name
      }

      /**
       * Create a new riot tag implementation (for use by the compiler)
       * @param   { String }   name - name/id of the new riot tag
       * @param   { String }   tmpl - tag template
       * @param   { String }   css - custom tag css
       * @param   { String }   attrs - root tag attributes
       * @param   { Function } fn - user function
       * @returns { String } name/id of the tag just created
       */
      function tag2$1(name, tmpl, css, attrs, fn) {
        if (css) { styleManager.add(css, name); }

        __TAG_IMPL[name] = {name: name, tmpl: tmpl, attrs: attrs, fn: fn};

        return name
      }

      /**
       * Mount a tag using a specific tag implementation
       * @param   { * } selector - tag DOM selector or DOM node/s
       * @param   { String } tagName - tag implementation name
       * @param   { Object } opts - tag logic
       * @returns { Array } new tags instances
       */
      function mount$1(selector, tagName, opts) {
        var tags = [];
        var elem, allTags;

        function pushTagsTo(root) {
          if (root.tagName) {
            var riotTag = getAttr(root, IS_DIRECTIVE), tag;

            // have tagName? force riot-tag to be the same
            if (tagName && riotTag !== tagName) {
              riotTag = tagName;
              setAttr(root, IS_DIRECTIVE, tagName);
            }

            tag = mountTo(root, riotTag || root.tagName.toLowerCase(), opts);

            if (tag) { tags.push(tag); }
          } else if (root.length) { each(root, pushTagsTo); } // assume nodeList
        }

        // inject styles into DOM
        styleManager.inject();

        if (isObject(tagName)) {
          opts = tagName;
          tagName = 0;
        }

        // crawl the DOM to find the tag
        if (isString(selector)) {
          selector = selector === '*' ?
            // select all registered tags
            // & tags found with the riot-tag attribute set
            allTags = selectTags() :
            // or just the ones named like the selector
            selector + selectTags(selector.split(/, */));

          // make sure to pass always a selector
          // to the querySelectorAll function
          elem = selector ? $$(selector) : [];
        }
        else
        // probably you have passed already a tag or a NodeList
        { elem = selector; }

        // select all the registered and mount them inside their root elements
        if (tagName === '*') {
          // get all custom tags
          tagName = allTags || selectTags();
          // if the root els it's just a single tag
          if (elem.tagName) { elem = $$(tagName, elem); }
          else {
            // select all the children for all the different root elements
            var nodeList = [];

            each(elem, function (_el) {
              return nodeList.push($$(tagName, _el));
            });

            elem = nodeList;
          }
          // get rid of the tagName
          tagName = 0;
        }

        pushTagsTo(elem);

        return tags
      }

// Create a mixin that could be globally shared across all the tags
      var mixins = {};
      var globals = mixins[GLOBAL_MIXIN] = {};
      var mixins_id = 0;

      /**
       * Create/Return a mixin by its name
       * @param   { String }  name - mixin name (global mixin if object)
       * @param   { Object }  mix - mixin logic
       * @param   { Boolean } g - is global?
       * @returns { Object }  the mixin logic
       */
      function mixin$1(name, mix, g) {
        // Unnamed global
        if (isObject(name)) {
          mixin$1(("__" + (mixins_id++) + "__"), name, true);
          return
        }

        var store = g ? globals : mixins;

        // Getter
        if (!mix) {
          if (isUndefined(store[name])) { throw new Error(("Unregistered mixin: " + name)) }

          return store[name]
        }

        // Setter
        store[name] = isFunction(mix) ?
          extend(mix.prototype, store[name] || {}) && mix :
          extend(store[name] || {}, mix);
      }

      /**
       * Update all the tags instances created
       * @returns { Array } all the tags instances
       */
      function update$1() {
        return each(__TAGS_CACHE, function (tag) {
          return tag.update();
        })
      }

      function unregister$1(name) {
        __TAG_IMPL[name] = null;
      }

      var version$1 = 'v3.6.1';

      var core = Object.freeze({
        Tag: Tag$2,
        tag: tag$1,
        tag2: tag2$1,
        mount: mount$1,
        mixin: mixin$1,
        update: update$1,
        unregister: unregister$1,
        version: version$1
      });

// counter to give a unique id to all the Tag instances
      var __uid = 0;

      /**
       * We need to update opts for this tag. That requires updating the expressions
       * in any attributes on the tag, and then copying the result onto opts.
       * @this Tag
       * @param   {Boolean} isLoop - is it a loop tag?
       * @param   { Tag }  parent - parent tag node
       * @param   { Boolean }  isAnonymous - is it a tag without any impl? (a tag not registered)
       * @param   { Object }  opts - tag options
       * @param   { Array }  instAttrs - tag attributes array
       */
      function updateOpts(isLoop, parent, isAnonymous, opts, instAttrs) {
        // isAnonymous `each` tags treat `dom` and `root` differently. In this case
        // (and only this case) we don't need to do updateOpts, because the regular parse
        // will update those attrs. Plus, isAnonymous tags don't need opts anyway
        if (isLoop && isAnonymous) { return }

        var ctx = !isAnonymous && isLoop ? this : parent || this;
        each(instAttrs, function (attr) {
          if (attr.expr) { updateAllExpressions.call(ctx, [attr.expr]); }
          // normalize the attribute names
          opts[toCamel(attr.name).replace(ATTRS_PREFIX, '')] = attr.expr ? attr.expr.value : attr.value;
        });
      }

      /**
       * Tag class
       * @constructor
       * @param { Object } impl - it contains the tag template, and logic
       * @param { Object } conf - tag options
       * @param { String } innerHTML - html that eventually we need to inject in the tag
       */
      function Tag$1(impl, conf, innerHTML) {
        if (impl === void 0) impl = {};
        if (conf === void 0) conf = {};

        var opts = extend({}, conf.opts),
          parent = conf.parent,
          isLoop = conf.isLoop,
          isAnonymous = !!conf.isAnonymous,
          skipAnonymous = settings$1.skipAnonymousTags && isAnonymous,
          item = cleanUpData(conf.item),
          index = conf.index, // available only for the looped nodes
          instAttrs = [], // All attributes on the Tag when it's first parsed
          implAttrs = [], // expressions on this type of Tag
          expressions = [],
          root = conf.root,
          tagName = conf.tagName || getTagName(root),
          isVirtual = tagName === 'virtual',
          isInline = !isVirtual && !impl.tmpl,
          propsInSyncWithParent = [],
          dom;

        // make this tag observable
        if (!skipAnonymous) { observable$1(this); }
        // only call unmount if we have a valid __TAG_IMPL (has name property)
        if (impl.name && root._tag) { root._tag.unmount(true); }

        // not yet mounted
        this.isMounted = false;

        defineProperty(this, '__', {
          isAnonymous: isAnonymous,
          instAttrs: instAttrs,
          innerHTML: innerHTML,
          tagName: tagName,
          index: index,
          isLoop: isLoop,
          isInline: isInline,
          // tags having event listeners
          // it would be better to use weak maps here but we can not introduce breaking changes now
          listeners: [],
          // these vars will be needed only for the virtual tags
          virts: [],
          tail: null,
          head: null,
          parent: null,
          item: null
        });

        // create a unique id to this tag
        // it could be handy to use it also to improve the virtual dom rendering speed
        defineProperty(this, '_riot_id', ++__uid); // base 1 allows test !t._riot_id
        defineProperty(this, 'root', root);
        extend(this, {opts: opts}, item);
        // protect the "tags" and "refs" property from being overridden
        defineProperty(this, 'parent', parent || null);
        defineProperty(this, 'tags', {});
        defineProperty(this, 'refs', {});

        if (isInline || isLoop && isAnonymous) {
          dom = root;
        } else {
          if (!isVirtual) { root.innerHTML = ''; }
          dom = mkdom(impl.tmpl, innerHTML, isSvg(root));
        }

        /**
         * Update the tag expressions and options
         * @param   { * }  data - data we want to use to extend the tag properties
         * @returns { Tag } the current tag instance
         */
        defineProperty(this, 'update', function tagUpdate(data) {
          var nextOpts = {},
            canTrigger = this.isMounted && !skipAnonymous;

          // make sure the data passed will not override
          // the component core methods
          data = cleanUpData(data);
          extend(this, data);
          updateOpts.apply(this, [isLoop, parent, isAnonymous, nextOpts, instAttrs]);

          if (canTrigger && this.isMounted && isFunction(this.shouldUpdate) && !this.shouldUpdate(data, nextOpts)) {
            return this
          }

          // inherit properties from the parent, but only for isAnonymous tags
          if (isLoop && isAnonymous) { inheritFrom.apply(this, [this.parent, propsInSyncWithParent]); }
          extend(opts, nextOpts);
          if (canTrigger) { this.trigger('update', data); }
          updateAllExpressions.call(this, expressions);
          if (canTrigger) { this.trigger('updated'); }

          return this

        }.bind(this));

        /**
         * Add a mixin to this tag
         * @returns { Tag } the current tag instance
         */
        defineProperty(this, 'mixin', function tagMixin() {
          var this$1 = this;

          each(arguments, function (mix) {
            var instance, obj;
            var props = [];

            // properties blacklisted and will not be bound to the tag instance
            var propsBlacklist = ['init', '__proto__'];

            mix = isString(mix) ? mixin$1(mix) : mix;

            // check if the mixin is a function
            if (isFunction(mix)) {
              // create the new mixin instance
              instance = new mix();
            } else { instance = mix; }

            var proto = Object.getPrototypeOf(instance);

            // build multilevel prototype inheritance chain property list
            do { props = props.concat(Object.getOwnPropertyNames(obj || instance)); }
            while (obj = Object.getPrototypeOf(obj || instance));

            // loop the keys in the function prototype or the all object keys
            each(props, function (key) {
              // bind methods to this
              // allow mixins to override other properties/parent mixins
              if (!contains(propsBlacklist, key)) {
                // check for getters/setters
                var descriptor = Object.getOwnPropertyDescriptor(instance, key) || Object.getOwnPropertyDescriptor(proto, key);
                var hasGetterSetter = descriptor && (descriptor.get || descriptor.set);

                // apply method only if it does not already exist on the instance
                if (!this$1.hasOwnProperty(key) && hasGetterSetter) {
                  Object.defineProperty(this$1, key, descriptor);
                } else {
                  this$1[key] = isFunction(instance[key]) ?
                    instance[key].bind(this$1) :
                    instance[key];
                }
              }
            });

            // init method will be called automatically
            if (instance.init) { instance.init.bind(this$1)(); }
          });
          return this
        }.bind(this));

        /**
         * Mount the current tag instance
         * @returns { Tag } the current tag instance
         */
        defineProperty(this, 'mount', function tagMount() {
          var this$1 = this;

          root._tag = this; // keep a reference to the tag just created

          // Read all the attrs on this instance. This give us the info we need for updateOpts
          parseAttributes.apply(parent, [root, root.attributes, function (attr, expr) {
            if (!isAnonymous && RefExpr.isPrototypeOf(expr)) { expr.tag = this$1; }
            attr.expr = expr;
            instAttrs.push(attr);
          }]);

          // update the root adding custom attributes coming from the compiler
          implAttrs = [];
          walkAttrs(impl.attrs, function (k, v) {
            implAttrs.push({name: k, value: v});
          });
          parseAttributes.apply(this, [root, implAttrs, function (attr, expr) {
            if (expr) { expressions.push(expr); }
            else { setAttr(root, attr.name, attr.value); }
          }]);

          // initialiation
          updateOpts.apply(this, [isLoop, parent, isAnonymous, opts, instAttrs]);

          // add global mixins
          var globalMixin = mixin$1(GLOBAL_MIXIN);

          if (globalMixin && !skipAnonymous) {
            for (var i in globalMixin) {
              if (globalMixin.hasOwnProperty(i)) {
                this$1.mixin(globalMixin[i]);
              }
            }
          }

          if (impl.fn) { impl.fn.call(this, opts); }

          if (!skipAnonymous) { this.trigger('before-mount'); }

          // parse layout after init. fn may calculate args for nested custom tags
          parseExpressions.apply(this, [dom, expressions, isAnonymous]);

          this.update(item);

          if (!isAnonymous && !isInline) {
            while (dom.firstChild) { root.appendChild(dom.firstChild); }
          }

          defineProperty(this, 'root', root);
          defineProperty(this, 'isMounted', true);

          if (skipAnonymous) { return }

          // if it's not a child tag we can trigger its mount event
          if (!this.parent) {
            this.trigger('mount');
          }
          // otherwise we need to wait that the parent "mount" or "updated" event gets triggered
          else {
            var p = getImmediateCustomParentTag(this.parent);
            p.one(!p.isMounted ? 'mount' : 'updated', function () {
              this$1.trigger('mount');
            });
          }

          return this

        }.bind(this));

        /**
         * Unmount the tag instance
         * @param { Boolean } mustKeepRoot - if it's true the root node will not be removed
         * @returns { Tag } the current tag instance
         */
        defineProperty(this, 'unmount', function tagUnmount(mustKeepRoot) {
          var this$1 = this;

          var el = this.root,
            p = el.parentNode,
            ptag,
            tagIndex = __TAGS_CACHE.indexOf(this);

          if (!skipAnonymous) { this.trigger('before-unmount'); }

          // clear all attributes coming from the mounted tag
          walkAttrs(impl.attrs, function (name) {
            if (startsWith(name, ATTRS_PREFIX)) { name = name.slice(ATTRS_PREFIX.length); }

            remAttr(root, name);
          });

          // remove all the event listeners
          this.__.listeners.forEach(function (dom) {
            Object.keys(dom[RIOT_EVENTS_KEY]).forEach(function (eventName) {
              dom.removeEventListener(eventName, dom[RIOT_EVENTS_KEY][eventName]);
            });
          });

          // remove this tag instance from the global virtualDom variable
          if (tagIndex !== -1) { __TAGS_CACHE.splice(tagIndex, 1); }

          if (p || isVirtual) {
            if (parent) {
              ptag = getImmediateCustomParentTag(parent);

              if (isVirtual) {
                Object.keys(this.tags).forEach(function (tagName) {
                  arrayishRemove(ptag.tags, tagName, this$1.tags[tagName]);
                });
              } else {
                arrayishRemove(ptag.tags, tagName, this);
                // remove from _parent too
                if (parent !== ptag) {
                  arrayishRemove(parent.tags, tagName, this);
                }
              }
            } else {
              // remove the tag contents
              setInnerHTML(el, '');
            }

            if (p && !mustKeepRoot) { p.removeChild(el); }
          }

          if (this.__.virts) {
            each(this.__.virts, function (v) {
              if (v.parentNode) { v.parentNode.removeChild(v); }
            });
          }

          // allow expressions to unmount themselves
          unmountAll(expressions);
          each(instAttrs, function (a) {
            return a.expr && a.expr.unmount && a.expr.unmount();
          });

          // custom internal unmount function to avoid relying on the observable
          if (this.__.onUnmount) { this.__.onUnmount(); }

          if (!skipAnonymous) {
            this.trigger('unmount');
            this.off('*');
          }

          defineProperty(this, 'isMounted', false);

          delete this.root._tag;

          return this

        }.bind(this));
      }

      /**
       * Detect the tag implementation by a DOM node
       * @param   { Object } dom - DOM node we need to parse to get its tag implementation
       * @returns { Object } it returns an object containing the implementation of a custom tag (template and boot function)
       */
      function getTag(dom) {
        return dom.tagName && __TAG_IMPL[getAttr(dom, IS_DIRECTIVE) ||
        getAttr(dom, IS_DIRECTIVE) || dom.tagName.toLowerCase()]
      }

      /**
       * Inherit properties from a target tag instance
       * @this Tag
       * @param   { Tag } target - tag where we will inherit properties
       * @param   { Array } propsInSyncWithParent - array of properties to sync with the target
       */
      function inheritFrom(target, propsInSyncWithParent) {
        var this$1 = this;

        each(Object.keys(target), function (k) {
          // some properties must be always in sync with the parent tag
          var mustSync = !isReservedName(k) && contains(propsInSyncWithParent, k);

          if (isUndefined(this$1[k]) || mustSync) {
            // track the property to keep in sync
            // so we can keep it updated
            if (!mustSync) { propsInSyncWithParent.push(k); }
            this$1[k] = target[k];
          }
        });
      }

      /**
       * Move the position of a custom tag in its parent tag
       * @this Tag
       * @param   { String } tagName - key where the tag was stored
       * @param   { Number } newPos - index where the new tag will be stored
       */
      function moveChildTag(tagName, newPos) {
        var parent = this.parent,
          tags;
        // no parent no move
        if (!parent) { return }

        tags = parent.tags[tagName];

        if (isArray(tags)) { tags.splice(newPos, 0, tags.splice(tags.indexOf(this), 1)[0]); }
        else { arrayishAdd(parent.tags, tagName, this); }
      }

      /**
       * Create a new child tag including it correctly into its parent
       * @param   { Object } child - child tag implementation
       * @param   { Object } opts - tag options containing the DOM node where the tag will be mounted
       * @param   { String } innerHTML - inner html of the child node
       * @param   { Object } parent - instance of the parent tag including the child custom tag
       * @returns { Object } instance of the new child tag just created
       */
      function initChildTag(child, opts, innerHTML, parent) {
        var tag = new Tag$1(child, opts, innerHTML),
          tagName = opts.tagName || getTagName(opts.root, true),
          ptag = getImmediateCustomParentTag(parent);
        // fix for the parent attribute in the looped elements
        defineProperty(tag, 'parent', ptag);
        // store the real parent tag
        // in some cases this could be different from the custom parent tag
        // for example in nested loops
        tag.__.parent = parent;

        // add this tag to the custom parent tag
        arrayishAdd(ptag.tags, tagName, tag);

        // and also to the real parent tag
        if (ptag !== parent) { arrayishAdd(parent.tags, tagName, tag); }

        return tag
      }

      /**
       * Loop backward all the parents tree to detect the first custom parent tag
       * @param   { Object } tag - a Tag instance
       * @returns { Object } the instance of the first custom parent tag found
       */
      function getImmediateCustomParentTag(tag) {
        var ptag = tag;
        while (ptag.__.isAnonymous) {
          if (!ptag.parent) { break }
          ptag = ptag.parent;
        }
        return ptag
      }

      /**
       * Trigger the unmount method on all the expressions
       * @param   { Array } expressions - DOM expressions
       */
      function unmountAll(expressions) {
        each(expressions, function (expr) {
          if (expr instanceof Tag$1) { expr.unmount(true); }
          else if (expr.tagName) { expr.tag.unmount(true); }
          else if (expr.unmount) { expr.unmount(); }
        });
      }

      /**
       * Get the tag name of any DOM node
       * @param   { Object } dom - DOM node we want to parse
       * @param   { Boolean } skipDataIs - hack to ignore the data-is attribute when attaching to parent
       * @returns { String } name to identify this dom node in riot
       */
      function getTagName(dom, skipDataIs) {
        var child = getTag(dom),
          namedTag = !skipDataIs && getAttr(dom, IS_DIRECTIVE);
        return namedTag && !tmpl.hasExpr(namedTag) ?
          namedTag :
          child ? child.name : dom.tagName.toLowerCase()
      }

      /**
       * With this function we avoid that the internal Tag methods get overridden
       * @param   { Object } data - options we want to use to extend the tag instance
       * @returns { Object } clean object without containing the riot internal reserved words
       */
      function cleanUpData(data) {
        if (!(data instanceof Tag$1) && !(data && isFunction(data.trigger))) { return data }

        var o = {};
        for (var key in data) {
          if (!RE_RESERVED_NAMES.test(key)) { o[key] = data[key]; }
        }
        return o
      }

      /**
       * Set the property of an object for a given key. If something already
       * exists there, then it becomes an array containing both the old and new value.
       * @param { Object } obj - object on which to set the property
       * @param { String } key - property name
       * @param { Object } value - the value of the property to be set
       * @param { Boolean } ensureArray - ensure that the property remains an array
       * @param { Number } index - add the new item in a certain array position
       */
      function arrayishAdd(obj, key, value, ensureArray, index) {
        var dest = obj[key];
        var isArr = isArray(dest);
        var hasIndex = !isUndefined(index);

        if (dest && dest === value) { return }

        // if the key was never set, set it once
        if (!dest && ensureArray) { obj[key] = [value]; }
        else if (!dest) { obj[key] = value; }
        // if it was an array and not yet set
        else {
          if (isArr) {
            var oldIndex = dest.indexOf(value);
            // this item never changed its position
            if (oldIndex === index) { return }
            // remove the item from its old position
            if (oldIndex !== -1) { dest.splice(oldIndex, 1); }
            // move or add the item
            if (hasIndex) {
              dest.splice(index, 0, value);
            } else {
              dest.push(value);
            }
          } else { obj[key] = [dest, value]; }
        }
      }

      /**
       * Removes an item from an object at a given key. If the key points to an array,
       * then the item is just removed from the array.
       * @param { Object } obj - object on which to remove the property
       * @param { String } key - property name
       * @param { Object } value - the value of the property to be removed
       * @param { Boolean } ensureArray - ensure that the property remains an array
       */
      function arrayishRemove(obj, key, value, ensureArray) {
        if (isArray(obj[key])) {
          var index = obj[key].indexOf(value);
          if (index !== -1) { obj[key].splice(index, 1); }
          if (!obj[key].length) { delete obj[key]; }
          else if (obj[key].length === 1 && !ensureArray) { obj[key] = obj[key][0]; }
        } else { delete obj[key]; } // otherwise just delete the key
      }

      /**
       * Mount a tag creating new Tag instance
       * @param   { Object } root - dom node where the tag will be mounted
       * @param   { String } tagName - name of the riot tag we want to mount
       * @param   { Object } opts - options to pass to the Tag instance
       * @param   { Object } ctx - optional context that will be used to extend an existing class ( used in riot.Tag )
       * @returns { Tag } a new Tag instance
       */
      function mountTo(root, tagName, opts, ctx) {
        var impl = __TAG_IMPL[tagName],
          implClass = __TAG_IMPL[tagName].class,
          tag = ctx || (implClass ? Object.create(implClass.prototype) : {}),
          // cache the inner HTML to fix #855
          innerHTML = root._innerHTML = root._innerHTML || root.innerHTML;

        var conf = extend({root: root, opts: opts}, {parent: opts ? opts.parent : null});

        if (impl && root) { Tag$1.apply(tag, [impl, conf, innerHTML]); }

        if (tag && tag.mount) {
          tag.mount(true);
          // add this tag to the virtualDom variable
          if (!contains(__TAGS_CACHE, tag)) { __TAGS_CACHE.push(tag); }
        }

        return tag
      }

      /**
       * makes a tag virtual and replaces a reference in the dom
       * @this Tag
       * @param { tag } the tag to make virtual
       * @param { ref } the dom reference location
       */
      function makeReplaceVirtual(tag, ref) {
        var frag = createFrag();
        makeVirtual.call(tag, frag);
        ref.parentNode.replaceChild(frag, ref);
      }

      /**
       * Adds the elements for a virtual tag
       * @this Tag
       * @param { Node } src - the node that will do the inserting or appending
       * @param { Tag } target - only if inserting, insert before this tag's first child
       */
      function makeVirtual(src, target) {
        var this$1 = this;

        var head = createDOMPlaceholder(),
          tail = createDOMPlaceholder(),
          frag = createFrag(),
          sib, el;

        this.root.insertBefore(head, this.root.firstChild);
        this.root.appendChild(tail);

        this.__.head = el = head;
        this.__.tail = tail;

        while (el) {
          sib = el.nextSibling;
          frag.appendChild(el);
          this$1.__.virts.push(el); // hold for unmounting
          el = sib;
        }

        if (target) { src.insertBefore(frag, target.__.head); }
        else { src.appendChild(frag); }
      }

      /**
       * Move virtual tag and all child nodes
       * @this Tag
       * @param { Node } src  - the node that will do the inserting
       * @param { Tag } target - insert before this tag's first child
       */
      function moveVirtual(src, target) {
        var this$1 = this;

        var el = this.__.head,
          frag = createFrag(),
          sib;

        while (el) {
          sib = el.nextSibling;
          frag.appendChild(el);
          el = sib;
          if (el === this$1.__.tail) {
            frag.appendChild(el);
            src.insertBefore(frag, target.__.head);
            break
          }
        }
      }

      /**
       * Get selectors for tags
       * @param   { Array } tags - tag names to select
       * @returns { String } selector
       */
      function selectTags(tags) {
        // select all tags
        if (!tags) {
          var keys = Object.keys(__TAG_IMPL);
          return keys + selectTags(keys)
        }

        return tags
          .filter(function (t) {
            return !/[^-\w]/.test(t);
          })
          .reduce(function (list, t) {
            var name = t.trim().toLowerCase();
            return list + ",[" + IS_DIRECTIVE + "=\"" + name + "\"]"
          }, '')
      }

      var tags = Object.freeze({
        getTag: getTag,
        inheritFrom: inheritFrom,
        moveChildTag: moveChildTag,
        initChildTag: initChildTag,
        getImmediateCustomParentTag: getImmediateCustomParentTag,
        unmountAll: unmountAll,
        getTagName: getTagName,
        cleanUpData: cleanUpData,
        arrayishAdd: arrayishAdd,
        arrayishRemove: arrayishRemove,
        mountTo: mountTo,
        makeReplaceVirtual: makeReplaceVirtual,
        makeVirtual: makeVirtual,
        moveVirtual: moveVirtual,
        selectTags: selectTags
      });

      /**
       * Riot public api
       */
      var settings = settings$1;
      var util = {
        tmpl: tmpl,
        brackets: brackets,
        styleManager: styleManager,
        vdom: __TAGS_CACHE,
        styleNode: styleManager.styleNode,
        // export the riot internal utils as well
        dom: dom,
        check: check,
        misc: misc,
        tags: tags
      };

// export the core props/methods
      var Tag$$1 = Tag$2;
      var tag$$1 = tag$1;
      var tag2$$1 = tag2$1;
      var mount$$1 = mount$1;
      var mixin$$1 = mixin$1;
      var update$$1 = update$1;
      var unregister$$1 = unregister$1;
      var version$$1 = version$1;
      var observable = observable$1;

      var riot$1 = extend({}, core, {
        observable: observable$1,
        settings: settings,
        util: util,
      });

      exports.settings = settings;
      exports.util = util;
      exports.Tag = Tag$$1;
      exports.tag = tag$$1;
      exports.tag2 = tag2$$1;
      exports.mount = mount$$1;
      exports.mixin = mixin$$1;
      exports.update = update$$1;
      exports.unregister = unregister$$1;
      exports.version = version$$1;
      exports.observable = observable;
      exports['default'] = riot$1;

      Object.defineProperty(exports, '__esModule', {value: true});

    })));

    /***/
  }),
  /* 1 */
  /***/ (function (module, exports) {

    module.exports = "<div class=\"ame toolbar-button\" title=\"AutoMate\">\n    <automate-button></automate-button>\n</div>\n";

    /***/
  }),
  /* 2 */
  /***/ (function (module, exports) {

    module.exports = "<div class=\"ame\" id=\"wmeAutoMateToolbar\" style=\"display:none; position: fixed; right: 0; width: 250px\">\n    <automate-toolbar></automate-toolbar>\n</div>\n";

    /***/
  }),
  /* 3 */
  /***/ (function (module, exports) {

    /*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
    module.exports = function (useSourceMap) {
      var list = [];

      // return the list of modules as css string
      list.toString = function toString() {
        return this.map(function (item) {
          var content = cssWithMappingToString(item, useSourceMap);
          if (item[2]) {
            return "@media " + item[2] + "{" + content + "}";
          } else {
            return content;
          }
        }).join("");
      };

      // import a list of modules into the list
      list.i = function (modules, mediaQuery) {
        if (typeof modules === "string")
          modules = [[null, modules, ""]];
        var alreadyImportedModules = {};
        for (var i = 0; i < this.length; i++) {
          var id = this[i][0];
          if (typeof id === "number")
            alreadyImportedModules[id] = true;
        }
        for (i = 0; i < modules.length; i++) {
          var item = modules[i];
          // skip already imported module
          // this implementation is not 100% perfect for weird media query combinations
          //  when a module is imported multiple times with different media queries.
          //  I hope this will never occur (Hey this way we have smaller bundles)
          if (typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
            if (mediaQuery && !item[2]) {
              item[2] = mediaQuery;
            } else if (mediaQuery) {
              item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
            }
            list.push(item);
          }
        }
      };
      return list;
    };

    function cssWithMappingToString(item, useSourceMap) {
      var content = item[1] || '';
      var cssMapping = item[3];
      if (!cssMapping) {
        return content;
      }

      if (useSourceMap && typeof btoa === 'function') {
        var sourceMapping = toComment(cssMapping);
        var sourceURLs = cssMapping.sources.map(function (source) {
          return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
        });

        return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
      }

      return [content].join('\n');
    }

// Adapted from convert-source-map (MIT)
    function toComment(sourceMap) {
      // eslint-disable-next-line no-undef
      var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
      var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

      return '/*# ' + data + ' */';
    }

    /***/
  }),
  /* 4 */
  /***/ (function (module, exports, __webpack_require__) {

    /*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

    var stylesInDom = {};

    var memoize = function (fn) {
      var memo;

      return function () {
        if (typeof memo === "undefined") memo = fn.apply(this, arguments);
        return memo;
      };
    };

    var isOldIE = memoize(function () {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      return window && document && document.all && !window.atob;
    });

    var getElement = (function (fn) {
      var memo = {};

      return function (selector) {
        if (typeof memo[selector] === "undefined") {
          memo[selector] = fn.call(this, selector);
        }

        return memo[selector]
      };
    })(function (target) {
      return document.querySelector(target)
    });

    var singleton = null;
    var singletonCounter = 0;
    var stylesInsertedAtTop = [];

    var fixUrls = __webpack_require__(16);

    module.exports = function (list, options) {
      if (typeof DEBUG !== "undefined" && DEBUG) {
        if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
      }

      options = options || {};

      options.attrs = typeof options.attrs === "object" ? options.attrs : {};

      // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
      // tags it will allow on a page
      if (!options.singleton) options.singleton = isOldIE();

      // By default, add <style> tags to the <head> element
      if (!options.insertInto) options.insertInto = "head";

      // By default, add <style> tags to the bottom of the target
      if (!options.insertAt) options.insertAt = "bottom";

      var styles = listToStyles(list, options);

      addStylesToDom(styles, options);

      return function update(newList) {
        var mayRemove = [];

        for (var i = 0; i < styles.length; i++) {
          var item = styles[i];
          var domStyle = stylesInDom[item.id];

          domStyle.refs--;
          mayRemove.push(domStyle);
        }

        if (newList) {
          var newStyles = listToStyles(newList, options);
          addStylesToDom(newStyles, options);
        }

        for (var i = 0; i < mayRemove.length; i++) {
          var domStyle = mayRemove[i];

          if (domStyle.refs === 0) {
            for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

            delete stylesInDom[domStyle.id];
          }
        }
      };
    };

    function addStylesToDom(styles, options) {
      for (var i = 0; i < styles.length; i++) {
        var item = styles[i];
        var domStyle = stylesInDom[item.id];

        if (domStyle) {
          domStyle.refs++;

          for (var j = 0; j < domStyle.parts.length; j++) {
            domStyle.parts[j](item.parts[j]);
          }

          for (; j < item.parts.length; j++) {
            domStyle.parts.push(addStyle(item.parts[j], options));
          }
        } else {
          var parts = [];

          for (var j = 0; j < item.parts.length; j++) {
            parts.push(addStyle(item.parts[j], options));
          }

          stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
        }
      }
    }

    function listToStyles(list, options) {
      var styles = [];
      var newStyles = {};

      for (var i = 0; i < list.length; i++) {
        var item = list[i];
        var id = options.base ? item[0] + options.base : item[0];
        var css = item[1];
        var media = item[2];
        var sourceMap = item[3];
        var part = {css: css, media: media, sourceMap: sourceMap};

        if (!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
        else newStyles[id].parts.push(part);
      }

      return styles;
    }

    function insertStyleElement(options, style) {
      var target = getElement(options.insertInto);

      if (!target) {
        throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
      }

      var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

      if (options.insertAt === "top") {
        if (!lastStyleElementInsertedAtTop) {
          target.insertBefore(style, target.firstChild);
        } else if (lastStyleElementInsertedAtTop.nextSibling) {
          target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
        } else {
          target.appendChild(style);
        }
        stylesInsertedAtTop.push(style);
      } else if (options.insertAt === "bottom") {
        target.appendChild(style);
      } else {
        throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
      }
    }

    function removeStyleElement(style) {
      if (style.parentNode === null) return false;
      style.parentNode.removeChild(style);

      var idx = stylesInsertedAtTop.indexOf(style);
      if (idx >= 0) {
        stylesInsertedAtTop.splice(idx, 1);
      }
    }

    function createStyleElement(options) {
      var style = document.createElement("style");

      options.attrs.type = "text/css";

      addAttrs(style, options.attrs);
      insertStyleElement(options, style);

      return style;
    }

    function createLinkElement(options) {
      var link = document.createElement("link");

      options.attrs.type = "text/css";
      options.attrs.rel = "stylesheet";

      addAttrs(link, options.attrs);
      insertStyleElement(options, link);

      return link;
    }

    function addAttrs(el, attrs) {
      Object.keys(attrs).forEach(function (key) {
        el.setAttribute(key, attrs[key]);
      });
    }

    function addStyle(obj, options) {
      var style, update, remove, result;

      // If a transform function was defined, run it on the css
      if (options.transform && obj.css) {
        result = options.transform(obj.css);

        if (result) {
          // If transform returns a value, use that instead of the original css.
          // This allows running runtime transformations on the css.
          obj.css = result;
        } else {
          // If the transform function returns a falsy value, don't add this css.
          // This allows conditional loading of css
          return function () {
            // noop
          };
        }
      }

      if (options.singleton) {
        var styleIndex = singletonCounter++;

        style = singleton || (singleton = createStyleElement(options));

        update = applyToSingletonTag.bind(null, style, styleIndex, false);
        remove = applyToSingletonTag.bind(null, style, styleIndex, true);

      } else if (
        obj.sourceMap &&
        typeof URL === "function" &&
        typeof URL.createObjectURL === "function" &&
        typeof URL.revokeObjectURL === "function" &&
        typeof Blob === "function" &&
        typeof btoa === "function"
      ) {
        style = createLinkElement(options);
        update = updateLink.bind(null, style, options);
        remove = function () {
          removeStyleElement(style);

          if (style.href) URL.revokeObjectURL(style.href);
        };
      } else {
        style = createStyleElement(options);
        update = applyToTag.bind(null, style);
        remove = function () {
          removeStyleElement(style);
        };
      }

      update(obj);

      return function updateStyle(newObj) {
        if (newObj) {
          if (
            newObj.css === obj.css &&
            newObj.media === obj.media &&
            newObj.sourceMap === obj.sourceMap
          ) {
            return;
          }

          update(obj = newObj);
        } else {
          remove();
        }
      };
    }

    var replaceText = (function () {
      var textStore = [];

      return function (index, replacement) {
        textStore[index] = replacement;

        return textStore.filter(Boolean).join('\n');
      };
    })();

    function applyToSingletonTag(style, index, remove, obj) {
      var css = remove ? "" : obj.css;

      if (style.styleSheet) {
        style.styleSheet.cssText = replaceText(index, css);
      } else {
        var cssNode = document.createTextNode(css);
        var childNodes = style.childNodes;

        if (childNodes[index]) style.removeChild(childNodes[index]);

        if (childNodes.length) {
          style.insertBefore(cssNode, childNodes[index]);
        } else {
          style.appendChild(cssNode);
        }
      }
    }

    function applyToTag(style, obj) {
      var css = obj.css;
      var media = obj.media;

      if (media) {
        style.setAttribute("media", media)
      }

      if (style.styleSheet) {
        style.styleSheet.cssText = css;
      } else {
        while (style.firstChild) {
          style.removeChild(style.firstChild);
        }

        style.appendChild(document.createTextNode(css));
      }
    }

    function updateLink(link, options, obj) {
      var css = obj.css;
      var sourceMap = obj.sourceMap;

      /*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
      var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

      if (options.convertToAbsoluteUrls || autoFixUrls) {
        css = fixUrls(css);
      }

      if (sourceMap) {
        // http://stackoverflow.com/a/26603875
        css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
      }

      var blob = new Blob([css], {type: "text/css"});

      var oldSrc = link.href;

      link.href = URL.createObjectURL(blob);

      if (oldSrc) URL.revokeObjectURL(oldSrc);
    }

    /***/
  }),
  /* 5 */
  /***/ (function (module, exports, __webpack_require__) {

    "use strict";

    var index$2 = function isMergeableObject(value) {
      return isNonNullObject(value) && isNotSpecial(value)
    };

    function isNonNullObject(value) {
      return !!value && typeof value === 'object'
    }

    function isNotSpecial(value) {
      var stringValue = Object.prototype.toString.call(value);

      return stringValue !== '[object RegExp]'
        && stringValue !== '[object Date]'
    }

    function emptyTarget(val) {
      return Array.isArray(val) ? [] : {}
    }

    function cloneIfNecessary(value, optionsArgument) {
      var clone = optionsArgument && optionsArgument.clone === true;
      return (clone && index$2(value)) ? deepmerge(emptyTarget(value), value, optionsArgument) : value
    }

    function defaultArrayMerge(target, source, optionsArgument) {
      var destination = target.slice();
      source.forEach(function (e, i) {
        if (typeof destination[i] === 'undefined') {
          destination[i] = cloneIfNecessary(e, optionsArgument);
        } else if (index$2(e)) {
          destination[i] = deepmerge(target[i], e, optionsArgument);
        } else if (target.indexOf(e) === -1) {
          destination.push(cloneIfNecessary(e, optionsArgument));
        }
      });
      return destination
    }

    function mergeObject(target, source, optionsArgument) {
      var destination = {};
      if (index$2(target)) {
        Object.keys(target).forEach(function (key) {
          destination[key] = cloneIfNecessary(target[key], optionsArgument);
        });
      }
      Object.keys(source).forEach(function (key) {
        if (!index$2(source[key]) || !target[key]) {
          destination[key] = cloneIfNecessary(source[key], optionsArgument);
        } else {
          destination[key] = deepmerge(target[key], source[key], optionsArgument);
        }
      });
      return destination
    }

    function deepmerge(target, source, optionsArgument) {
      var sourceIsArray = Array.isArray(source);
      var targetIsArray = Array.isArray(target);
      var options = optionsArgument || {arrayMerge: defaultArrayMerge};
      var sourceAndTargetTypesMatch = sourceIsArray === targetIsArray;

      if (!sourceAndTargetTypesMatch) {
        return cloneIfNecessary(source, optionsArgument)
      } else if (sourceIsArray) {
        var arrayMerge = options.arrayMerge || defaultArrayMerge;
        return arrayMerge(target, source, optionsArgument)
      } else {
        return mergeObject(target, source, optionsArgument)
      }
    }

    deepmerge.all = function deepmergeAll(array, optionsArgument) {
      if (!Array.isArray(array) || array.length < 2) {
        throw new Error('first argument should be an array with at least two elements')
      }

      // we are sure there are at least 2 values, so it is safe to have no initial value
      return array.reduce(function (prev, next) {
        return deepmerge(prev, next, optionsArgument)
      })
    };

    var index = deepmerge;

    module.exports = index;

    /***/
  }),
  /* 6 */
  /***/ (function (module, exports, __webpack_require__) {


    var riot = __webpack_require__(0);
    riot.tag2('automate-button', '<div class="icon-toolbar" onclick="{toggle}"></div>', '', '', function (opts) {
      this.toggle = (e) => {
        e.preventDefault();
        this.opts.controller.ui.toggleToolbar();
      }
    });

    /***/
  }),
  /* 7 */
  /***/ (function (module, __webpack_exports__, __webpack_require__) {

    "use strict";
    Object.defineProperty(__webpack_exports__, "__esModule", {value: true});
    /* harmony import */
    var __WEBPACK_IMPORTED_MODULE_0__automate_header_tag__ = __webpack_require__(15);
    /* harmony import */
    var __WEBPACK_IMPORTED_MODULE_0__automate_header_tag___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__automate_header_tag__);
    /* harmony import */
    var __WEBPACK_IMPORTED_MODULE_1__automate_content_tag__ = __webpack_require__(13);
    /* harmony import */
    var __WEBPACK_IMPORTED_MODULE_1__automate_content_tag___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__automate_content_tag__);
    /* harmony import */
    var __WEBPACK_IMPORTED_MODULE_2__automate_footer_tag__ = __webpack_require__(14);
    /* harmony import */
    var __WEBPACK_IMPORTED_MODULE_2__automate_footer_tag___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__automate_footer_tag__);

    var riot = __webpack_require__(0);

    riot.tag2('automate-toolbar', '<section class="hero is-fullheight" style="min-height: 94vh;"> <automate-header ctrl="{this.opts.controller}" parent="{this}"></automate-header> <automate-content ctrl="{this.opts.controller}" parent="{this}"></automate-content> <automate-footer ctrl="{this.opts.controller}" parent="{this}"></automate-footer> </section>', '', '', function (opts) {
    });

    /***/
  }),
  /* 8 */
  /***/ (function (module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
    var content = __webpack_require__(11);
    if (typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
    var transform;

    var options = {};
    options.transform = transform;
// add the styles to the DOM
    var update = __webpack_require__(4)(content, options);
    if (content.locals) module.exports = content.locals;
// Hot Module Replacement
    if (false) {
      // When the styles change, update the <style> tags
      if (!content.locals) {
        module.hot.accept("!!../../css-loader/index.js??ref--1-1!../../postcss-loader/lib/index.js??ref--1-2!./bulma.css", function () {
          var newContent = require("!!../../css-loader/index.js??ref--1-1!../../postcss-loader/lib/index.js??ref--1-2!./bulma.css");
          if (typeof newContent === 'string') newContent = [[module.id, newContent, '']];
          update(newContent);
        });
      }
      // When the module is disposed, remove the <style> tags
      module.hot.dispose(function () {
        update();
      });
    }

    /***/
  }),
  /* 9 */
  /***/ (function (module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
    var content = __webpack_require__(12);
    if (typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
    var transform;

    var options = {};
    options.transform = transform;
// add the styles to the DOM
    var update = __webpack_require__(4)(content, options);
    if (content.locals) module.exports = content.locals;
// Hot Module Replacement
    if (false) {
      // When the styles change, update the <style> tags
      if (!content.locals) {
        module.hot.accept("!!../../node_modules/css-loader/index.js??ref--1-1!../../node_modules/postcss-loader/lib/index.js??ref--1-2!./index.css", function () {
          var newContent = require("!!../../node_modules/css-loader/index.js??ref--1-1!../../node_modules/postcss-loader/lib/index.js??ref--1-2!./index.css");
          if (typeof newContent === 'string') newContent = [[module.id, newContent, '']];
          update(newContent);
        });
      }
      // When the module is disposed, remove the <style> tags
      module.hot.dispose(function () {
        update();
      });
    }

    /***/
  }),
  /* 10 */
  /***/ (function (module, exports) {

    module.exports = {
      "$schema": "file:../meta.schema.json",
      "name": "wme-automate",
      "namespace": "https://waze.com",
      "description": "This script automates correction of various names",
      "version": "1.1.4",
      "include": [
        "https://editor-beta.waze.com/*",
        "https://*.waze.com/editor/editor/*",
        "https://*.waze.com/*/editor/*"
      ],
      "match": [
        "https://world.waze.com/editor/*",
        "https://*.waze.com/editor/*",
        "https://*.waze.com/*/editor/*",
        "https://world.waze.com/map-editor/*",
        "https://world.waze.com/beta_editor/*",
        "https://www.waze.com/map-editor/*"
      ],
      "grant": "none",
      "updateURL": "https://gitlab.com/m03geek/wme-automate/raw/master/dist/wme-automate.user.js",
      "downloadURL": "https://gitlab.com/m03geek/wme-automate/raw/master/dist/wme-automate.user.js",
      "supportURL": "https://gitlab.com/m03geek/wme-automate/issues"
    };

    /***/
  }),
  /* 11 */
  /***/ (function (module, exports, __webpack_require__) {

    exports = module.exports = __webpack_require__(3)(undefined);
// imports


// module
    exports.push([module.i, "/*! bulma.io v0.4.2 | MIT License | github.com/jgthms/bulma */\n@-webkit-keyframes spinAround {\n  .ame from {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n  .ame to {\n    -webkit-transform: rotate(359deg);\n            transform: rotate(359deg);\n  }\n}\n@keyframes spinAround {\n  .ame from {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n  .ame to {\n    -webkit-transform: rotate(359deg);\n            transform: rotate(359deg);\n  }\n}\n\n/*! minireset.css v0.0.2 | MIT License | github.com/jgthms/minireset.css */\n.ame .html, .ame .\nbody, .ame p, .ame ol, .ame ul, .ame li, .ame dl, .ame dt, .ame dd, .ame blockquote, .ame figure, .ame fieldset, .ame legend, .ame textarea, .ame pre, .ame iframe, .ame hr, .ame h1, .ame h2, .ame h3, .ame h4, .ame h5, .ame h6 {\n  margin: 0;\n  padding: 0;\n}\n\n.ame h1, .ame h2, .ame h3, .ame h4, .ame h5, .ame h6 {\n  font-size: 100%;\n  font-weight: normal;\n}\n\n.ame ul {\n  list-style: none;\n}\n\n.ame button, .ame input, .ame select, .ame textarea {\n  margin: 0;\n}\n\n.ame .html {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n.ame * {\n  -webkit-box-sizing: inherit;\n          box-sizing: inherit;\n}\n\n.ame *:before, .ame *:after {\n  -webkit-box-sizing: inherit;\n          box-sizing: inherit;\n}\n\n.ame img, .ame embed, .ame object, .ame audio, .ame video {\n  max-width: 100%;\n}\n\niframe {\n  border: 0;\n}\n\n.ame table {\n  border-collapse: collapse;\n  border-spacing: 0;\n}\n\n.ame td, .ame th {\n  padding: 0;\n  text-align: left;\n}\n\n.ame .html {\n  background-color: #fff;\n  font-size: 16px;\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  min-width: 300px;\n  overflow-x: hidden;\n  overflow-y: scroll;\n  text-rendering: optimizeLegibility;\n}\n\n.ame article, .ame aside, .ame figure, .ame footer, .ame header, .ame hgroup, .ame section {\n  display: block;\n}\n\n.ame .body, .ame button, .ame input, .ame select, .ame textarea {\n  font-family: BlinkMacSystemFont, -apple-system, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif;\n}\n\n.ame code, .ame pre {\n  -moz-osx-font-smoothing: auto;\n  -webkit-font-smoothing: auto;\n  font-family: monospace;\n}\n\n.ame .body {\n  color: #4a4a4a;\n  font-size: 1rem;\n  font-weight: 400;\n  line-height: 1.5;\n  overflow-x: hidden;\n}\n\n.ame a {\n  color: #00d1b2;\n  cursor: pointer;\n  text-decoration: none;\n  -webkit-transition: none 86ms ease-out;\n  transition: none 86ms ease-out;\n}\n\n.ame a:hover {\n  color: #363636;\n}\n\n.ame code {\n  background-color: whitesmoke;\n  color: #ff3860;\n  font-size: 0.8em;\n  font-weight: normal;\n  padding: 0.25em 0.5em 0.25em;\n}\n\n.ame hr {\n  background-color: #dbdbdb;\n  border: none;\n  display: block;\n  height: 1px;\n  margin: 1.5rem 0;\n}\n\n.ame img {\n  max-width: 100%;\n}\n\n.ame input[type=\"checkbox\"], .ame input[type=\"radio\"] {\n  vertical-align: baseline;\n}\n\n.ame small {\n  font-size: 0.875em;\n}\n\n.ame span {\n  font-style: inherit;\n  font-weight: inherit;\n}\n\n.ame strong {\n  color: #363636;\n  font-weight: 700;\n}\n\n.ame pre {\n  background-color: whitesmoke;\n  color: #4a4a4a;\n  font-size: 0.8em;\n  white-space: pre;\n  word-wrap: normal;\n}\n\n.ame pre code {\n  -webkit-overflow-scrolling: touch;\n  background: none;\n  color: inherit;\n  display: block;\n  font-size: 1em;\n  overflow-x: auto;\n  padding: 1.25rem 1.5rem;\n}\n\n.ame table {\n  width: 100%;\n}\n\n.ame table td, .ame table th {\n  text-align: left;\n  vertical-align: top;\n}\n\n.ame table th {\n  color: #363636;\n}\n\n.ame .is-block {\n  display: block;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .is-block-mobile {\n    display: block !important;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .is-block-tablet {\n    display: block !important;\n  }\n}\n\n@media screen and (min-width: 769px) and (max-width: 999px) {\n  .ame .is-block-tablet-only {\n    display: block !important;\n  }\n}\n\n@media screen and (max-width: 999px) {\n  .ame .is-block-touch {\n    display: block !important;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .is-block-desktop {\n    display: block !important;\n  }\n}\n\n@media screen and (min-width: 1000px) and (max-width: 1191px) {\n  .ame .is-block-desktop-only {\n    display: block !important;\n  }\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .is-block-widescreen {\n    display: block !important;\n  }\n}\n\n.ame .is-flex {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .is-flex-mobile {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .is-flex-tablet {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n  }\n}\n\n@media screen and (min-width: 769px) and (max-width: 999px) {\n  .ame .is-flex-tablet-only {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n  }\n}\n\n@media screen and (max-width: 999px) {\n  .ame .is-flex-touch {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .is-flex-desktop {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n  }\n}\n\n@media screen and (min-width: 1000px) and (max-width: 1191px) {\n  .ame .is-flex-desktop-only {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n  }\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .is-flex-widescreen {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n  }\n}\n\n.ame .is-inline {\n  display: inline;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .is-inline-mobile {\n    display: inline !important;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .is-inline-tablet {\n    display: inline !important;\n  }\n}\n\n@media screen and (min-width: 769px) and (max-width: 999px) {\n  .ame .is-inline-tablet-only {\n    display: inline !important;\n  }\n}\n\n@media screen and (max-width: 999px) {\n  .ame .is-inline-touch {\n    display: inline !important;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .is-inline-desktop {\n    display: inline !important;\n  }\n}\n\n@media screen and (min-width: 1000px) and (max-width: 1191px) {\n  .ame .is-inline-desktop-only {\n    display: inline !important;\n  }\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .is-inline-widescreen {\n    display: inline !important;\n  }\n}\n\n.ame .is-inline-block {\n  display: inline-block;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .is-inline-block-mobile {\n    display: inline-block !important;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .is-inline-block-tablet {\n    display: inline-block !important;\n  }\n}\n\n@media screen and (min-width: 769px) and (max-width: 999px) {\n  .ame .is-inline-block-tablet-only {\n    display: inline-block !important;\n  }\n}\n\n@media screen and (max-width: 999px) {\n  .ame .is-inline-block-touch {\n    display: inline-block !important;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .is-inline-block-desktop {\n    display: inline-block !important;\n  }\n}\n\n@media screen and (min-width: 1000px) and (max-width: 1191px) {\n  .ame .is-inline-block-desktop-only {\n    display: inline-block !important;\n  }\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .is-inline-block-widescreen {\n    display: inline-block !important;\n  }\n}\n\n.ame .is-inline-flex {\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .is-inline-flex-mobile {\n    display: -webkit-inline-box !important;\n    display: -ms-inline-flexbox !important;\n    display: inline-flex !important;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .is-inline-flex-tablet {\n    display: -webkit-inline-box !important;\n    display: -ms-inline-flexbox !important;\n    display: inline-flex !important;\n  }\n}\n\n@media screen and (min-width: 769px) and (max-width: 999px) {\n  .ame .is-inline-flex-tablet-only {\n    display: -webkit-inline-box !important;\n    display: -ms-inline-flexbox !important;\n    display: inline-flex !important;\n  }\n}\n\n@media screen and (max-width: 999px) {\n  .ame .is-inline-flex-touch {\n    display: -webkit-inline-box !important;\n    display: -ms-inline-flexbox !important;\n    display: inline-flex !important;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .is-inline-flex-desktop {\n    display: -webkit-inline-box !important;\n    display: -ms-inline-flexbox !important;\n    display: inline-flex !important;\n  }\n}\n\n@media screen and (min-width: 1000px) and (max-width: 1191px) {\n  .ame .is-inline-flex-desktop-only {\n    display: -webkit-inline-box !important;\n    display: -ms-inline-flexbox !important;\n    display: inline-flex !important;\n  }\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .is-inline-flex-widescreen {\n    display: -webkit-inline-box !important;\n    display: -ms-inline-flexbox !important;\n    display: inline-flex !important;\n  }\n}\n\n.ame .is-clearfix:after {\n  clear: both;\n  content: \" \";\n  display: table;\n}\n\n.ame .is-pulled-left {\n  float: left;\n}\n\n.ame .is-pulled-right {\n  float: right;\n}\n\n.ame .is-clipped {\n  overflow: hidden !important;\n}\n\n.ame .is-overlay {\n  bottom: 0;\n  left: 0;\n  position: absolute;\n  right: 0;\n  top: 0;\n}\n\n.ame .has-text-centered {\n  text-align: center;\n}\n\n.ame .has-text-left {\n  text-align: left;\n}\n\n.ame .has-text-right {\n  text-align: right;\n}\n\n.ame .has-text-white {\n  color: white;\n}\n\n.ame a.has-text-white:hover, .ame a.has-text-white:focus {\n  color: #e6e6e6;\n}\n\n.ame .has-text-black {\n  color: #0a0a0a;\n}\n\n.ame a.has-text-black:hover, .ame a.has-text-black:focus {\n  color: black;\n}\n\n.ame .has-text-light {\n  color: whitesmoke;\n}\n\n.ame a.has-text-light:hover, .ame a.has-text-light:focus {\n  color: #dbdbdb;\n}\n\n.ame .has-text-dark {\n  color: #363636;\n}\n\n.ame a.has-text-dark:hover, .ame a.has-text-dark:focus {\n  color: #1c1c1c;\n}\n\n.ame .has-text-primary {\n  color: #00d1b2;\n}\n\n.ame a.has-text-primary:hover, .ame a.has-text-primary:focus {\n  color: #009e86;\n}\n\n.ame .has-text-info {\n  color: #3273dc;\n}\n\n.ame a.has-text-info:hover, .ame a.has-text-info:focus {\n  color: #205bbc;\n}\n\n.ame .has-text-success {\n  color: #23d160;\n}\n\n.ame a.has-text-success:hover, .ame a.has-text-success:focus {\n  color: #1ca64c;\n}\n\n.ame .has-text-warning {\n  color: #ffdd57;\n}\n\n.ame a.has-text-warning:hover, .ame a.has-text-warning:focus {\n  color: #ffd324;\n}\n\n.ame .has-text-danger {\n  color: #ff3860;\n}\n\n.ame a.has-text-danger:hover, .ame a.has-text-danger:focus {\n  color: #ff0537;\n}\n\n.ame .is-hidden {\n  display: none !important;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .is-hidden-mobile {\n    display: none !important;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .is-hidden-tablet {\n    display: none !important;\n  }\n}\n\n@media screen and (min-width: 769px) and (max-width: 999px) {\n  .ame .is-hidden-tablet-only {\n    display: none !important;\n  }\n}\n\n@media screen and (max-width: 999px) {\n  .ame .is-hidden-touch {\n    display: none !important;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .is-hidden-desktop {\n    display: none !important;\n  }\n}\n\n@media screen and (min-width: 1000px) and (max-width: 1191px) {\n  .ame .is-hidden-desktop-only {\n    display: none !important;\n  }\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .is-hidden-widescreen {\n    display: none !important;\n  }\n}\n\n.ame .is-marginless {\n  margin: 0 !important;\n}\n\n.ame .is-paddingless {\n  padding: 0 !important;\n}\n\n.ame .is-unselectable {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n\n.ame .box {\n  background-color: white;\n  border-radius: 5px;\n  -webkit-box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1), 0 0 0 1px rgba(10, 10, 10, 0.1);\n          box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1), 0 0 0 1px rgba(10, 10, 10, 0.1);\n  display: block;\n  padding: 1.25rem;\n}\n\n.ame .box:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame a.box:hover, .ame a.box:focus {\n  -webkit-box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1), 0 0 0 1px #00d1b2;\n          box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1), 0 0 0 1px #00d1b2;\n}\n\n.ame a.box:active {\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2), 0 0 0 1px #00d1b2;\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2), 0 0 0 1px #00d1b2;\n}\n\n.ame .button {\n  -moz-appearance: none;\n  -webkit-appearance: none;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border: 1px solid transparent;\n  border-radius: 3px;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  font-size: 1rem;\n  height: 2.25em;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  line-height: 1.5;\n  padding-bottom: calc(0.375em - 1px);\n  padding-left: calc(0.625em - 1px);\n  padding-right: calc(0.625em - 1px);\n  padding-top: calc(0.375em - 1px);\n  position: relative;\n  vertical-align: top;\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  background-color: white;\n  border-color: #dbdbdb;\n  color: #363636;\n  cursor: pointer;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  padding-left: 0.75em;\n  padding-right: 0.75em;\n  text-align: center;\n  white-space: nowrap;\n}\n\n.ame .button:focus, .ame .button.is-focused, .ame .button:active, .ame .button.is-active {\n  outline: none;\n}\n\n.ame .button[disabled] {\n  cursor: not-allowed;\n}\n\n.ame .button strong {\n  color: inherit;\n}\n\n.ame .button .icon, .ame .button .icon.is-small, .ame .button .icon.is-medium, .ame .button .icon.is-large {\n  height: 1.5em;\n  width: 1.5em;\n}\n\n.ame .button .icon:first-child:not(:last-child) {\n  margin-left: calc(-0.375em - 1px);\n  margin-right: 0.1875em;\n}\n\n.ame .button .icon:last-child:not(:first-child) {\n  margin-left: 0.1875em;\n  margin-right: calc(-0.375em - 1px);\n}\n\n.ame .button .icon:first-child:last-child {\n  margin-left: calc(-0.375em - 1px);\n  margin-right: calc(-0.375em - 1px);\n}\n\n.ame .button:hover, .ame .button.is-hovered {\n  border-color: #b5b5b5;\n  color: #363636;\n}\n\n.ame .button:focus, .ame .button.is-focused {\n  border-color: #00d1b2;\n  -webkit-box-shadow: 0 0 0.5em rgba(0, 209, 178, 0.25);\n          box-shadow: 0 0 0.5em rgba(0, 209, 178, 0.25);\n  color: #363636;\n}\n\n.ame .button:active, .ame .button.is-active {\n  border-color: #4a4a4a;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: #363636;\n}\n\n.ame .button.is-link {\n  background-color: transparent;\n  border-color: transparent;\n  color: #4a4a4a;\n  text-decoration: underline;\n}\n\n.ame .button.is-link:hover, .ame .button.is-link.is-hovered, .ame .button.is-link:focus, .ame .button.is-link.is-focused, .ame .button.is-link:active, .ame .button.is-link.is-active {\n  background-color: whitesmoke;\n  color: #363636;\n}\n\n.ame .button.is-link[disabled] {\n  background-color: transparent;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-white {\n  background-color: white;\n  border-color: transparent;\n  color: #0a0a0a;\n}\n\n.ame .button.is-white:hover, .ame .button.is-white.is-hovered {\n  background-color: #f9f9f9;\n  border-color: transparent;\n  color: #0a0a0a;\n}\n\n.ame .button.is-white:focus, .ame .button.is-white.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(255, 255, 255, 0.25);\n          box-shadow: 0 0 0.5em rgba(255, 255, 255, 0.25);\n  color: #0a0a0a;\n}\n\n.ame .button.is-white:active, .ame .button.is-white.is-active {\n  background-color: #f2f2f2;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: #0a0a0a;\n}\n\n.ame .button.is-white[disabled] {\n  background-color: white;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-white.is-inverted {\n  background-color: #0a0a0a;\n  color: white;\n}\n\n.ame .button.is-white.is-inverted:hover {\n  background-color: black;\n}\n\n.ame .button.is-white.is-inverted[disabled] {\n  background-color: #0a0a0a;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: white;\n}\n\n.ame .button.is-white.is-loading:after {\n  border-color: transparent transparent #0a0a0a #0a0a0a !important;\n}\n\n.ame .button.is-white.is-outlined {\n  background-color: transparent;\n  border-color: white;\n  color: white;\n}\n\n.ame .button.is-white.is-outlined:hover, .ame .button.is-white.is-outlined:focus {\n  background-color: white;\n  border-color: white;\n  color: #0a0a0a;\n}\n\n.ame .button.is-white.is-outlined.is-loading:after {\n  border-color: transparent transparent white white !important;\n}\n\n.ame .button.is-white.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: white;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: white;\n}\n\n.ame .button.is-white.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: #0a0a0a;\n  color: #0a0a0a;\n}\n\n.ame .button.is-white.is-inverted.is-outlined:hover, .ame .button.is-white.is-inverted.is-outlined:focus {\n  background-color: #0a0a0a;\n  color: white;\n}\n\n.ame .button.is-white.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #0a0a0a;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #0a0a0a;\n}\n\n.ame .button.is-black {\n  background-color: #0a0a0a;\n  border-color: transparent;\n  color: white;\n}\n\n.ame .button.is-black:hover, .ame .button.is-black.is-hovered {\n  background-color: #040404;\n  border-color: transparent;\n  color: white;\n}\n\n.ame .button.is-black:focus, .ame .button.is-black.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(10, 10, 10, 0.25);\n          box-shadow: 0 0 0.5em rgba(10, 10, 10, 0.25);\n  color: white;\n}\n\n.ame .button.is-black:active, .ame .button.is-black.is-active {\n  background-color: black;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: white;\n}\n\n.ame .button.is-black[disabled] {\n  background-color: #0a0a0a;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-black.is-inverted {\n  background-color: white;\n  color: #0a0a0a;\n}\n\n.ame .button.is-black.is-inverted:hover {\n  background-color: #f2f2f2;\n}\n\n.ame .button.is-black.is-inverted[disabled] {\n  background-color: white;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #0a0a0a;\n}\n\n.ame .button.is-black.is-loading:after {\n  border-color: transparent transparent white white !important;\n}\n\n.ame .button.is-black.is-outlined {\n  background-color: transparent;\n  border-color: #0a0a0a;\n  color: #0a0a0a;\n}\n\n.ame .button.is-black.is-outlined:hover, .ame .button.is-black.is-outlined:focus {\n  background-color: #0a0a0a;\n  border-color: #0a0a0a;\n  color: white;\n}\n\n.ame .button.is-black.is-outlined.is-loading:after {\n  border-color: transparent transparent #0a0a0a #0a0a0a !important;\n}\n\n.ame .button.is-black.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #0a0a0a;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #0a0a0a;\n}\n\n.ame .button.is-black.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: white;\n  color: white;\n}\n\n.ame .button.is-black.is-inverted.is-outlined:hover, .ame .button.is-black.is-inverted.is-outlined:focus {\n  background-color: white;\n  color: #0a0a0a;\n}\n\n.ame .button.is-black.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: white;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: white;\n}\n\n.ame .button.is-light {\n  background-color: whitesmoke;\n  border-color: transparent;\n  color: #363636;\n}\n\n.ame .button.is-light:hover, .ame .button.is-light.is-hovered {\n  background-color: #eeeeee;\n  border-color: transparent;\n  color: #363636;\n}\n\n.ame .button.is-light:focus, .ame .button.is-light.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(245, 245, 245, 0.25);\n          box-shadow: 0 0 0.5em rgba(245, 245, 245, 0.25);\n  color: #363636;\n}\n\n.ame .button.is-light:active, .ame .button.is-light.is-active {\n  background-color: #e8e8e8;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: #363636;\n}\n\n.ame .button.is-light[disabled] {\n  background-color: whitesmoke;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-light.is-inverted {\n  background-color: #363636;\n  color: whitesmoke;\n}\n\n.ame .button.is-light.is-inverted:hover {\n  background-color: #292929;\n}\n\n.ame .button.is-light.is-inverted[disabled] {\n  background-color: #363636;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: whitesmoke;\n}\n\n.ame .button.is-light.is-loading:after {\n  border-color: transparent transparent #363636 #363636 !important;\n}\n\n.ame .button.is-light.is-outlined {\n  background-color: transparent;\n  border-color: whitesmoke;\n  color: whitesmoke;\n}\n\n.ame .button.is-light.is-outlined:hover, .ame .button.is-light.is-outlined:focus {\n  background-color: whitesmoke;\n  border-color: whitesmoke;\n  color: #363636;\n}\n\n.ame .button.is-light.is-outlined.is-loading:after {\n  border-color: transparent transparent whitesmoke whitesmoke !important;\n}\n\n.ame .button.is-light.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: whitesmoke;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: whitesmoke;\n}\n\n.ame .button.is-light.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: #363636;\n  color: #363636;\n}\n\n.ame .button.is-light.is-inverted.is-outlined:hover, .ame .button.is-light.is-inverted.is-outlined:focus {\n  background-color: #363636;\n  color: whitesmoke;\n}\n\n.ame .button.is-light.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #363636;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #363636;\n}\n\n.ame .button.is-dark {\n  background-color: #363636;\n  border-color: transparent;\n  color: whitesmoke;\n}\n\n.ame .button.is-dark:hover, .ame .button.is-dark.is-hovered {\n  background-color: #2f2f2f;\n  border-color: transparent;\n  color: whitesmoke;\n}\n\n.ame .button.is-dark:focus, .ame .button.is-dark.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(54, 54, 54, 0.25);\n          box-shadow: 0 0 0.5em rgba(54, 54, 54, 0.25);\n  color: whitesmoke;\n}\n\n.ame .button.is-dark:active, .ame .button.is-dark.is-active {\n  background-color: #292929;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: whitesmoke;\n}\n\n.ame .button.is-dark[disabled] {\n  background-color: #363636;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-dark.is-inverted {\n  background-color: whitesmoke;\n  color: #363636;\n}\n\n.ame .button.is-dark.is-inverted:hover {\n  background-color: #e8e8e8;\n}\n\n.ame .button.is-dark.is-inverted[disabled] {\n  background-color: whitesmoke;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #363636;\n}\n\n.ame .button.is-dark.is-loading:after {\n  border-color: transparent transparent whitesmoke whitesmoke !important;\n}\n\n.ame .button.is-dark.is-outlined {\n  background-color: transparent;\n  border-color: #363636;\n  color: #363636;\n}\n\n.ame .button.is-dark.is-outlined:hover, .ame .button.is-dark.is-outlined:focus {\n  background-color: #363636;\n  border-color: #363636;\n  color: whitesmoke;\n}\n\n.ame .button.is-dark.is-outlined.is-loading:after {\n  border-color: transparent transparent #363636 #363636 !important;\n}\n\n.ame .button.is-dark.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #363636;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #363636;\n}\n\n.ame .button.is-dark.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: whitesmoke;\n  color: whitesmoke;\n}\n\n.ame .button.is-dark.is-inverted.is-outlined:hover, .ame .button.is-dark.is-inverted.is-outlined:focus {\n  background-color: whitesmoke;\n  color: #363636;\n}\n\n.ame .button.is-dark.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: whitesmoke;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: whitesmoke;\n}\n\n.ame .button.is-primary {\n  background-color: #00d1b2;\n  border-color: transparent;\n  color: #fff;\n}\n\n.ame .button.is-primary:hover, .ame .button.is-primary.is-hovered {\n  background-color: #00c4a7;\n  border-color: transparent;\n  color: #fff;\n}\n\n.ame .button.is-primary:focus, .ame .button.is-primary.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(0, 209, 178, 0.25);\n          box-shadow: 0 0 0.5em rgba(0, 209, 178, 0.25);\n  color: #fff;\n}\n\n.ame .button.is-primary:active, .ame .button.is-primary.is-active {\n  background-color: #00b89c;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: #fff;\n}\n\n.ame .button.is-primary[disabled] {\n  background-color: #00d1b2;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-primary.is-inverted {\n  background-color: #fff;\n  color: #00d1b2;\n}\n\n.ame .button.is-primary.is-inverted:hover {\n  background-color: #f2f2f2;\n}\n\n.ame .button.is-primary.is-inverted[disabled] {\n  background-color: #fff;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #00d1b2;\n}\n\n.ame .button.is-primary.is-loading:after {\n  border-color: transparent transparent #fff #fff !important;\n}\n\n.ame .button.is-primary.is-outlined {\n  background-color: transparent;\n  border-color: #00d1b2;\n  color: #00d1b2;\n}\n\n.ame .button.is-primary.is-outlined:hover, .ame .button.is-primary.is-outlined:focus {\n  background-color: #00d1b2;\n  border-color: #00d1b2;\n  color: #fff;\n}\n\n.ame .button.is-primary.is-outlined.is-loading:after {\n  border-color: transparent transparent #00d1b2 #00d1b2 !important;\n}\n\n.ame .button.is-primary.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #00d1b2;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #00d1b2;\n}\n\n.ame .button.is-primary.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: #fff;\n  color: #fff;\n}\n\n.ame .button.is-primary.is-inverted.is-outlined:hover, .ame .button.is-primary.is-inverted.is-outlined:focus {\n  background-color: #fff;\n  color: #00d1b2;\n}\n\n.ame .button.is-primary.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #fff;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #fff;\n}\n\n.ame .button.is-info {\n  background-color: #3273dc;\n  border-color: transparent;\n  color: #fff;\n}\n\n.ame .button.is-info:hover, .ame .button.is-info.is-hovered {\n  background-color: #276cda;\n  border-color: transparent;\n  color: #fff;\n}\n\n.ame .button.is-info:focus, .ame .button.is-info.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(50, 115, 220, 0.25);\n          box-shadow: 0 0 0.5em rgba(50, 115, 220, 0.25);\n  color: #fff;\n}\n\n.ame .button.is-info:active, .ame .button.is-info.is-active {\n  background-color: #2366d1;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: #fff;\n}\n\n.ame .button.is-info[disabled] {\n  background-color: #3273dc;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-info.is-inverted {\n  background-color: #fff;\n  color: #3273dc;\n}\n\n.ame .button.is-info.is-inverted:hover {\n  background-color: #f2f2f2;\n}\n\n.ame .button.is-info.is-inverted[disabled] {\n  background-color: #fff;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #3273dc;\n}\n\n.ame .button.is-info.is-loading:after {\n  border-color: transparent transparent #fff #fff !important;\n}\n\n.ame .button.is-info.is-outlined {\n  background-color: transparent;\n  border-color: #3273dc;\n  color: #3273dc;\n}\n\n.ame .button.is-info.is-outlined:hover, .ame .button.is-info.is-outlined:focus {\n  background-color: #3273dc;\n  border-color: #3273dc;\n  color: #fff;\n}\n\n.ame .button.is-info.is-outlined.is-loading:after {\n  border-color: transparent transparent #3273dc #3273dc !important;\n}\n\n.ame .button.is-info.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #3273dc;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #3273dc;\n}\n\n.ame .button.is-info.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: #fff;\n  color: #fff;\n}\n\n.ame .button.is-info.is-inverted.is-outlined:hover, .ame .button.is-info.is-inverted.is-outlined:focus {\n  background-color: #fff;\n  color: #3273dc;\n}\n\n.ame .button.is-info.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #fff;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #fff;\n}\n\n.ame .button.is-success {\n  background-color: #23d160;\n  border-color: transparent;\n  color: #fff;\n}\n\n.ame .button.is-success:hover, .ame .button.is-success.is-hovered {\n  background-color: #22c65b;\n  border-color: transparent;\n  color: #fff;\n}\n\n.ame .button.is-success:focus, .ame .button.is-success.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(35, 209, 96, 0.25);\n          box-shadow: 0 0 0.5em rgba(35, 209, 96, 0.25);\n  color: #fff;\n}\n\n.ame .button.is-success:active, .ame .button.is-success.is-active {\n  background-color: #20bc56;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: #fff;\n}\n\n.ame .button.is-success[disabled] {\n  background-color: #23d160;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-success.is-inverted {\n  background-color: #fff;\n  color: #23d160;\n}\n\n.ame .button.is-success.is-inverted:hover {\n  background-color: #f2f2f2;\n}\n\n.ame .button.is-success.is-inverted[disabled] {\n  background-color: #fff;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #23d160;\n}\n\n.ame .button.is-success.is-loading:after {\n  border-color: transparent transparent #fff #fff !important;\n}\n\n.ame .button.is-success.is-outlined {\n  background-color: transparent;\n  border-color: #23d160;\n  color: #23d160;\n}\n\n.ame .button.is-success.is-outlined:hover, .ame .button.is-success.is-outlined:focus {\n  background-color: #23d160;\n  border-color: #23d160;\n  color: #fff;\n}\n\n.ame .button.is-success.is-outlined.is-loading:after {\n  border-color: transparent transparent #23d160 #23d160 !important;\n}\n\n.ame .button.is-success.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #23d160;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #23d160;\n}\n\n.ame .button.is-success.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: #fff;\n  color: #fff;\n}\n\n.ame .button.is-success.is-inverted.is-outlined:hover, .ame .button.is-success.is-inverted.is-outlined:focus {\n  background-color: #fff;\n  color: #23d160;\n}\n\n.ame .button.is-success.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #fff;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #fff;\n}\n\n.ame .button.is-warning {\n  background-color: #ffdd57;\n  border-color: transparent;\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .button.is-warning:hover, .ame .button.is-warning.is-hovered {\n  background-color: #ffdb4a;\n  border-color: transparent;\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .button.is-warning:focus, .ame .button.is-warning.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(255, 221, 87, 0.25);\n          box-shadow: 0 0 0.5em rgba(255, 221, 87, 0.25);\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .button.is-warning:active, .ame .button.is-warning.is-active {\n  background-color: #ffd83d;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .button.is-warning[disabled] {\n  background-color: #ffdd57;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-warning.is-inverted {\n  background-color: rgba(0, 0, 0, 0.7);\n  color: #ffdd57;\n}\n\n.ame .button.is-warning.is-inverted:hover {\n  background-color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .button.is-warning.is-inverted[disabled] {\n  background-color: rgba(0, 0, 0, 0.7);\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #ffdd57;\n}\n\n.ame .button.is-warning.is-loading:after {\n  border-color: transparent transparent rgba(0, 0, 0, 0.7) rgba(0, 0, 0, 0.7) !important;\n}\n\n.ame .button.is-warning.is-outlined {\n  background-color: transparent;\n  border-color: #ffdd57;\n  color: #ffdd57;\n}\n\n.ame .button.is-warning.is-outlined:hover, .ame .button.is-warning.is-outlined:focus {\n  background-color: #ffdd57;\n  border-color: #ffdd57;\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .button.is-warning.is-outlined.is-loading:after {\n  border-color: transparent transparent #ffdd57 #ffdd57 !important;\n}\n\n.ame .button.is-warning.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #ffdd57;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #ffdd57;\n}\n\n.ame .button.is-warning.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: rgba(0, 0, 0, 0.7);\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .button.is-warning.is-inverted.is-outlined:hover, .ame .button.is-warning.is-inverted.is-outlined:focus {\n  background-color: rgba(0, 0, 0, 0.7);\n  color: #ffdd57;\n}\n\n.ame .button.is-warning.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: rgba(0, 0, 0, 0.7);\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .button.is-danger {\n  background-color: #ff3860;\n  border-color: transparent;\n  color: #fff;\n}\n\n.ame .button.is-danger:hover, .ame .button.is-danger.is-hovered {\n  background-color: #ff2b56;\n  border-color: transparent;\n  color: #fff;\n}\n\n.ame .button.is-danger:focus, .ame .button.is-danger.is-focused {\n  border-color: transparent;\n  -webkit-box-shadow: 0 0 0.5em rgba(255, 56, 96, 0.25);\n          box-shadow: 0 0 0.5em rgba(255, 56, 96, 0.25);\n  color: #fff;\n}\n\n.ame .button.is-danger:active, .ame .button.is-danger.is-active {\n  background-color: #ff1f4b;\n  border-color: transparent;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n  color: #fff;\n}\n\n.ame .button.is-danger[disabled] {\n  background-color: #ff3860;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n\n.ame .button.is-danger.is-inverted {\n  background-color: #fff;\n  color: #ff3860;\n}\n\n.ame .button.is-danger.is-inverted:hover {\n  background-color: #f2f2f2;\n}\n\n.ame .button.is-danger.is-inverted[disabled] {\n  background-color: #fff;\n  border-color: transparent;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #ff3860;\n}\n\n.ame .button.is-danger.is-loading:after {\n  border-color: transparent transparent #fff #fff !important;\n}\n\n.ame .button.is-danger.is-outlined {\n  background-color: transparent;\n  border-color: #ff3860;\n  color: #ff3860;\n}\n\n.ame .button.is-danger.is-outlined:hover, .ame .button.is-danger.is-outlined:focus {\n  background-color: #ff3860;\n  border-color: #ff3860;\n  color: #fff;\n}\n\n.ame .button.is-danger.is-outlined.is-loading:after {\n  border-color: transparent transparent #ff3860 #ff3860 !important;\n}\n\n.ame .button.is-danger.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #ff3860;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #ff3860;\n}\n\n.ame .button.is-danger.is-inverted.is-outlined {\n  background-color: transparent;\n  border-color: #fff;\n  color: #fff;\n}\n\n.ame .button.is-danger.is-inverted.is-outlined:hover, .ame .button.is-danger.is-inverted.is-outlined:focus {\n  background-color: #fff;\n  color: #ff3860;\n}\n\n.ame .button.is-danger.is-inverted.is-outlined[disabled] {\n  background-color: transparent;\n  border-color: #fff;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #fff;\n}\n\n.ame .button.is-small {\n  border-radius: 2px;\n  font-size: 0.75rem;\n}\n\n.ame .button.is-medium {\n  font-size: 1.25rem;\n}\n\n.ame .button.is-large {\n  font-size: 1.5rem;\n}\n\n.ame .button[disabled] {\n  background-color: white;\n  border-color: #dbdbdb;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  opacity: 0.5;\n}\n\n.ame .button.is-fullwidth {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n}\n\n.ame .button.is-loading {\n  color: transparent !important;\n  pointer-events: none;\n}\n\n.ame .button.is-loading:after {\n  -webkit-animation: spinAround 500ms infinite linear;\n          animation: spinAround 500ms infinite linear;\n  border: 2px solid #dbdbdb;\n  border-radius: 290486px;\n  border-right-color: transparent;\n  border-top-color: transparent;\n  content: \"\";\n  display: block;\n  height: 1em;\n  position: relative;\n  width: 1em;\n  position: absolute;\n  left: calc(50% - (1em / 2));\n  top: calc(50% - (1em / 2));\n  position: absolute !important;\n}\n\n.ame .button.is-static {\n  background-color: whitesmoke;\n  border-color: #dbdbdb;\n  color: #7a7a7a;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  pointer-events: none;\n}\n\n.ame button.button, .ame input[type=\"submit\"].button {\n  line-height: 1;\n  padding-bottom: 0.4em;\n  padding-top: 0.35em;\n}\n\n.ame .content {\n  color: #4a4a4a;\n}\n\n.ame .content:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .content li + li {\n  margin-top: 0.25em;\n}\n\n.ame .content p:not(:last-child), .ame .content dl:not(:last-child), .ame .content ol:not(:last-child), .ame .content ul:not(:last-child), .ame .content blockquote:not(:last-child), .ame .content pre:not(:last-child), .ame .content table:not(:last-child) {\n  margin-bottom: 1em;\n}\n\n.ame .content h1, .ame .content h2, .ame .content h3, .ame .content h4, .ame .content h5, .ame .content h6 {\n  color: #363636;\n  font-weight: 400;\n  line-height: 1.125;\n}\n\n.ame .content h1 {\n  font-size: 2em;\n  margin-bottom: 0.5em;\n}\n\n.ame .content h1:not(:first-child) {\n  margin-top: 1em;\n}\n\n.ame .content h2 {\n  font-size: 1.75em;\n  margin-bottom: 0.5714em;\n}\n\n.ame .content h2:not(:first-child) {\n  margin-top: 1.1428em;\n}\n\n.ame .content h3 {\n  font-size: 1.5em;\n  margin-bottom: 0.6666em;\n}\n\n.ame .content h3:not(:first-child) {\n  margin-top: 1.3333em;\n}\n\n.ame .content h4 {\n  font-size: 1.25em;\n  margin-bottom: 0.8em;\n}\n\n.ame .content h5 {\n  font-size: 1.125em;\n  margin-bottom: 0.8888em;\n}\n\n.ame .content h6 {\n  font-size: 1em;\n  margin-bottom: 1em;\n}\n\n.ame .content blockquote {\n  background-color: whitesmoke;\n  border-left: 5px solid #dbdbdb;\n  padding: 1.25em 1.5em;\n}\n\n.ame .content ol {\n  list-style: decimal outside;\n  margin-left: 2em;\n  margin-top: 1em;\n}\n\n.ame .content ul {\n  list-style: disc outside;\n  margin-left: 2em;\n  margin-top: 1em;\n}\n\n.ame .content ul ul {\n  list-style-type: circle;\n  margin-top: 0.5em;\n}\n\n.ame .content ul ul ul {\n  list-style-type: square;\n}\n\n.ame .content dd {\n  margin-left: 2em;\n}\n\n.ame .content figure {\n  text-align: center;\n}\n\n.ame .content figure img {\n  display: inline-block;\n}\n\n.ame .content figure figcaption {\n  font-style: italic;\n}\n\n.ame .content pre {\n  -webkit-overflow-scrolling: touch;\n  overflow-x: auto;\n  padding: 1.25em 1.5em;\n  white-space: pre;\n  word-wrap: normal;\n}\n\n.ame .content sup, .ame .content sub {\n  font-size: 70%;\n}\n\n.ame .content table {\n  width: 100%;\n}\n\n.ame .content table td, .ame .content table th {\n  border: 1px solid #dbdbdb;\n  border-width: 0 0 1px;\n  padding: 0.5em 0.75em;\n  vertical-align: top;\n}\n\n.ame .content table th {\n  color: #363636;\n  text-align: left;\n}\n\n.ame .content table tr:hover {\n  background-color: whitesmoke;\n}\n\n.ame .content table thead td, .ame .content table thead th {\n  border-width: 0 0 2px;\n  color: #363636;\n}\n\n.ame .content table tfoot td, .ame .content table tfoot th {\n  border-width: 2px 0 0;\n  color: #363636;\n}\n\n.ame ..content table tbody tr:last-child td, .ame .\n.content table tbody tr:last-child th {\n  border-bottom-width: 0;\n}\n\n.ame .content.is-small {\n  font-size: 0.75rem;\n}\n\n.ame .content.is-medium {\n  font-size: 1.25rem;\n}\n\n.ame .content.is-large {\n  font-size: 1.5rem;\n}\n\n.ame .input, .ame .textarea {\n  -moz-appearance: none;\n  -webkit-appearance: none;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border: 1px solid transparent;\n  border-radius: 3px;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  font-size: 1rem;\n  height: 2.25em;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  line-height: 1.5;\n  padding-bottom: calc(0.375em - 1px);\n  padding-left: calc(0.625em - 1px);\n  padding-right: calc(0.625em - 1px);\n  padding-top: calc(0.375em - 1px);\n  position: relative;\n  vertical-align: top;\n  background-color: white;\n  border-color: #dbdbdb;\n  color: #363636;\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);\n  max-width: 100%;\n  width: 100%;\n}\n\n.ame .input:focus, .ame .input.is-focused, .ame .input:active, .ame .input.is-active, .ame .textarea:focus, .ame .textarea.is-focused, .ame .textarea:active, .ame .textarea.is-active {\n  outline: none;\n}\n\n.ame .input[disabled], .ame .textarea[disabled] {\n  cursor: not-allowed;\n}\n\n.ame .input:hover, .ame .input.is-hovered, .ame .textarea:hover, .ame .textarea.is-hovered {\n  border-color: #b5b5b5;\n}\n\n.ame .input:focus, .ame .input.is-focused, .ame .input:active, .ame .input.is-active, .ame .textarea:focus, .ame .textarea.is-focused, .ame .textarea:active, .ame .textarea.is-active {\n  border-color: #00d1b2;\n}\n\n.ame .input[disabled], .ame .textarea[disabled] {\n  background-color: whitesmoke;\n  border-color: whitesmoke;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #7a7a7a;\n}\n\n.ame .input[disabled]::-moz-placeholder, .ame .textarea[disabled]::-moz-placeholder {\n  color: rgba(54, 54, 54, 0.3);\n}\n\n.ame .input[disabled]::-webkit-input-placeholder, .ame .textarea[disabled]::-webkit-input-placeholder {\n  color: rgba(54, 54, 54, 0.3);\n}\n\n.ame .input[disabled]:-moz-placeholder, .ame .textarea[disabled]:-moz-placeholder {\n  color: rgba(54, 54, 54, 0.3);\n}\n\n.ame .input[disabled]:-ms-input-placeholder, .ame .textarea[disabled]:-ms-input-placeholder {\n  color: rgba(54, 54, 54, 0.3);\n}\n\n.ame .input[type=\"search\"], .ame .textarea[type=\"search\"] {\n  border-radius: 290486px;\n}\n\n.ame .input.is-white, .ame .textarea.is-white {\n  border-color: white;\n}\n\n.ame .input.is-black, .ame .textarea.is-black {\n  border-color: #0a0a0a;\n}\n\n.ame .input.is-light, .ame .textarea.is-light {\n  border-color: whitesmoke;\n}\n\n.ame .input.is-dark, .ame .textarea.is-dark {\n  border-color: #363636;\n}\n\n.ame .input.is-primary, .ame .textarea.is-primary {\n  border-color: #00d1b2;\n}\n\n.ame .input.is-info, .ame .textarea.is-info {\n  border-color: #3273dc;\n}\n\n.ame .input.is-success, .ame .textarea.is-success {\n  border-color: #23d160;\n}\n\n.ame .input.is-warning, .ame .textarea.is-warning {\n  border-color: #ffdd57;\n}\n\n.ame .input.is-danger, .ame .textarea.is-danger {\n  border-color: #ff3860;\n}\n\n.ame .input.is-small, .ame .textarea.is-small {\n  border-radius: 2px;\n  font-size: 0.75rem;\n}\n\n.ame .input.is-medium, .ame .textarea.is-medium {\n  font-size: 1.25rem;\n}\n\n.ame .input.is-large, .ame .textarea.is-large {\n  font-size: 1.5rem;\n}\n\n.ame .input.is-fullwidth, .ame .textarea.is-fullwidth {\n  display: block;\n  width: 100%;\n}\n\n.ame .input.is-inline, .ame .textarea.is-inline {\n  display: inline;\n  width: auto;\n}\n\n.ame .textarea {\n  display: block;\n  max-height: 600px;\n  max-width: 100%;\n  min-height: 120px;\n  min-width: 100%;\n  padding: 0.625em;\n  resize: vertical;\n}\n\n.ame .checkbox, .ame .radio {\n  cursor: pointer;\n  display: inline-block;\n  line-height: 1.25;\n  position: relative;\n}\n\n.ame .checkbox input, .ame .radio input {\n  cursor: pointer;\n}\n\n.ame .checkbox:hover, .ame .radio:hover {\n  color: #363636;\n}\n\n.ame .checkbox[disabled], .ame .radio[disabled] {\n  color: #7a7a7a;\n  cursor: not-allowed;\n}\n\n.ame .radio + .radio {\n  margin-left: 0.5em;\n}\n\n.ame .select {\n  display: inline-block;\n  height: 2.25em;\n  max-width: 100%;\n  position: relative;\n  vertical-align: top;\n}\n\n.ame .select:after {\n  border: 1px solid #00d1b2;\n  border-right: 0;\n  border-top: 0;\n  content: \" \";\n  display: block;\n  height: 0.5em;\n  pointer-events: none;\n  position: absolute;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n  width: 0.5em;\n  margin-top: -0.375em;\n  right: 1.125em;\n  top: 50%;\n  z-index: 4;\n}\n\n.ame .select select {\n  -moz-appearance: none;\n  -webkit-appearance: none;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border: 1px solid transparent;\n  border-radius: 3px;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  font-size: 1rem;\n  height: 2.25em;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  line-height: 1.5;\n  padding-bottom: calc(0.375em - 1px);\n  padding-left: calc(0.625em - 1px);\n  padding-right: calc(0.625em - 1px);\n  padding-top: calc(0.375em - 1px);\n  position: relative;\n  vertical-align: top;\n  background-color: white;\n  border-color: #dbdbdb;\n  color: #363636;\n  cursor: pointer;\n  display: block;\n  font-size: 1em;\n  max-width: 100%;\n  outline: none;\n  padding-right: 2.5em;\n}\n\n.ame .select select:focus, .ame .select select.is-focused, .ame .select select:active, .ame .select select.is-active {\n  outline: none;\n}\n\n.ame .select select[disabled] {\n  cursor: not-allowed;\n}\n\n.ame .select select:hover, .ame .select select.is-hovered {\n  border-color: #b5b5b5;\n}\n\n.ame .select select:focus, .ame .select select.is-focused, .ame .select select:active, .ame .select select.is-active {\n  border-color: #00d1b2;\n}\n\n.ame .select select[disabled] {\n  background-color: whitesmoke;\n  border-color: whitesmoke;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #7a7a7a;\n}\n\n.ame .select select[disabled]::-moz-placeholder {\n  color: rgba(54, 54, 54, 0.3);\n}\n\n.ame .select select[disabled]::-webkit-input-placeholder {\n  color: rgba(54, 54, 54, 0.3);\n}\n\n.ame .select select[disabled]:-moz-placeholder {\n  color: rgba(54, 54, 54, 0.3);\n}\n\n.ame .select select[disabled]:-ms-input-placeholder {\n  color: rgba(54, 54, 54, 0.3);\n}\n\n.ame .select select:hover {\n  border-color: #b5b5b5;\n}\n\n.ame .select select::-ms-expand {\n  display: none;\n}\n\n.ame .select select[disabled]:hover {\n  border-color: whitesmoke;\n}\n\n.ame .select:hover:after {\n  border-color: #363636;\n}\n\n.ame .select.is-white select {\n  border-color: white;\n}\n\n.ame .select.is-black select {\n  border-color: #0a0a0a;\n}\n\n.ame .select.is-light select {\n  border-color: whitesmoke;\n}\n\n.ame .select.is-dark select {\n  border-color: #363636;\n}\n\n.ame .select.is-primary select {\n  border-color: #00d1b2;\n}\n\n.ame .select.is-info select {\n  border-color: #3273dc;\n}\n\n.ame .select.is-success select {\n  border-color: #23d160;\n}\n\n.ame .select.is-warning select {\n  border-color: #ffdd57;\n}\n\n.ame .select.is-danger select {\n  border-color: #ff3860;\n}\n\n.ame .select.is-small {\n  border-radius: 2px;\n  font-size: 0.75rem;\n}\n\n.ame .select.is-medium {\n  font-size: 1.25rem;\n}\n\n.ame .select.is-large {\n  font-size: 1.5rem;\n}\n\n.ame .select.is-disabled:after {\n  border-color: #7a7a7a;\n}\n\n.ame .select.is-fullwidth {\n  width: 100%;\n}\n\n.ame .select.is-fullwidth select {\n  width: 100%;\n}\n\n.ame .select.is-loading:after {\n  -webkit-animation: spinAround 500ms infinite linear;\n          animation: spinAround 500ms infinite linear;\n  border: 2px solid #dbdbdb;\n  border-radius: 290486px;\n  border-right-color: transparent;\n  border-top-color: transparent;\n  content: \"\";\n  display: block;\n  height: 1em;\n  position: relative;\n  width: 1em;\n  margin-top: 0;\n  position: absolute;\n  right: 0.625em;\n  top: 0.625em;\n  -webkit-transform: none;\n          transform: none;\n}\n\n.ame .select.is-loading.is-small:after {\n  font-size: 0.75rem;\n}\n\n.ame .select.is-loading.is-medium:after {\n  font-size: 1.25rem;\n}\n\n.ame .select.is-loading.is-large:after {\n  font-size: 1.5rem;\n}\n\n.ame .label {\n  color: #363636;\n  display: block;\n  font-size: 1rem;\n  font-weight: 700;\n}\n\n.ame .label:not(:last-child) {\n  margin-bottom: 0.5em;\n}\n\n.ame .label.is-small {\n  font-size: 0.75rem;\n}\n\n.ame .label.is-medium {\n  font-size: 1.25rem;\n}\n\n.ame .label.is-large {\n  font-size: 1.5rem;\n}\n\n.ame .help {\n  display: block;\n  font-size: 0.75rem;\n  margin-top: 0.25rem;\n}\n\n.ame .help.is-white {\n  color: white;\n}\n\n.ame .help.is-black {\n  color: #0a0a0a;\n}\n\n.ame .help.is-light {\n  color: whitesmoke;\n}\n\n.ame .help.is-dark {\n  color: #363636;\n}\n\n.ame .help.is-primary {\n  color: #00d1b2;\n}\n\n.ame .help.is-info {\n  color: #3273dc;\n}\n\n.ame .help.is-success {\n  color: #23d160;\n}\n\n.ame .help.is-warning {\n  color: #ffdd57;\n}\n\n.ame .help.is-danger {\n  color: #ff3860;\n}\n\n.ame .field:not(:last-child) {\n  margin-bottom: 0.75rem;\n}\n\n.ame .field.has-addons {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n\n.ame .field.has-addons .control {\n  margin-right: -1px;\n}\n\n.ame .field.has-addons .control:first-child .button, .ame .field.has-addons .control:first-child .input, .ame .field.has-addons .control:first-child .select select {\n  border-bottom-left-radius: 3px;\n  border-top-left-radius: 3px;\n}\n\n.ame .field.has-addons .control:last-child .button, .ame .field.has-addons .control:last-child .input, .ame .field.has-addons .control:last-child .select select {\n  border-bottom-right-radius: 3px;\n  border-top-right-radius: 3px;\n}\n\n.ame .field.has-addons .control .button, .ame .field.has-addons .control .input, .ame .field.has-addons .control .select select {\n  border-radius: 0;\n}\n\n.ame .field.has-addons .control .button:hover, .ame .field.has-addons .control .button.is-hovered, .ame .field.has-addons .control .input:hover, .ame .field.has-addons .control .input.is-hovered, .ame .field.has-addons .control .select select:hover, .ame .field.has-addons .control .select select.is-hovered {\n  z-index: 2;\n}\n\n.ame .field.has-addons .control .button:focus, .ame .field.has-addons .control .button.is-focused, .ame .field.has-addons .control .button:active, .ame .field.has-addons .control .button.is-active, .ame .field.has-addons .control .input:focus, .ame .field.has-addons .control .input.is-focused, .ame .field.has-addons .control .input:active, .ame .field.has-addons .control .input.is-active, .ame .field.has-addons .control .select select:focus, .ame .field.has-addons .control .select select.is-focused, .ame .field.has-addons .control .select select:active, .ame .field.has-addons .control .select select.is-active {\n  z-index: 3;\n}\n\n.ame .field.has-addons .control .button:focus:hover, .ame .field.has-addons .control .button.is-focused:hover, .ame .field.has-addons .control .button:active:hover, .ame .field.has-addons .control .button.is-active:hover, .ame .field.has-addons .control .input:focus:hover, .ame .field.has-addons .control .input.is-focused:hover, .ame .field.has-addons .control .input:active:hover, .ame .field.has-addons .control .input.is-active:hover, .ame .field.has-addons .control .select select:focus:hover, .ame .field.has-addons .control .select select.is-focused:hover, .ame .field.has-addons .control .select select:active:hover, .ame .field.has-addons .control .select select.is-active:hover {\n  z-index: 4;\n}\n\n.ame .field.has-addons .control.is-expanded {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n}\n\n.ame .field.has-addons.has-addons-centered {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n\n.ame .field.has-addons.has-addons-right {\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n\n.ame .field.has-addons.has-addons-fullwidth .control {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n}\n\n.ame .field.is-grouped {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n\n.ame .field.is-grouped > .control {\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n}\n\n.ame .field.is-grouped > .control:not(:last-child) {\n  margin-bottom: 0;\n  margin-right: 0.75rem;\n}\n\n.ame .field.is-grouped > .control.is-expanded {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 1;\n      flex-shrink: 1;\n}\n\n.ame .field.is-grouped.is-grouped-centered {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n\n.ame .field.is-grouped.is-grouped-right {\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .field.is-horizontal {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n}\n\n.ame .field-label .label {\n  font-size: inherit;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .field-label {\n    margin-bottom: 0.5rem;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .field-label {\n    -ms-flex-preferred-size: 0;\n        flex-basis: 0;\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1;\n    -ms-flex-negative: 0;\n        flex-shrink: 0;\n    margin-right: 1.5rem;\n    text-align: right;\n  }\n  .ame .field-label.is-small {\n    font-size: 0.75rem;\n    padding-top: 0.375em;\n  }\n  .ame .field-label.is-normal {\n    padding-top: 0.375em;\n  }\n  .ame .field-label.is-medium {\n    font-size: 1.25rem;\n    padding-top: 0.375em;\n  }\n  .ame .field-label.is-large {\n    font-size: 1.5rem;\n    padding-top: 0.375em;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame ..field-body {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-preferred-size: 0;\n        flex-basis: 0;\n    -webkit-box-flex: 5;\n        -ms-flex-positive: 5;\n            flex-grow: 5;\n    -ms-flex-negative: 1;\n        flex-shrink: 1;\n  }\n  .ame ..field-body .field {\n    -ms-flex-negative: 1;\n        flex-shrink: 1;\n  }\n  .ame ..field-body .field:not(.is-narrow) {\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1;\n  }\n  .ame ..field-body .field:not(:last-child) {\n    margin-bottom: 0;\n    margin-right: 0.75rem;\n  }\n}\n\n.ame .control {\n  font-size: 1rem;\n  position: relative;\n  text-align: left;\n}\n\n.ame .control.has-icon .icon {\n  color: #dbdbdb;\n  height: 2.25em;\n  pointer-events: none;\n  position: absolute;\n  top: 0;\n  width: 2.25em;\n  z-index: 4;\n}\n\n.ame .control.has-icon .input:focus + .icon {\n  color: #7a7a7a;\n}\n\n.ame .control.has-icon .input.is-small + .icon {\n  font-size: 0.75rem;\n}\n\n.ame .control.has-icon .input.is-medium + .icon {\n  font-size: 1.25rem;\n}\n\n.ame .control.has-icon .input.is-large + .icon {\n  font-size: 1.5rem;\n}\n\n.ame .control.has-icon:not(.has-icon-right) .icon {\n  left: 0;\n}\n\n.ame .control.has-icon:not(.has-icon-right) .input {\n  padding-left: 2.25em;\n}\n\n.ame .control.has-icon.has-icon-right .icon {\n  right: 0;\n}\n\n.ame .control.has-icon.has-icon-right .input {\n  padding-right: 2.25em;\n}\n\n.ame .control.has-icons-left .input:focus ~ .icon, .ame .control.has-icons-left .select select:focus ~ .icon, .ame .control.has-icons-right .input:focus ~ .icon, .ame .control.has-icons-right .select select:focus ~ .icon {\n  color: #7a7a7a;\n}\n\n.ame .control.has-icons-left .input.is-small ~ .icon, .ame .control.has-icons-left .select select.is-small ~ .icon, .ame .control.has-icons-right .input.is-small ~ .icon, .ame .control.has-icons-right .select select.is-small ~ .icon {\n  font-size: 0.75rem;\n}\n\n.ame .control.has-icons-left .input.is-medium ~ .icon, .ame .control.has-icons-left .select select.is-medium ~ .icon, .ame .control.has-icons-right .input.is-medium ~ .icon, .ame .control.has-icons-right .select select.is-medium ~ .icon {\n  font-size: 1.25rem;\n}\n\n.ame .control.has-icons-left .input.is-large ~ .icon, .ame .control.has-icons-left .select select.is-large ~ .icon, .ame .control.has-icons-right .input.is-large ~ .icon, .ame .control.has-icons-right .select select.is-large ~ .icon {\n  font-size: 1.5rem;\n}\n\n.ame .control.has-icons-left .icon, .ame .control.has-icons-right .icon {\n  color: #dbdbdb;\n  height: 2.25em;\n  pointer-events: none;\n  position: absolute;\n  top: 0;\n  width: 2.25em;\n  z-index: 4;\n}\n\n.ame .control.has-icons-left .input, .ame .control.has-icons-left .select select {\n  padding-left: 2.25em;\n}\n\n.ame .control.has-icons-left .icon.is-left {\n  left: 0;\n}\n\n.ame .control.has-icons-right .input, .ame .control.has-icons-right .select select {\n  padding-right: 2.25em;\n}\n\n.ame .control.has-icons-right .icon.is-right {\n  right: 0;\n}\n\n.ame .control.is-loading:after {\n  -webkit-animation: spinAround 500ms infinite linear;\n          animation: spinAround 500ms infinite linear;\n  border: 2px solid #dbdbdb;\n  border-radius: 290486px;\n  border-right-color: transparent;\n  border-top-color: transparent;\n  content: \"\";\n  display: block;\n  height: 1em;\n  position: relative;\n  width: 1em;\n  position: absolute !important;\n  right: 0.625em;\n  top: 0.625em;\n}\n\n.ame .control.is-loading.is-small:after {\n  font-size: 0.75rem;\n}\n\n.ame .control.is-loading.is-medium:after {\n  font-size: 1.25rem;\n}\n\n.ame .control.is-loading.is-large:after {\n  font-size: 1.5rem;\n}\n\n.ame .icon {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  height: 1.5rem;\n  width: 1.5rem;\n}\n\n.ame .icon .fa {\n  font-size: 21px;\n}\n\n.ame .icon.is-small {\n  height: 1rem;\n  width: 1rem;\n}\n\n.ame .icon.is-small .fa {\n  font-size: 14px;\n}\n\n.ame .icon.is-medium {\n  height: 2rem;\n  width: 2rem;\n}\n\n.ame .icon.is-medium .fa {\n  font-size: 28px;\n}\n\n.ame .icon.is-large {\n  height: 3rem;\n  width: 3rem;\n}\n\n.ame .icon.is-large .fa {\n  font-size: 42px;\n}\n\n.ame .image {\n  display: block;\n  position: relative;\n}\n\n.ame .image img {\n  display: block;\n  height: auto;\n  width: 100%;\n}\n\n.ame .image.is-square img, .ame .image.is-1by1 img, .ame .image.is-4by3 img, .ame .image.is-3by2 img, .ame .image.is-16by9 img, .ame .image.is-2by1 img {\n  bottom: 0;\n  left: 0;\n  position: absolute;\n  right: 0;\n  top: 0;\n  height: 100%;\n  width: 100%;\n}\n\n.ame .image.is-square, .ame .image.is-1by1 {\n  padding-top: 100%;\n}\n\n.ame .image.is-4by3 {\n  padding-top: 75%;\n}\n\n.ame .image.is-3by2 {\n  padding-top: 66.6666%;\n}\n\n.ame .image.is-16by9 {\n  padding-top: 56.25%;\n}\n\n.ame .image.is-2by1 {\n  padding-top: 50%;\n}\n\n.ame .image.is-16x16 {\n  height: 16px;\n  width: 16px;\n}\n\n.ame .image.is-24x24 {\n  height: 24px;\n  width: 24px;\n}\n\n.ame .image.is-32x32 {\n  height: 32px;\n  width: 32px;\n}\n\n.ame .image.is-48x48 {\n  height: 48px;\n  width: 48px;\n}\n\n.ame .image.is-64x64 {\n  height: 64px;\n  width: 64px;\n}\n\n.ame .image.is-96x96 {\n  height: 96px;\n  width: 96px;\n}\n\n.ame .image.is-128x128 {\n  height: 128px;\n  width: 128px;\n}\n\n.ame .notification {\n  background-color: whitesmoke;\n  border-radius: 3px;\n  padding: 1.25rem 2.5rem 1.25rem 1.5rem;\n  position: relative;\n}\n\n.ame .notification:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .notification a:not(.button) {\n  color: currentColor;\n  text-decoration: underline;\n}\n\n.ame .notification code, .ame .notification pre {\n  background: white;\n}\n\n.ame .notification pre code {\n  background: transparent;\n}\n\n.ame .notification > .delete {\n  position: absolute;\n  right: 0.5em;\n  top: 0.5em;\n}\n\n.ame .notification .title, .ame .notification .subtitle, .ame .notification .content {\n  color: inherit;\n}\n\n.ame .notification.is-white {\n  background-color: white;\n  color: #0a0a0a;\n}\n\n.ame .notification.is-black {\n  background-color: #0a0a0a;\n  color: white;\n}\n\n.ame .notification.is-light {\n  background-color: whitesmoke;\n  color: #363636;\n}\n\n.ame .notification.is-dark {\n  background-color: #363636;\n  color: whitesmoke;\n}\n\n.ame .notification.is-primary {\n  background-color: #00d1b2;\n  color: #fff;\n}\n\n.ame .notification.is-info {\n  background-color: #3273dc;\n  color: #fff;\n}\n\n.ame .notification.is-success {\n  background-color: #23d160;\n  color: #fff;\n}\n\n.ame .notification.is-warning {\n  background-color: #ffdd57;\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .notification.is-danger {\n  background-color: #ff3860;\n  color: #fff;\n}\n\n.ame .progress {\n  -moz-appearance: none;\n  -webkit-appearance: none;\n  border: none;\n  border-radius: 290486px;\n  display: block;\n  height: 1rem;\n  overflow: hidden;\n  padding: 0;\n  width: 100%;\n}\n\n.ame .progress:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .progress::-webkit-progress-bar {\n  background-color: #dbdbdb;\n}\n\n.ame .progress::-webkit-progress-value {\n  background-color: #4a4a4a;\n}\n\n.ame .progress::-moz-progress-bar {\n  background-color: #4a4a4a;\n}\n\n.ame .progress.is-white::-webkit-progress-value {\n  background-color: white;\n}\n\n.ame .progress.is-white::-moz-progress-bar {\n  background-color: white;\n}\n\n.ame .progress.is-black::-webkit-progress-value {\n  background-color: #0a0a0a;\n}\n\n.ame .progress.is-black::-moz-progress-bar {\n  background-color: #0a0a0a;\n}\n\n.ame .progress.is-light::-webkit-progress-value {\n  background-color: whitesmoke;\n}\n\n.ame .progress.is-light::-moz-progress-bar {\n  background-color: whitesmoke;\n}\n\n.ame .progress.is-dark::-webkit-progress-value {\n  background-color: #363636;\n}\n\n.ame .progress.is-dark::-moz-progress-bar {\n  background-color: #363636;\n}\n\n.ame .progress.is-primary::-webkit-progress-value {\n  background-color: #00d1b2;\n}\n\n.ame .progress.is-primary::-moz-progress-bar {\n  background-color: #00d1b2;\n}\n\n.ame .progress.is-info::-webkit-progress-value {\n  background-color: #3273dc;\n}\n\n.ame .progress.is-info::-moz-progress-bar {\n  background-color: #3273dc;\n}\n\n.ame .progress.is-success::-webkit-progress-value {\n  background-color: #23d160;\n}\n\n.ame .progress.is-success::-moz-progress-bar {\n  background-color: #23d160;\n}\n\n.ame .progress.is-warning::-webkit-progress-value {\n  background-color: #ffdd57;\n}\n\n.ame .progress.is-warning::-moz-progress-bar {\n  background-color: #ffdd57;\n}\n\n.ame .progress.is-danger::-webkit-progress-value {\n  background-color: #ff3860;\n}\n\n.ame .progress.is-danger::-moz-progress-bar {\n  background-color: #ff3860;\n}\n\n.ame .progress.is-small {\n  height: 0.75rem;\n}\n\n.ame .progress.is-medium {\n  height: 1.25rem;\n}\n\n.ame .progress.is-large {\n  height: 1.5rem;\n}\n\n.ame .table {\n  background-color: white;\n  color: #363636;\n  margin-bottom: 1.5rem;\n  width: 100%;\n}\n\n.ame .table td, .ame .table th {\n  border: 1px solid #dbdbdb;\n  border-width: 0 0 1px;\n  padding: 0.5em 0.75em;\n  vertical-align: top;\n}\n\n.ame .table td.is-narrow, .ame .table th.is-narrow {\n  white-space: nowrap;\n  width: 1%;\n}\n\n.ame .table th {\n  color: #363636;\n  text-align: left;\n}\n\n.ame .table tr:hover {\n  background-color: #fafafa;\n}\n\n.ame .table tr.is-selected {\n  background-color: #00d1b2;\n  color: #fff;\n}\n\n.ame .table tr.is-selected a, .ame .table tr.is-selected strong {\n  color: currentColor;\n}\n\n.ame .table tr.is-selected td, .ame .table tr.is-selected th {\n  border-color: #fff;\n  color: currentColor;\n}\n\n.ame .table thead td, .ame .table thead th {\n  border-width: 0 0 2px;\n  color: #7a7a7a;\n}\n\n.ame .table tfoot td, .ame .table tfoot th {\n  border-width: 2px 0 0;\n  color: #7a7a7a;\n}\n\n.ame ..table tbody tr:last-child td, .ame .\n.table tbody tr:last-child th {\n  border-bottom-width: 0;\n}\n\n.ame .table.is-bordered td, .ame .table.is-bordered th {\n  border-width: 1px;\n}\n\n.ame .table.is-bordered tr:last-child td, .ame .table.is-bordered tr:last-child th {\n  border-bottom-width: 1px;\n}\n\n.ame .table.is-narrow td, .ame .table.is-narrow th {\n  padding: 0.25em 0.5em;\n}\n\n.ame ..table.is-striped tbody tr:not(.is-selected):nth-child(even) {\n  background-color: #fafafa;\n}\n\n.ame ..table.is-striped tbody tr:not(.is-selected):nth-child(even):hover {\n  background-color: whitesmoke;\n}\n\n.ame .tag {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: whitesmoke;\n  border-radius: 290486px;\n  color: #4a4a4a;\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  font-size: 0.75rem;\n  height: 2em;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  line-height: 1.5;\n  padding-left: 0.875em;\n  padding-right: 0.875em;\n  white-space: nowrap;\n}\n\n.ame .tag .delete {\n  margin-left: 0.25em;\n  margin-right: -0.375em;\n}\n\n.ame .tag.is-white {\n  background-color: white;\n  color: #0a0a0a;\n}\n\n.ame .tag.is-black {\n  background-color: #0a0a0a;\n  color: white;\n}\n\n.ame .tag.is-light {\n  background-color: whitesmoke;\n  color: #363636;\n}\n\n.ame .tag.is-dark {\n  background-color: #363636;\n  color: whitesmoke;\n}\n\n.ame .tag.is-primary {\n  background-color: #00d1b2;\n  color: #fff;\n}\n\n.ame .tag.is-info {\n  background-color: #3273dc;\n  color: #fff;\n}\n\n.ame .tag.is-success {\n  background-color: #23d160;\n  color: #fff;\n}\n\n.ame .tag.is-warning {\n  background-color: #ffdd57;\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .tag.is-danger {\n  background-color: #ff3860;\n  color: #fff;\n}\n\n.ame .tag.is-medium {\n  font-size: 1rem;\n}\n\n.ame .tag.is-large {\n  font-size: 1.25rem;\n}\n\n.ame .title, .ame .subtitle {\n  word-break: break-word;\n}\n\n.ame .title:not(:last-child), .ame .subtitle:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .title em, .ame .title span, .ame .subtitle em, .ame .subtitle span {\n  font-weight: 300;\n}\n\n.ame .title strong, .ame .subtitle strong {\n  font-weight: 500;\n}\n\n.ame .title .tag, .ame .subtitle .tag {\n  vertical-align: middle;\n}\n\n.ame .title {\n  color: #363636;\n  font-size: 2rem;\n  font-weight: 300;\n  line-height: 1.125;\n}\n\n.ame .title strong {\n  color: inherit;\n}\n\n.ame .title + .highlight {\n  margin-top: -0.75rem;\n}\n\n.ame .title:not(.is-spaced) + .subtitle {\n  margin-top: -1.5rem;\n}\n\n.ame .title.is-1 {\n  font-size: 3rem;\n}\n\n.ame .title.is-2 {\n  font-size: 2.5rem;\n}\n\n.ame .title.is-3 {\n  font-size: 2rem;\n}\n\n.ame .title.is-4 {\n  font-size: 1.5rem;\n}\n\n.ame .title.is-5 {\n  font-size: 1.25rem;\n}\n\n.ame .title.is-6 {\n  font-size: 1rem;\n}\n\n.ame .subtitle {\n  color: #4a4a4a;\n  font-size: 1.25rem;\n  font-weight: 300;\n  line-height: 1.25;\n}\n\n.ame .subtitle strong {\n  color: #363636;\n}\n\n.ame .subtitle:not(.is-spaced) + .title {\n  margin-top: -1.5rem;\n}\n\n.ame .subtitle.is-1 {\n  font-size: 3rem;\n}\n\n.ame .subtitle.is-2 {\n  font-size: 2.5rem;\n}\n\n.ame .subtitle.is-3 {\n  font-size: 2rem;\n}\n\n.ame .subtitle.is-4 {\n  font-size: 1.5rem;\n}\n\n.ame .subtitle.is-5 {\n  font-size: 1.25rem;\n}\n\n.ame .subtitle.is-6 {\n  font-size: 1rem;\n}\n\n.ame .block:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .container {\n  position: relative;\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .container {\n    margin: 0 auto;\n    max-width: 960px;\n    width: 960px;\n  }\n  .ame .container.is-fluid {\n    margin: 0 20px;\n    max-width: none;\n    width: auto;\n  }\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .container {\n    max-width: 1152px;\n    width: 1152px;\n  }\n}\n\n@media screen and (min-width: 1384px) {\n  .ame .container {\n    max-width: 1344px;\n    width: 1344px;\n  }\n}\n\n.ame .delete {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  -moz-appearance: none;\n  -webkit-appearance: none;\n  background-color: rgba(10, 10, 10, 0.2);\n  border: none;\n  border-radius: 290486px;\n  cursor: pointer;\n  display: inline-block;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  font-size: 1rem;\n  height: 20px;\n  max-height: 20px;\n  max-width: 20px;\n  min-height: 20px;\n  min-width: 20px;\n  outline: none;\n  position: relative;\n  vertical-align: top;\n  width: 20px;\n}\n\n.ame .delete:before, .ame .delete:after {\n  background-color: white;\n  content: \"\";\n  display: block;\n  left: 50%;\n  position: absolute;\n  top: 50%;\n  -webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);\n          transform: translateX(-50%) translateY(-50%) rotate(45deg);\n  -webkit-transform-origin: center center;\n          transform-origin: center center;\n}\n\n.ame .delete:before {\n  height: 2px;\n  width: 50%;\n}\n\n.ame .delete:after {\n  height: 50%;\n  width: 2px;\n}\n\n.ame .delete:hover, .ame .delete:focus {\n  background-color: rgba(10, 10, 10, 0.3);\n}\n\n.ame .delete:active {\n  background-color: rgba(10, 10, 10, 0.4);\n}\n\n.ame .delete.is-small {\n  height: 16px;\n  max-height: 16px;\n  max-width: 16px;\n  min-height: 16px;\n  min-width: 16px;\n  width: 16px;\n}\n\n.ame .delete.is-medium {\n  height: 24px;\n  max-height: 24px;\n  max-width: 24px;\n  min-height: 24px;\n  min-width: 24px;\n  width: 24px;\n}\n\n.ame .delete.is-large {\n  height: 32px;\n  max-height: 32px;\n  max-width: 32px;\n  min-height: 32px;\n  min-width: 32px;\n  width: 32px;\n}\n\n.ame .fa {\n  font-size: 21px;\n  text-align: center;\n  vertical-align: top;\n}\n\n.ame .heading {\n  display: block;\n  font-size: 11px;\n  letter-spacing: 1px;\n  margin-bottom: 5px;\n  text-transform: uppercase;\n}\n\n.ame .highlight {\n  font-weight: 400;\n  max-width: 100%;\n  overflow: hidden;\n  padding: 0;\n}\n\n.ame .highlight:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .highlight pre {\n  overflow: auto;\n  max-width: 100%;\n}\n\n.ame .loader {\n  -webkit-animation: spinAround 500ms infinite linear;\n          animation: spinAround 500ms infinite linear;\n  border: 2px solid #dbdbdb;\n  border-radius: 290486px;\n  border-right-color: transparent;\n  border-top-color: transparent;\n  content: \"\";\n  display: block;\n  height: 1em;\n  position: relative;\n  width: 1em;\n}\n\n.ame .number {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: whitesmoke;\n  border-radius: 290486px;\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  font-size: 1.25rem;\n  height: 2em;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-right: 1.5rem;\n  min-width: 2.5em;\n  padding: 0.25rem 0.5rem;\n  text-align: center;\n  vertical-align: top;\n}\n\n.ame .breadcrumb {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  font-size: 1rem;\n  overflow: hidden;\n  overflow-x: auto;\n  white-space: nowrap;\n}\n\n.ame .breadcrumb:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .breadcrumb a {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  color: #7a7a7a;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  padding: 0.5em 0.75em;\n}\n\n.ame .breadcrumb a:hover {\n  color: #363636;\n}\n\n.ame .breadcrumb li {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame .breadcrumb li.is-active a {\n  color: #363636;\n  cursor: default;\n  pointer-events: none;\n}\n\n.ame .breadcrumb li + li:before {\n  color: #4a4a4a;\n  content: '/';\n}\n\n.ame .breadcrumb ul, .ame .breadcrumb ol {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n\n.ame .breadcrumb .icon:first-child {\n  margin-right: 0.5em;\n}\n\n.ame .breadcrumb .icon:last-child {\n  margin-left: 0.5em;\n}\n\n.ame .breadcrumb.is-centered ol, .ame .breadcrumb.is-centered ul {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n\n.ame .breadcrumb.is-right ol, .ame .breadcrumb.is-right ul {\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n\n.ame .breadcrumb.is-small {\n  font-size: 0.75rem;\n}\n\n.ame .breadcrumb.is-medium {\n  font-size: 1.25rem;\n}\n\n.ame .breadcrumb.is-large {\n  font-size: 1.5rem;\n}\n\n.ame .breadcrumb.has-arrow-separator li + li:before {\n  content: '\\2192';\n}\n\n.ame .breadcrumb.has-bullet-separator li + li:before {\n  content: '\\2022';\n}\n\n.ame .breadcrumb.has-dot-separator li + li:before {\n  content: '\\B7';\n}\n\n.ame .breadcrumb.has-succeeds-separator li + li:before {\n  content: '\\227B';\n}\n\n.ame .card-header {\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-shadow: 0 1px 2px rgba(10, 10, 10, 0.1);\n          box-shadow: 0 1px 2px rgba(10, 10, 10, 0.1);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame .card-header-title {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  color: #363636;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  font-weight: 700;\n  padding: 0.75rem;\n}\n\n.ame .card-header-icon {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  cursor: pointer;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  padding: 0.75rem;\n}\n\n.ame .card-image {\n  display: block;\n  position: relative;\n}\n\n.ame .card-content {\n  padding: 1.5rem;\n}\n\n.ame .card-footer {\n  border-top: 1px solid #dbdbdb;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame .card-footer-item {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-preferred-size: 0;\n      flex-basis: 0;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  padding: 0.75rem;\n}\n\n.ame .card-footer-item:not(:last-child) {\n  border-right: 1px solid #dbdbdb;\n}\n\n.ame .card {\n  background-color: white;\n  -webkit-box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1), 0 0 0 1px rgba(10, 10, 10, 0.1);\n          box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1), 0 0 0 1px rgba(10, 10, 10, 0.1);\n  color: #4a4a4a;\n  max-width: 100%;\n  position: relative;\n}\n\n.ame .card .media:not(:last-child) {\n  margin-bottom: 0.75rem;\n}\n\n.ame .level-item {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-preferred-size: auto;\n      flex-basis: auto;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n\n.ame .level-item .title, .ame .level-item .subtitle {\n  margin-bottom: 0;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .level-item:not(:last-child) {\n    margin-bottom: 0.75rem;\n  }\n}\n\n.ame .level-left, .ame .level-right {\n  -ms-flex-preferred-size: auto;\n      flex-basis: auto;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n}\n\n.ame .level-left .level-item:not(:last-child), .ame .level-right .level-item:not(:last-child) {\n  margin-right: 0.75rem;\n}\n\n.ame .level-left .level-item.is-flexible, .ame .level-right .level-item.is-flexible {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n}\n\n.ame .level-left {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .level-left + .level-right {\n    margin-top: 1.5rem;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .level-left {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n}\n\n.ame .level-right {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .level-right {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n}\n\n.ame .level {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n\n.ame .level:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .level code {\n  border-radius: 3px;\n}\n\n.ame .level img {\n  display: inline-block;\n  vertical-align: top;\n}\n\n.ame .level.is-mobile {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame .level.is-mobile .level-left, .ame .level.is-mobile .level-right {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame .level.is-mobile .level-left + .level-right {\n  margin-top: 0;\n}\n\n.ame .level.is-mobile .level-item:not(:last-child) {\n  margin-bottom: 0;\n}\n\n.ame .level.is-mobile .level-item:not(.is-narrow) {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .level {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n  .ame .level > .level-item:not(.is-narrow) {\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1;\n  }\n}\n\n.ame .media-left, .ame .media-right {\n  -ms-flex-preferred-size: auto;\n      flex-basis: auto;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n}\n\n.ame .media-left {\n  margin-right: 1rem;\n}\n\n.ame .media-right {\n  margin-left: 1rem;\n}\n\n.ame .media-content {\n  -ms-flex-preferred-size: auto;\n      flex-basis: auto;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 1;\n      flex-shrink: 1;\n  text-align: left;\n}\n\n.ame .media {\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  text-align: left;\n}\n\n.ame .media .content:not(:last-child) {\n  margin-bottom: 0.75rem;\n}\n\n.ame .media .media {\n  border-top: 1px solid rgba(219, 219, 219, 0.5);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding-top: 0.75rem;\n}\n\n.ame .media .media .content:not(:last-child), .ame .media .media .control:not(:last-child) {\n  margin-bottom: 0.5rem;\n}\n\n.ame .media .media .media {\n  padding-top: 0.5rem;\n}\n\n.ame .media .media .media + .media {\n  margin-top: 0.5rem;\n}\n\n.ame .media + .media {\n  border-top: 1px solid rgba(219, 219, 219, 0.5);\n  margin-top: 1rem;\n  padding-top: 1rem;\n}\n\n.ame .media.is-large + .media {\n  margin-top: 1.5rem;\n  padding-top: 1.5rem;\n}\n\n.ame .menu {\n  font-size: 1rem;\n}\n\n.ame .menu-list {\n  line-height: 1.25;\n}\n\n.ame .menu-list a {\n  border-radius: 2px;\n  color: #4a4a4a;\n  display: block;\n  padding: 0.5em 0.75em;\n}\n\n.ame .menu-list a:hover {\n  background-color: whitesmoke;\n  color: #00d1b2;\n}\n\n.ame .menu-list a.is-active {\n  background-color: #00d1b2;\n  color: #fff;\n}\n\n.ame .menu-list li ul {\n  border-left: 1px solid #dbdbdb;\n  margin: 0.75em;\n  padding-left: 0.75em;\n}\n\n.ame .menu-label {\n  color: #7a7a7a;\n  font-size: 0.8em;\n  letter-spacing: 0.1em;\n  text-transform: uppercase;\n}\n\n.ame .menu-label:not(:first-child) {\n  margin-top: 1em;\n}\n\n.ame .menu-label:not(:last-child) {\n  margin-bottom: 1em;\n}\n\n.ame .message {\n  background-color: whitesmoke;\n  border-radius: 3px;\n  font-size: 1rem;\n}\n\n.ame .message:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .message.is-white {\n  background-color: white;\n}\n\n.ame .message.is-white .message-header {\n  background-color: white;\n  color: #0a0a0a;\n}\n\n.ame ..message.is-white .message-body {\n  border-color: white;\n  color: #4d4d4d;\n}\n\n.ame .message.is-black {\n  background-color: #fafafa;\n}\n\n.ame .message.is-black .message-header {\n  background-color: #0a0a0a;\n  color: white;\n}\n\n.ame ..message.is-black .message-body {\n  border-color: #0a0a0a;\n  color: #090909;\n}\n\n.ame .message.is-light {\n  background-color: #fafafa;\n}\n\n.ame .message.is-light .message-header {\n  background-color: whitesmoke;\n  color: #363636;\n}\n\n.ame ..message.is-light .message-body {\n  border-color: whitesmoke;\n  color: #505050;\n}\n\n.ame .message.is-dark {\n  background-color: #fafafa;\n}\n\n.ame .message.is-dark .message-header {\n  background-color: #363636;\n  color: whitesmoke;\n}\n\n.ame ..message.is-dark .message-body {\n  border-color: #363636;\n  color: #2a2a2a;\n}\n\n.ame .message.is-primary {\n  background-color: #f5fffd;\n}\n\n.ame .message.is-primary .message-header {\n  background-color: #00d1b2;\n  color: #fff;\n}\n\n.ame ..message.is-primary .message-body {\n  border-color: #00d1b2;\n  color: #021310;\n}\n\n.ame .message.is-info {\n  background-color: #f6f9fe;\n}\n\n.ame .message.is-info .message-header {\n  background-color: #3273dc;\n  color: #fff;\n}\n\n.ame ..message.is-info .message-body {\n  border-color: #3273dc;\n  color: #22509a;\n}\n\n.ame .message.is-success {\n  background-color: #f6fef9;\n}\n\n.ame .message.is-success .message-header {\n  background-color: #23d160;\n  color: #fff;\n}\n\n.ame ..message.is-success .message-body {\n  border-color: #23d160;\n  color: #0e301a;\n}\n\n.ame .message.is-warning {\n  background-color: #fffdf5;\n}\n\n.ame .message.is-warning .message-header {\n  background-color: #ffdd57;\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame ..message.is-warning .message-body {\n  border-color: #ffdd57;\n  color: #3b3108;\n}\n\n.ame .message.is-danger {\n  background-color: #fff5f7;\n}\n\n.ame .message.is-danger .message-header {\n  background-color: #ff3860;\n  color: #fff;\n}\n\n.ame ..message.is-danger .message-body {\n  border-color: #ff3860;\n  color: #cd0930;\n}\n\n.ame .message-header {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: #4a4a4a;\n  border-radius: 3px 3px 0 0;\n  color: #fff;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  line-height: 1.25;\n  padding: 0.5em 0.75em;\n  position: relative;\n}\n\n.ame .message-header a, .ame .message-header strong {\n  color: inherit;\n}\n\n.ame .message-header a {\n  text-decoration: underline;\n}\n\n.ame .message-header .delete {\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  margin-left: 0.75em;\n}\n\n.ame ..message-header + .message-body {\n  border-top-left-radius: 0;\n  border-top-right-radius: 0;\n  border-top: none;\n}\n\n.ame ..message-body {\n  border: 1px solid #dbdbdb;\n  border-radius: 3px;\n  color: #4a4a4a;\n  padding: 1em 1.25em;\n}\n\n.ame ..message-body a, .ame .\n.message-body strong {\n  color: inherit;\n}\n\n.ame ..message-body a {\n  text-decoration: underline;\n}\n\n.ame ..message-body code, .ame .\n.message-body pre {\n  background: white;\n}\n\n.ame ..message-body pre code {\n  background: transparent;\n}\n\n.ame .modal-background {\n  bottom: 0;\n  left: 0;\n  position: absolute;\n  right: 0;\n  top: 0;\n  background-color: rgba(10, 10, 10, 0.86);\n}\n\n.ame .modal-content, .ame .modal-card {\n  margin: 0 20px;\n  max-height: calc(100vh - 160px);\n  overflow: auto;\n  position: relative;\n  width: 100%;\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .modal-content, .ame .modal-card {\n    margin: 0 auto;\n    max-height: calc(100vh - 40px);\n    width: 640px;\n  }\n}\n\n.ame .modal-close {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  -moz-appearance: none;\n  -webkit-appearance: none;\n  background-color: rgba(10, 10, 10, 0.2);\n  border: none;\n  border-radius: 290486px;\n  cursor: pointer;\n  display: inline-block;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  font-size: 1rem;\n  height: 20px;\n  max-height: 20px;\n  max-width: 20px;\n  min-height: 20px;\n  min-width: 20px;\n  outline: none;\n  position: relative;\n  vertical-align: top;\n  width: 20px;\n  background: none;\n  height: 40px;\n  position: fixed;\n  right: 20px;\n  top: 20px;\n  width: 40px;\n}\n\n.ame .modal-close:before, .ame .modal-close:after {\n  background-color: white;\n  content: \"\";\n  display: block;\n  left: 50%;\n  position: absolute;\n  top: 50%;\n  -webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);\n          transform: translateX(-50%) translateY(-50%) rotate(45deg);\n  -webkit-transform-origin: center center;\n          transform-origin: center center;\n}\n\n.ame .modal-close:before {\n  height: 2px;\n  width: 50%;\n}\n\n.ame .modal-close:after {\n  height: 50%;\n  width: 2px;\n}\n\n.ame .modal-close:hover, .ame .modal-close:focus {\n  background-color: rgba(10, 10, 10, 0.3);\n}\n\n.ame .modal-close:active {\n  background-color: rgba(10, 10, 10, 0.4);\n}\n\n.ame .modal-close.is-small {\n  height: 16px;\n  max-height: 16px;\n  max-width: 16px;\n  min-height: 16px;\n  min-width: 16px;\n  width: 16px;\n}\n\n.ame .modal-close.is-medium {\n  height: 24px;\n  max-height: 24px;\n  max-width: 24px;\n  min-height: 24px;\n  min-width: 24px;\n  width: 24px;\n}\n\n.ame .modal-close.is-large {\n  height: 32px;\n  max-height: 32px;\n  max-width: 32px;\n  min-height: 32px;\n  min-width: 32px;\n  width: 32px;\n}\n\n.ame .modal-card {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  max-height: calc(100vh - 40px);\n  overflow: hidden;\n}\n\n.ame .modal-card-head, .ame .modal-card-foot {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: whitesmoke;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  padding: 20px;\n  position: relative;\n}\n\n.ame .modal-card-head {\n  border-bottom: 1px solid #dbdbdb;\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px;\n}\n\n.ame .modal-card-title {\n  color: #363636;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  font-size: 1.5rem;\n  line-height: 1;\n}\n\n.ame .modal-card-foot {\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px;\n  border-top: 1px solid #dbdbdb;\n}\n\n.ame .modal-card-foot .button:not(:last-child) {\n  margin-right: 10px;\n}\n\n.ame ..modal-card-body {\n  -webkit-overflow-scrolling: touch;\n  background-color: white;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 1;\n      flex-shrink: 1;\n  overflow: auto;\n  padding: 20px;\n}\n\n.ame .modal {\n  bottom: 0;\n  left: 0;\n  position: absolute;\n  right: 0;\n  top: 0;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: none;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  overflow: hidden;\n  position: fixed;\n  z-index: 20;\n}\n\n.ame .modal.is-active {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame .nav-toggle {\n  cursor: pointer;\n  display: block;\n  height: 3.25rem;\n  position: relative;\n  width: 3.25rem;\n}\n\n.ame .nav-toggle span {\n  background-color: #4a4a4a;\n  display: block;\n  height: 1px;\n  left: 50%;\n  margin-left: -7px;\n  position: absolute;\n  top: 50%;\n  -webkit-transition: none 86ms ease-out;\n  transition: none 86ms ease-out;\n  -webkit-transition-property: background, left, opacity, -webkit-transform;\n  transition-property: background, left, opacity, -webkit-transform;\n  transition-property: background, left, opacity, transform;\n  transition-property: background, left, opacity, transform, -webkit-transform;\n  width: 15px;\n}\n\n.ame .nav-toggle span:nth-child(1) {\n  margin-top: -6px;\n}\n\n.ame .nav-toggle span:nth-child(2) {\n  margin-top: -1px;\n}\n\n.ame .nav-toggle span:nth-child(3) {\n  margin-top: 4px;\n}\n\n.ame .nav-toggle:hover {\n  background-color: whitesmoke;\n}\n\n.ame .nav-toggle.is-active span {\n  background-color: #00d1b2;\n}\n\n.ame .nav-toggle.is-active span:nth-child(1) {\n  margin-left: -5px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  -webkit-transform-origin: left top;\n          transform-origin: left top;\n}\n\n.ame .nav-toggle.is-active span:nth-child(2) {\n  opacity: 0;\n}\n\n.ame .nav-toggle.is-active span:nth-child(3) {\n  margin-left: -5px;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n  -webkit-transform-origin: left bottom;\n          transform-origin: left bottom;\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .nav-toggle {\n    display: none;\n  }\n}\n\n.ame .nav-item {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  font-size: 1rem;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  line-height: 1.5;\n  padding: 0.5rem 0.75rem;\n}\n\n.ame .nav-item a {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n}\n\n.ame .nav-item img {\n  max-height: 1.75rem;\n}\n\n.ame .nav-item .tag:first-child:not(:last-child) {\n  margin-right: 0.5rem;\n}\n\n.ame .nav-item .tag:last-child:not(:first-child) {\n  margin-left: 0.5rem;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .nav-item {\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n  }\n}\n\n.ame .nav-item a:not(.button), .ame a.nav-item:not(.button) {\n  color: #7a7a7a;\n}\n\n.ame .nav-item a:not(.button):hover, .ame a.nav-item:not(.button):hover {\n  color: #363636;\n}\n\n.ame .nav-item a:not(.button).is-active, .ame a.nav-item:not(.button).is-active {\n  color: #363636;\n}\n\n.ame .nav-item a:not(.button).is-tab, .ame a.nav-item:not(.button).is-tab {\n  border-bottom: 1px solid transparent;\n  border-top: 1px solid transparent;\n  padding-bottom: calc(0.75rem - 1px);\n  padding-left: 1rem;\n  padding-right: 1rem;\n  padding-top: calc(0.75rem - 1px);\n}\n\n.ame .nav-item a:not(.button).is-tab:hover, .ame a.nav-item:not(.button).is-tab:hover {\n  border-bottom-color: #00d1b2;\n  border-top-color: transparent;\n}\n\n.ame .nav-item a:not(.button).is-tab.is-active, .ame a.nav-item:not(.button).is-tab.is-active {\n  border-bottom: 3px solid #00d1b2;\n  color: #00d1b2;\n  padding-bottom: calc(0.75rem - 3px);\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .nav-item a:not(.button).is-brand, .ame a.nav-item:not(.button).is-brand {\n    padding-left: 0;\n  }\n}\n\n.ame .nav-left, .ame .nav-right {\n  -webkit-overflow-scrolling: touch;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  max-width: 100%;\n  overflow: auto;\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .nav-left, .ame .nav-right {\n    -ms-flex-preferred-size: 0;\n        flex-basis: 0;\n  }\n}\n\n.ame .nav-left {\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  white-space: nowrap;\n}\n\n.ame .nav-right {\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n\n.ame .nav-center {\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .nav-menu.nav-right {\n    background-color: white;\n    -webkit-box-shadow: 0 4px 7px rgba(10, 10, 10, 0.1);\n            box-shadow: 0 4px 7px rgba(10, 10, 10, 0.1);\n    left: 0;\n    display: none;\n    right: 0;\n    top: 100%;\n    position: absolute;\n  }\n  .ame .nav-menu.nav-right .nav-item {\n    border-top: 1px solid rgba(219, 219, 219, 0.5);\n    padding: 0.75rem;\n  }\n  .ame .nav-menu.nav-right.is-active {\n    display: block;\n  }\n}\n\n.ame .nav {\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  background-color: white;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: 3.25rem;\n  position: relative;\n  text-align: center;\n  z-index: 10;\n}\n\n.ame .nav > .container {\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 3.25rem;\n  width: 100%;\n}\n\n.ame .nav.has-shadow {\n  -webkit-box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1);\n          box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1);\n}\n\n.ame .navbar {\n  background-color: white;\n  min-height: 3.25rem;\n  position: relative;\n}\n\n.ame .navbar-brand {\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  height: 3.25rem;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame .navbar-burger {\n  cursor: pointer;\n  display: block;\n  height: 3.25rem;\n  position: relative;\n  width: 3.25rem;\n  margin-left: auto;\n}\n\n.ame .navbar-burger span {\n  background-color: #4a4a4a;\n  display: block;\n  height: 1px;\n  left: 50%;\n  margin-left: -7px;\n  position: absolute;\n  top: 50%;\n  -webkit-transition: none 86ms ease-out;\n  transition: none 86ms ease-out;\n  -webkit-transition-property: background, left, opacity, -webkit-transform;\n  transition-property: background, left, opacity, -webkit-transform;\n  transition-property: background, left, opacity, transform;\n  transition-property: background, left, opacity, transform, -webkit-transform;\n  width: 15px;\n}\n\n.ame .navbar-burger span:nth-child(1) {\n  margin-top: -6px;\n}\n\n.ame .navbar-burger span:nth-child(2) {\n  margin-top: -1px;\n}\n\n.ame .navbar-burger span:nth-child(3) {\n  margin-top: 4px;\n}\n\n.ame .navbar-burger:hover {\n  background-color: whitesmoke;\n}\n\n.ame .navbar-burger.is-active span {\n  background-color: #00d1b2;\n}\n\n.ame .navbar-burger.is-active span:nth-child(1) {\n  margin-left: -5px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  -webkit-transform-origin: left top;\n          transform-origin: left top;\n}\n\n.ame .navbar-burger.is-active span:nth-child(2) {\n  opacity: 0;\n}\n\n.ame .navbar-burger.is-active span:nth-child(3) {\n  margin-left: -5px;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n  -webkit-transform-origin: left bottom;\n          transform-origin: left bottom;\n}\n\n.ame .navbar-menu {\n  display: none;\n}\n\n.ame .navbar-item, .ame .navbar-link {\n  color: #4a4a4a;\n  display: block;\n  line-height: 1.5;\n  padding: 0.5rem 1rem;\n  position: relative;\n}\n\n.ame a.navbar-item:hover, .ame a.navbar-item.is-active, .ame .navbar-link:hover, .ame .navbar-link.is-active {\n  background-color: whitesmoke;\n  color: #0a0a0a;\n}\n\n.ame .navbar-item {\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n}\n\n.ame .navbar-item img {\n  max-height: 1.75rem;\n}\n\n.ame .navbar-item.has-dropdown {\n  padding: 0;\n}\n\n.ame .navbar-content {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 1;\n      flex-shrink: 1;\n}\n\n.ame .navbar-link {\n  padding-right: 2.5em;\n}\n\n.ame .navbar-dropdown {\n  font-size: 0.875rem;\n  padding-bottom: 0.5rem;\n  padding-top: 0.5rem;\n}\n\n.ame .navbar-dropdown .navbar-item {\n  padding-left: 1.5rem;\n  padding-right: 1.5rem;\n}\n\n.ame .navbar-divider {\n  background-color: #dbdbdb;\n  border: none;\n  display: none;\n  height: 1px;\n  margin: 0.5rem 0;\n}\n\n@media screen and (max-width: 999px) {\n  .ame .navbar-brand .navbar-item {\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n  .ame .navbar-menu {\n    -webkit-box-shadow: 0 8px 16px rgba(10, 10, 10, 0.1);\n            box-shadow: 0 8px 16px rgba(10, 10, 10, 0.1);\n    padding: 0.5rem 0;\n  }\n  .ame .navbar-menu.is-active {\n    display: block;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .navbar, .ame .navbar-menu, .ame .navbar-start, .ame .navbar-end {\n    -webkit-box-align: stretch;\n        -ms-flex-align: stretch;\n            align-items: stretch;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n  .ame .navbar {\n    height: 3.25rem;\n  }\n  .ame .navbar.is-transparent a.navbar-item:hover, .ame .navbar.is-transparent a.navbar-item.is-active, .ame .navbar.is-transparent .navbar-link:hover, .ame .navbar.is-transparent .navbar-link.is-active {\n    background-color: transparent;\n  }\n  .ame .navbar.is-transparent .navbar-item.has-dropdown.is-active .navbar-link, .ame .navbar.is-transparent .navbar-item.has-dropdown.is-hoverable:hover .navbar-link {\n    background-color: transparent;\n  }\n  .ame .navbar.is-transparent .navbar-dropdown a.navbar-item:hover {\n    background-color: whitesmoke;\n    color: #0a0a0a;\n  }\n  .ame .navbar.is-transparent .navbar-dropdown a.navbar-item.is-active {\n    background-color: whitesmoke;\n    color: #00d1b2;\n  }\n  .ame .navbar-burger {\n    display: none;\n  }\n  .ame .navbar-item, .ame .navbar-link {\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n  .ame .navbar-item.has-dropdown {\n    -webkit-box-align: stretch;\n        -ms-flex-align: stretch;\n            align-items: stretch;\n  }\n  .ame .navbar-item.is-active .navbar-dropdown, .ame .navbar-item.is-hoverable:hover .navbar-dropdown {\n    display: block;\n  }\n  .ame .navbar-item.is-active .navbar-dropdown.is-boxed, .ame .navbar-item.is-hoverable:hover .navbar-dropdown.is-boxed {\n    opacity: 1;\n    pointer-events: auto;\n    -webkit-transform: translateY(0);\n            transform: translateY(0);\n  }\n  .ame .navbar-link::after {\n    border: 1px solid #00d1b2;\n    border-right: 0;\n    border-top: 0;\n    content: \" \";\n    display: block;\n    height: 0.5em;\n    pointer-events: none;\n    position: absolute;\n    -webkit-transform: rotate(-45deg);\n            transform: rotate(-45deg);\n    width: 0.5em;\n    margin-top: -0.375em;\n    right: 1.125em;\n    top: 50%;\n  }\n  .ame .navbar-menu {\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1;\n    -ms-flex-negative: 0;\n        flex-shrink: 0;\n  }\n  .ame .navbar-start {\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n    margin-right: auto;\n  }\n  .ame .navbar-end {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end;\n    margin-left: auto;\n  }\n  .ame .navbar-dropdown {\n    background-color: white;\n    border-bottom-left-radius: 5px;\n    border-bottom-right-radius: 5px;\n    border-top: 1px solid #dbdbdb;\n    -webkit-box-shadow: 0 8px 8px rgba(10, 10, 10, 0.1);\n            box-shadow: 0 8px 8px rgba(10, 10, 10, 0.1);\n    display: none;\n    font-size: 0.875rem;\n    left: 0;\n    min-width: 100%;\n    position: absolute;\n    top: 100%;\n    z-index: 20;\n  }\n  .ame .navbar-dropdown .navbar-item {\n    padding: 0.375rem 1rem;\n    white-space: nowrap;\n  }\n  .ame .navbar-dropdown a.navbar-item {\n    padding-right: 3rem;\n  }\n  .ame .navbar-dropdown a.navbar-item:hover {\n    background-color: whitesmoke;\n    color: #0a0a0a;\n  }\n  .ame .navbar-dropdown a.navbar-item.is-active {\n    background-color: whitesmoke;\n    color: #00d1b2;\n  }\n  .ame .navbar-dropdown.is-boxed {\n    border-radius: 5px;\n    border-top: none;\n    -webkit-box-shadow: 0 8px 8px rgba(10, 10, 10, 0.1), 0 0 0 1px rgba(10, 10, 10, 0.1);\n            box-shadow: 0 8px 8px rgba(10, 10, 10, 0.1), 0 0 0 1px rgba(10, 10, 10, 0.1);\n    display: block;\n    opacity: 0;\n    pointer-events: none;\n    top: calc(100% + (-4px));\n    -webkit-transform: translateY(-5px);\n            transform: translateY(-5px);\n    -webkit-transition-duration: 86ms;\n            transition-duration: 86ms;\n    -webkit-transition-property: opacity, -webkit-transform;\n    transition-property: opacity, -webkit-transform;\n    transition-property: opacity, transform;\n    transition-property: opacity, transform, -webkit-transform;\n  }\n  .ame .navbar-divider {\n    display: block;\n  }\n  .ame .container > .navbar {\n    margin-left: -1rem;\n    margin-right: -1rem;\n  }\n  .ame a.navbar-item.is-active, .ame .navbar-link.is-active {\n    color: #0a0a0a;\n  }\n  .ame a.navbar-item.is-active:not(:hover), .ame .navbar-link.is-active:not(:hover) {\n    background-color: transparent;\n  }\n  .ame .navbar-item.has-dropdown:hover .navbar-link, .ame .navbar-item.has-dropdown.is-active .navbar-link {\n    background-color: whitesmoke;\n  }\n}\n\n.ame .pagination {\n  font-size: 1rem;\n  margin: -0.25rem;\n}\n\n.ame .pagination.is-small {\n  font-size: 0.75rem;\n}\n\n.ame .pagination.is-medium {\n  font-size: 1.25rem;\n}\n\n.ame .pagination.is-large {\n  font-size: 1.5rem;\n}\n\n.ame .pagination, .ame .pagination-list {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  text-align: center;\n}\n\n.ame .pagination-previous, .ame .pagination-next, .ame .pagination-link, .ame .pagination-ellipsis {\n  -moz-appearance: none;\n  -webkit-appearance: none;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border: 1px solid transparent;\n  border-radius: 3px;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  font-size: 1rem;\n  height: 2.25em;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  line-height: 1.5;\n  padding-bottom: calc(0.375em - 1px);\n  padding-left: calc(0.625em - 1px);\n  padding-right: calc(0.625em - 1px);\n  padding-top: calc(0.375em - 1px);\n  position: relative;\n  vertical-align: top;\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  font-size: 1em;\n  padding-left: 0.5em;\n  padding-right: 0.5em;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin: 0.25rem;\n  text-align: center;\n}\n\n.ame .pagination-previous:focus, .ame .pagination-previous.is-focused, .ame .pagination-previous:active, .ame .pagination-previous.is-active, .ame .pagination-next:focus, .ame .pagination-next.is-focused, .ame .pagination-next:active, .ame .pagination-next.is-active, .ame .pagination-link:focus, .ame .pagination-link.is-focused, .ame .pagination-link:active, .ame .pagination-link.is-active, .ame .pagination-ellipsis:focus, .ame .pagination-ellipsis.is-focused, .ame .pagination-ellipsis:active, .ame .pagination-ellipsis.is-active {\n  outline: none;\n}\n\n.ame .pagination-previous[disabled], .ame .pagination-next[disabled], .ame .pagination-link[disabled], .ame .pagination-ellipsis[disabled] {\n  cursor: not-allowed;\n}\n\n.ame .pagination-previous, .ame .pagination-next, .ame .pagination-link {\n  border-color: #dbdbdb;\n  min-width: 2.25em;\n}\n\n.ame .pagination-previous:hover, .ame .pagination-next:hover, .ame .pagination-link:hover {\n  border-color: #b5b5b5;\n  color: #363636;\n}\n\n.ame .pagination-previous:focus, .ame .pagination-next:focus, .ame .pagination-link:focus {\n  border-color: #00d1b2;\n}\n\n.ame .pagination-previous:active, .ame .pagination-next:active, .ame .pagination-link:active {\n  -webkit-box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n          box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.2);\n}\n\n.ame .pagination-previous[disabled], .ame .pagination-next[disabled], .ame .pagination-link[disabled] {\n  background-color: #dbdbdb;\n  border-color: #dbdbdb;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  color: #7a7a7a;\n  opacity: 0.5;\n}\n\n.ame .pagination-previous, .ame .pagination-next {\n  padding-left: 0.75em;\n  padding-right: 0.75em;\n  white-space: nowrap;\n}\n\n.ame .pagination-link.is-current {\n  background-color: #00d1b2;\n  border-color: #00d1b2;\n  color: #fff;\n}\n\n.ame .pagination-ellipsis {\n  color: #b5b5b5;\n  pointer-events: none;\n}\n\n.ame .pagination-list {\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .pagination {\n    -ms-flex-wrap: wrap;\n        flex-wrap: wrap;\n  }\n  .ame .pagination-previous, .ame .pagination-next {\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1;\n    -ms-flex-negative: 1;\n        flex-shrink: 1;\n  }\n  .ame .pagination-list li {\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1;\n    -ms-flex-negative: 1;\n        flex-shrink: 1;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .pagination-list {\n    -webkit-box-flex: 1;\n        -ms-flex-positive: 1;\n            flex-grow: 1;\n    -ms-flex-negative: 1;\n        flex-shrink: 1;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n    -webkit-box-ordinal-group: 2;\n        -ms-flex-order: 1;\n            order: 1;\n  }\n  .ame .pagination-previous {\n    -webkit-box-ordinal-group: 3;\n        -ms-flex-order: 2;\n            order: 2;\n  }\n  .ame .pagination-next {\n    -webkit-box-ordinal-group: 4;\n        -ms-flex-order: 3;\n            order: 3;\n  }\n  .ame .pagination {\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n  }\n  .ame .pagination.is-centered .pagination-previous {\n    -webkit-box-ordinal-group: 2;\n        -ms-flex-order: 1;\n            order: 1;\n  }\n  .ame .pagination.is-centered .pagination-list {\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-ordinal-group: 3;\n        -ms-flex-order: 2;\n            order: 2;\n  }\n  .ame .pagination.is-centered .pagination-next {\n    -webkit-box-ordinal-group: 4;\n        -ms-flex-order: 3;\n            order: 3;\n  }\n  .ame .pagination.is-right .pagination-previous {\n    -webkit-box-ordinal-group: 2;\n        -ms-flex-order: 1;\n            order: 1;\n  }\n  .ame .pagination.is-right .pagination-next {\n    -webkit-box-ordinal-group: 3;\n        -ms-flex-order: 2;\n            order: 2;\n  }\n  .ame .pagination.is-right .pagination-list {\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end;\n    -webkit-box-ordinal-group: 4;\n        -ms-flex-order: 3;\n            order: 3;\n  }\n}\n\n.ame .panel {\n  font-size: 1rem;\n}\n\n.ame .panel:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .panel-heading, .ame .panel-tabs, .ame .panel-block {\n  border-bottom: 1px solid #dbdbdb;\n  border-left: 1px solid #dbdbdb;\n  border-right: 1px solid #dbdbdb;\n}\n\n.ame .panel-heading:first-child, .ame .panel-tabs:first-child, .ame .panel-block:first-child {\n  border-top: 1px solid #dbdbdb;\n}\n\n.ame .panel-heading {\n  background-color: whitesmoke;\n  border-radius: 3px 3px 0 0;\n  color: #363636;\n  font-size: 1.25em;\n  font-weight: 300;\n  line-height: 1.25;\n  padding: 0.5em 0.75em;\n}\n\n.ame .panel-tabs {\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  font-size: 0.875em;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n\n.ame .panel-tabs a {\n  border-bottom: 1px solid #dbdbdb;\n  margin-bottom: -1px;\n  padding: 0.5em;\n}\n\n.ame .panel-tabs a.is-active {\n  border-bottom-color: #4a4a4a;\n  color: #363636;\n}\n\n.ame .panel-list a {\n  color: #4a4a4a;\n}\n\n.ame .panel-list a:hover {\n  color: #00d1b2;\n}\n\n.ame .panel-block {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  color: #363636;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  padding: 0.5em 0.75em;\n}\n\n.ame .panel-block input[type=\"checkbox\"] {\n  margin-right: 0.75em;\n}\n\n.ame .panel-block > .control {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 1;\n      flex-shrink: 1;\n  width: 100%;\n}\n\n.ame .panel-block.is-wrapped {\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n}\n\n.ame .panel-block.is-active {\n  border-left-color: #00d1b2;\n  color: #363636;\n}\n\n.ame .panel-block.is-active .panel-icon {\n  color: #00d1b2;\n}\n\n.ame a.panel-block, .ame label.panel-block {\n  cursor: pointer;\n}\n\n.ame a.panel-block:hover, .ame label.panel-block:hover {\n  background-color: whitesmoke;\n}\n\n.ame .panel-icon {\n  display: inline-block;\n  font-size: 14px;\n  height: 1em;\n  line-height: 1em;\n  text-align: center;\n  vertical-align: top;\n  width: 1em;\n  color: #7a7a7a;\n  margin-right: 0.75em;\n}\n\n.ame .panel-icon .fa {\n  font-size: inherit;\n  line-height: inherit;\n}\n\n.ame .tabs {\n  -webkit-overflow-scrolling: touch;\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  font-size: 1rem;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  overflow: hidden;\n  overflow-x: auto;\n  white-space: nowrap;\n}\n\n.ame .tabs:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .tabs a {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-bottom: 1px solid #dbdbdb;\n  color: #4a4a4a;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: -1px;\n  padding: 0.5em 1em;\n  vertical-align: top;\n}\n\n.ame .tabs a:hover {\n  border-bottom-color: #363636;\n  color: #363636;\n}\n\n.ame .tabs li {\n  display: block;\n}\n\n.ame .tabs li.is-active a {\n  border-bottom-color: #00d1b2;\n  color: #00d1b2;\n}\n\n.ame .tabs ul {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-bottom: 1px solid #dbdbdb;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n\n.ame .tabs ul.is-left {\n  padding-right: 0.75em;\n}\n\n.ame .tabs ul.is-center {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  padding-left: 0.75em;\n  padding-right: 0.75em;\n}\n\n.ame .tabs ul.is-right {\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  padding-left: 0.75em;\n}\n\n.ame .tabs .icon:first-child {\n  margin-right: 0.5em;\n}\n\n.ame .tabs .icon:last-child {\n  margin-left: 0.5em;\n}\n\n.ame .tabs.is-centered ul {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n\n.ame .tabs.is-right ul {\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n\n.ame .tabs.is-boxed a {\n  border: 1px solid transparent;\n  border-radius: 3px 3px 0 0;\n}\n\n.ame .tabs.is-boxed a:hover {\n  background-color: whitesmoke;\n  border-bottom-color: #dbdbdb;\n}\n\n.ame .tabs.is-boxed li.is-active a {\n  background-color: white;\n  border-color: #dbdbdb;\n  border-bottom-color: transparent !important;\n}\n\n.ame .tabs.is-fullwidth li {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n}\n\n.ame .tabs.is-toggle a {\n  border: 1px solid #dbdbdb;\n  margin-bottom: 0;\n  position: relative;\n}\n\n.ame .tabs.is-toggle a:hover {\n  background-color: whitesmoke;\n  border-color: #b5b5b5;\n  z-index: 2;\n}\n\n.ame .tabs.is-toggle li + li {\n  margin-left: -1px;\n}\n\n.ame .tabs.is-toggle li:first-child a {\n  border-radius: 3px 0 0 3px;\n}\n\n.ame .tabs.is-toggle li:last-child a {\n  border-radius: 0 3px 3px 0;\n}\n\n.ame .tabs.is-toggle li.is-active a {\n  background-color: #00d1b2;\n  border-color: #00d1b2;\n  color: #fff;\n  z-index: 1;\n}\n\n.ame .tabs.is-toggle ul {\n  border-bottom: none;\n}\n\n.ame .tabs.is-small {\n  font-size: 0.75rem;\n}\n\n.ame .tabs.is-medium {\n  font-size: 1.25rem;\n}\n\n.ame .tabs.is-large {\n  font-size: 1.5rem;\n}\n\n.ame .column {\n  display: block;\n  -ms-flex-preferred-size: 0;\n      flex-basis: 0;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 1;\n      flex-shrink: 1;\n  padding: 0.75rem;\n}\n\n.ame .columns.is-mobile > .column.is-narrow {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n}\n\n.ame .columns.is-mobile > .column.is-full {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 100%;\n}\n\n.ame .columns.is-mobile > .column.is-three-quarters {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 75%;\n}\n\n.ame .columns.is-mobile > .column.is-two-thirds {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 66.6666%;\n}\n\n.ame .columns.is-mobile > .column.is-half {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 50%;\n}\n\n.ame .columns.is-mobile > .column.is-one-third {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 33.3333%;\n}\n\n.ame .columns.is-mobile > .column.is-one-quarter {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 25%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-three-quarters {\n  margin-left: 75%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-two-thirds {\n  margin-left: 66.6666%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-half {\n  margin-left: 50%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-one-third {\n  margin-left: 33.3333%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-one-quarter {\n  margin-left: 25%;\n}\n\n.ame .columns.is-mobile > .column.is-1 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 8.33333%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-1 {\n  margin-left: 8.33333%;\n}\n\n.ame .columns.is-mobile > .column.is-2 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 16.66667%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-2 {\n  margin-left: 16.66667%;\n}\n\n.ame .columns.is-mobile > .column.is-3 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 25%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-3 {\n  margin-left: 25%;\n}\n\n.ame .columns.is-mobile > .column.is-4 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 33.33333%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-4 {\n  margin-left: 33.33333%;\n}\n\n.ame .columns.is-mobile > .column.is-5 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 41.66667%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-5 {\n  margin-left: 41.66667%;\n}\n\n.ame .columns.is-mobile > .column.is-6 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 50%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-6 {\n  margin-left: 50%;\n}\n\n.ame .columns.is-mobile > .column.is-7 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 58.33333%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-7 {\n  margin-left: 58.33333%;\n}\n\n.ame .columns.is-mobile > .column.is-8 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 66.66667%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-8 {\n  margin-left: 66.66667%;\n}\n\n.ame .columns.is-mobile > .column.is-9 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 75%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-9 {\n  margin-left: 75%;\n}\n\n.ame .columns.is-mobile > .column.is-10 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 83.33333%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-10 {\n  margin-left: 83.33333%;\n}\n\n.ame .columns.is-mobile > .column.is-11 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 91.66667%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-11 {\n  margin-left: 91.66667%;\n}\n\n.ame .columns.is-mobile > .column.is-12 {\n  -webkit-box-flex: 0;\n      -ms-flex: none;\n          flex: none;\n  width: 100%;\n}\n\n.ame .columns.is-mobile > .column.is-offset-12 {\n  margin-left: 100%;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .column.is-narrow-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n  }\n  .ame .column.is-full-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-three-quarters-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-two-thirds-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.6666%;\n  }\n  .ame .column.is-half-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-one-third-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.3333%;\n  }\n  .ame .column.is-one-quarter-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-three-quarters-mobile {\n    margin-left: 75%;\n  }\n  .ame .column.is-offset-two-thirds-mobile {\n    margin-left: 66.6666%;\n  }\n  .ame .column.is-offset-half-mobile {\n    margin-left: 50%;\n  }\n  .ame .column.is-offset-one-third-mobile {\n    margin-left: 33.3333%;\n  }\n  .ame .column.is-offset-one-quarter-mobile {\n    margin-left: 25%;\n  }\n  .ame .column.is-1-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 8.33333%;\n  }\n  .ame .column.is-offset-1-mobile {\n    margin-left: 8.33333%;\n  }\n  .ame .column.is-2-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 16.66667%;\n  }\n  .ame .column.is-offset-2-mobile {\n    margin-left: 16.66667%;\n  }\n  .ame .column.is-3-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-3-mobile {\n    margin-left: 25%;\n  }\n  .ame .column.is-4-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.33333%;\n  }\n  .ame .column.is-offset-4-mobile {\n    margin-left: 33.33333%;\n  }\n  .ame .column.is-5-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 41.66667%;\n  }\n  .ame .column.is-offset-5-mobile {\n    margin-left: 41.66667%;\n  }\n  .ame .column.is-6-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-offset-6-mobile {\n    margin-left: 50%;\n  }\n  .ame .column.is-7-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 58.33333%;\n  }\n  .ame .column.is-offset-7-mobile {\n    margin-left: 58.33333%;\n  }\n  .ame .column.is-8-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.66667%;\n  }\n  .ame .column.is-offset-8-mobile {\n    margin-left: 66.66667%;\n  }\n  .ame .column.is-9-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-offset-9-mobile {\n    margin-left: 75%;\n  }\n  .ame .column.is-10-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 83.33333%;\n  }\n  .ame .column.is-offset-10-mobile {\n    margin-left: 83.33333%;\n  }\n  .ame .column.is-11-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 91.66667%;\n  }\n  .ame .column.is-offset-11-mobile {\n    margin-left: 91.66667%;\n  }\n  .ame .column.is-12-mobile {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-offset-12-mobile {\n    margin-left: 100%;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .column.is-narrow, .ame .column.is-narrow-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n  }\n  .ame .column.is-full, .ame .column.is-full-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-three-quarters, .ame .column.is-three-quarters-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-two-thirds, .ame .column.is-two-thirds-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.6666%;\n  }\n  .ame .column.is-half, .ame .column.is-half-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-one-third, .ame .column.is-one-third-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.3333%;\n  }\n  .ame .column.is-one-quarter, .ame .column.is-one-quarter-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-three-quarters, .ame .column.is-offset-three-quarters-tablet {\n    margin-left: 75%;\n  }\n  .ame .column.is-offset-two-thirds, .ame .column.is-offset-two-thirds-tablet {\n    margin-left: 66.6666%;\n  }\n  .ame .column.is-offset-half, .ame .column.is-offset-half-tablet {\n    margin-left: 50%;\n  }\n  .ame .column.is-offset-one-third, .ame .column.is-offset-one-third-tablet {\n    margin-left: 33.3333%;\n  }\n  .ame .column.is-offset-one-quarter, .ame .column.is-offset-one-quarter-tablet {\n    margin-left: 25%;\n  }\n  .ame .column.is-1, .ame .column.is-1-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 8.33333%;\n  }\n  .ame .column.is-offset-1, .ame .column.is-offset-1-tablet {\n    margin-left: 8.33333%;\n  }\n  .ame .column.is-2, .ame .column.is-2-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 16.66667%;\n  }\n  .ame .column.is-offset-2, .ame .column.is-offset-2-tablet {\n    margin-left: 16.66667%;\n  }\n  .ame .column.is-3, .ame .column.is-3-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-3, .ame .column.is-offset-3-tablet {\n    margin-left: 25%;\n  }\n  .ame .column.is-4, .ame .column.is-4-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.33333%;\n  }\n  .ame .column.is-offset-4, .ame .column.is-offset-4-tablet {\n    margin-left: 33.33333%;\n  }\n  .ame .column.is-5, .ame .column.is-5-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 41.66667%;\n  }\n  .ame .column.is-offset-5, .ame .column.is-offset-5-tablet {\n    margin-left: 41.66667%;\n  }\n  .ame .column.is-6, .ame .column.is-6-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-offset-6, .ame .column.is-offset-6-tablet {\n    margin-left: 50%;\n  }\n  .ame .column.is-7, .ame .column.is-7-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 58.33333%;\n  }\n  .ame .column.is-offset-7, .ame .column.is-offset-7-tablet {\n    margin-left: 58.33333%;\n  }\n  .ame .column.is-8, .ame .column.is-8-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.66667%;\n  }\n  .ame .column.is-offset-8, .ame .column.is-offset-8-tablet {\n    margin-left: 66.66667%;\n  }\n  .ame .column.is-9, .ame .column.is-9-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-offset-9, .ame .column.is-offset-9-tablet {\n    margin-left: 75%;\n  }\n  .ame .column.is-10, .ame .column.is-10-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 83.33333%;\n  }\n  .ame .column.is-offset-10, .ame .column.is-offset-10-tablet {\n    margin-left: 83.33333%;\n  }\n  .ame .column.is-11, .ame .column.is-11-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 91.66667%;\n  }\n  .ame .column.is-offset-11, .ame .column.is-offset-11-tablet {\n    margin-left: 91.66667%;\n  }\n  .ame .column.is-12, .ame .column.is-12-tablet {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-offset-12, .ame .column.is-offset-12-tablet {\n    margin-left: 100%;\n  }\n}\n\n@media screen and (max-width: 999px) {\n  .ame .column.is-narrow-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n  }\n  .ame .column.is-full-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-three-quarters-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-two-thirds-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.6666%;\n  }\n  .ame .column.is-half-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-one-third-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.3333%;\n  }\n  .ame .column.is-one-quarter-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-three-quarters-touch {\n    margin-left: 75%;\n  }\n  .ame .column.is-offset-two-thirds-touch {\n    margin-left: 66.6666%;\n  }\n  .ame .column.is-offset-half-touch {\n    margin-left: 50%;\n  }\n  .ame .column.is-offset-one-third-touch {\n    margin-left: 33.3333%;\n  }\n  .ame .column.is-offset-one-quarter-touch {\n    margin-left: 25%;\n  }\n  .ame .column.is-1-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 8.33333%;\n  }\n  .ame .column.is-offset-1-touch {\n    margin-left: 8.33333%;\n  }\n  .ame .column.is-2-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 16.66667%;\n  }\n  .ame .column.is-offset-2-touch {\n    margin-left: 16.66667%;\n  }\n  .ame .column.is-3-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-3-touch {\n    margin-left: 25%;\n  }\n  .ame .column.is-4-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.33333%;\n  }\n  .ame .column.is-offset-4-touch {\n    margin-left: 33.33333%;\n  }\n  .ame .column.is-5-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 41.66667%;\n  }\n  .ame .column.is-offset-5-touch {\n    margin-left: 41.66667%;\n  }\n  .ame .column.is-6-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-offset-6-touch {\n    margin-left: 50%;\n  }\n  .ame .column.is-7-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 58.33333%;\n  }\n  .ame .column.is-offset-7-touch {\n    margin-left: 58.33333%;\n  }\n  .ame .column.is-8-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.66667%;\n  }\n  .ame .column.is-offset-8-touch {\n    margin-left: 66.66667%;\n  }\n  .ame .column.is-9-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-offset-9-touch {\n    margin-left: 75%;\n  }\n  .ame .column.is-10-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 83.33333%;\n  }\n  .ame .column.is-offset-10-touch {\n    margin-left: 83.33333%;\n  }\n  .ame .column.is-11-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 91.66667%;\n  }\n  .ame .column.is-offset-11-touch {\n    margin-left: 91.66667%;\n  }\n  .ame .column.is-12-touch {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-offset-12-touch {\n    margin-left: 100%;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .column.is-narrow-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n  }\n  .ame .column.is-full-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-three-quarters-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-two-thirds-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.6666%;\n  }\n  .ame .column.is-half-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-one-third-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.3333%;\n  }\n  .ame .column.is-one-quarter-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-three-quarters-desktop {\n    margin-left: 75%;\n  }\n  .ame .column.is-offset-two-thirds-desktop {\n    margin-left: 66.6666%;\n  }\n  .ame .column.is-offset-half-desktop {\n    margin-left: 50%;\n  }\n  .ame .column.is-offset-one-third-desktop {\n    margin-left: 33.3333%;\n  }\n  .ame .column.is-offset-one-quarter-desktop {\n    margin-left: 25%;\n  }\n  .ame .column.is-1-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 8.33333%;\n  }\n  .ame .column.is-offset-1-desktop {\n    margin-left: 8.33333%;\n  }\n  .ame .column.is-2-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 16.66667%;\n  }\n  .ame .column.is-offset-2-desktop {\n    margin-left: 16.66667%;\n  }\n  .ame .column.is-3-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-3-desktop {\n    margin-left: 25%;\n  }\n  .ame .column.is-4-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.33333%;\n  }\n  .ame .column.is-offset-4-desktop {\n    margin-left: 33.33333%;\n  }\n  .ame .column.is-5-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 41.66667%;\n  }\n  .ame .column.is-offset-5-desktop {\n    margin-left: 41.66667%;\n  }\n  .ame .column.is-6-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-offset-6-desktop {\n    margin-left: 50%;\n  }\n  .ame .column.is-7-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 58.33333%;\n  }\n  .ame .column.is-offset-7-desktop {\n    margin-left: 58.33333%;\n  }\n  .ame .column.is-8-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.66667%;\n  }\n  .ame .column.is-offset-8-desktop {\n    margin-left: 66.66667%;\n  }\n  .ame .column.is-9-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-offset-9-desktop {\n    margin-left: 75%;\n  }\n  .ame .column.is-10-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 83.33333%;\n  }\n  .ame .column.is-offset-10-desktop {\n    margin-left: 83.33333%;\n  }\n  .ame .column.is-11-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 91.66667%;\n  }\n  .ame .column.is-offset-11-desktop {\n    margin-left: 91.66667%;\n  }\n  .ame .column.is-12-desktop {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-offset-12-desktop {\n    margin-left: 100%;\n  }\n}\n\n@media screen and (min-width: 1192px) {\n  .ame .column.is-narrow-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n  }\n  .ame .column.is-full-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-three-quarters-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-two-thirds-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.6666%;\n  }\n  .ame .column.is-half-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-one-third-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.3333%;\n  }\n  .ame .column.is-one-quarter-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-three-quarters-widescreen {\n    margin-left: 75%;\n  }\n  .ame .column.is-offset-two-thirds-widescreen {\n    margin-left: 66.6666%;\n  }\n  .ame .column.is-offset-half-widescreen {\n    margin-left: 50%;\n  }\n  .ame .column.is-offset-one-third-widescreen {\n    margin-left: 33.3333%;\n  }\n  .ame .column.is-offset-one-quarter-widescreen {\n    margin-left: 25%;\n  }\n  .ame .column.is-1-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 8.33333%;\n  }\n  .ame .column.is-offset-1-widescreen {\n    margin-left: 8.33333%;\n  }\n  .ame .column.is-2-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 16.66667%;\n  }\n  .ame .column.is-offset-2-widescreen {\n    margin-left: 16.66667%;\n  }\n  .ame .column.is-3-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-3-widescreen {\n    margin-left: 25%;\n  }\n  .ame .column.is-4-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.33333%;\n  }\n  .ame .column.is-offset-4-widescreen {\n    margin-left: 33.33333%;\n  }\n  .ame .column.is-5-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 41.66667%;\n  }\n  .ame .column.is-offset-5-widescreen {\n    margin-left: 41.66667%;\n  }\n  .ame .column.is-6-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-offset-6-widescreen {\n    margin-left: 50%;\n  }\n  .ame .column.is-7-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 58.33333%;\n  }\n  .ame .column.is-offset-7-widescreen {\n    margin-left: 58.33333%;\n  }\n  .ame .column.is-8-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.66667%;\n  }\n  .ame .column.is-offset-8-widescreen {\n    margin-left: 66.66667%;\n  }\n  .ame .column.is-9-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-offset-9-widescreen {\n    margin-left: 75%;\n  }\n  .ame .column.is-10-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 83.33333%;\n  }\n  .ame .column.is-offset-10-widescreen {\n    margin-left: 83.33333%;\n  }\n  .ame .column.is-11-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 91.66667%;\n  }\n  .ame .column.is-offset-11-widescreen {\n    margin-left: 91.66667%;\n  }\n  .ame .column.is-12-widescreen {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-offset-12-widescreen {\n    margin-left: 100%;\n  }\n}\n\n@media screen and (min-width: 1384px) {\n  .ame .column.is-narrow-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n  }\n  .ame .column.is-full-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-three-quarters-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-two-thirds-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.6666%;\n  }\n  .ame .column.is-half-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-one-third-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.3333%;\n  }\n  .ame .column.is-one-quarter-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-three-quarters-fullhd {\n    margin-left: 75%;\n  }\n  .ame .column.is-offset-two-thirds-fullhd {\n    margin-left: 66.6666%;\n  }\n  .ame .column.is-offset-half-fullhd {\n    margin-left: 50%;\n  }\n  .ame .column.is-offset-one-third-fullhd {\n    margin-left: 33.3333%;\n  }\n  .ame .column.is-offset-one-quarter-fullhd {\n    margin-left: 25%;\n  }\n  .ame .column.is-1-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 8.33333%;\n  }\n  .ame .column.is-offset-1-fullhd {\n    margin-left: 8.33333%;\n  }\n  .ame .column.is-2-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 16.66667%;\n  }\n  .ame .column.is-offset-2-fullhd {\n    margin-left: 16.66667%;\n  }\n  .ame .column.is-3-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .column.is-offset-3-fullhd {\n    margin-left: 25%;\n  }\n  .ame .column.is-4-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.33333%;\n  }\n  .ame .column.is-offset-4-fullhd {\n    margin-left: 33.33333%;\n  }\n  .ame .column.is-5-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 41.66667%;\n  }\n  .ame .column.is-offset-5-fullhd {\n    margin-left: 41.66667%;\n  }\n  .ame .column.is-6-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .column.is-offset-6-fullhd {\n    margin-left: 50%;\n  }\n  .ame .column.is-7-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 58.33333%;\n  }\n  .ame .column.is-offset-7-fullhd {\n    margin-left: 58.33333%;\n  }\n  .ame .column.is-8-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.66667%;\n  }\n  .ame .column.is-offset-8-fullhd {\n    margin-left: 66.66667%;\n  }\n  .ame .column.is-9-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .column.is-offset-9-fullhd {\n    margin-left: 75%;\n  }\n  .ame .column.is-10-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 83.33333%;\n  }\n  .ame .column.is-offset-10-fullhd {\n    margin-left: 83.33333%;\n  }\n  .ame .column.is-11-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 91.66667%;\n  }\n  .ame .column.is-offset-11-fullhd {\n    margin-left: 91.66667%;\n  }\n  .ame .column.is-12-fullhd {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n  .ame .column.is-offset-12-fullhd {\n    margin-left: 100%;\n  }\n}\n\n.ame .columns {\n  margin-left: -0.75rem;\n  margin-right: -0.75rem;\n  margin-top: -0.75rem;\n}\n\n.ame .columns:last-child {\n  margin-bottom: -0.75rem;\n}\n\n.ame .columns:not(:last-child) {\n  margin-bottom: 0.75rem;\n}\n\n.ame .columns.is-centered {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n\n.ame .columns.is-gapless {\n  margin-left: 0;\n  margin-right: 0;\n  margin-top: 0;\n}\n\n.ame .columns.is-gapless:last-child {\n  margin-bottom: 0;\n}\n\n.ame .columns.is-gapless:not(:last-child) {\n  margin-bottom: 1.5rem;\n}\n\n.ame .columns.is-gapless > .column {\n  margin: 0;\n  padding: 0;\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .columns.is-grid {\n    -ms-flex-wrap: wrap;\n        flex-wrap: wrap;\n  }\n  .ame .columns.is-grid > .column {\n    max-width: 33.3333%;\n    padding: 0.75rem;\n    width: 33.3333%;\n  }\n  .ame .columns.is-grid > .column + .column {\n    margin-left: 0;\n  }\n}\n\n.ame .columns.is-mobile {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame .columns.is-multiline {\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n}\n\n.ame .columns.is-vcentered {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .columns:not(.is-desktop) {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .columns.is-desktop {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n}\n\n.ame .tile {\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  display: block;\n  -ms-flex-preferred-size: 0;\n      flex-basis: 0;\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 1;\n      flex-shrink: 1;\n  min-height: -webkit-min-content;\n  min-height: -moz-min-content;\n  min-height: min-content;\n}\n\n.ame .tile.is-ancestor {\n  margin-left: -0.75rem;\n  margin-right: -0.75rem;\n  margin-top: -0.75rem;\n}\n\n.ame .tile.is-ancestor:last-child {\n  margin-bottom: -0.75rem;\n}\n\n.ame .tile.is-ancestor:not(:last-child) {\n  margin-bottom: 0.75rem;\n}\n\n.ame .tile.is-child {\n  margin: 0 !important;\n}\n\n.ame .tile.is-parent {\n  padding: 0.75rem;\n}\n\n.ame .tile.is-vertical {\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.ame .tile.is-vertical > .tile.is-child:not(:last-child) {\n  margin-bottom: 1.5rem !important;\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .tile:not(.is-child) {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n  .ame .tile.is-1 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 8.33333%;\n  }\n  .ame .tile.is-2 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 16.66667%;\n  }\n  .ame .tile.is-3 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 25%;\n  }\n  .ame .tile.is-4 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 33.33333%;\n  }\n  .ame .tile.is-5 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 41.66667%;\n  }\n  .ame .tile.is-6 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 50%;\n  }\n  .ame .tile.is-7 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 58.33333%;\n  }\n  .ame .tile.is-8 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 66.66667%;\n  }\n  .ame .tile.is-9 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 75%;\n  }\n  .ame .tile.is-10 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 83.33333%;\n  }\n  .ame .tile.is-11 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 91.66667%;\n  }\n  .ame .tile.is-12 {\n    -webkit-box-flex: 0;\n        -ms-flex: none;\n            flex: none;\n    width: 100%;\n  }\n}\n\n.ame .hero-video {\n  bottom: 0;\n  left: 0;\n  position: absolute;\n  right: 0;\n  top: 0;\n  overflow: hidden;\n}\n\n.ame .hero-video video {\n  left: 50%;\n  min-height: 100%;\n  min-width: 100%;\n  position: absolute;\n  top: 50%;\n  -webkit-transform: translate3d(-50%, -50%, 0);\n          transform: translate3d(-50%, -50%, 0);\n}\n\n.ame .hero-video.is-transparent {\n  opacity: 0.3;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero-video {\n    display: none;\n  }\n}\n\n.ame .hero-buttons {\n  margin-top: 1.5rem;\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero-buttons .button {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n  .ame .hero-buttons .button:not(:last-child) {\n    margin-bottom: 0.75rem;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame .hero-buttons {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n  }\n  .ame .hero-buttons .button:not(:last-child) {\n    margin-right: 1.5rem;\n  }\n}\n\n.ame .hero-head, .ame .hero-foot {\n  -webkit-box-flex: 0;\n      -ms-flex-positive: 0;\n          flex-grow: 0;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n}\n\n.ame ..hero-body {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  padding: 3rem 1.5rem;\n}\n\n.ame .hero {\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  background-color: white;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n\n.ame .hero .nav {\n  background: none;\n  -webkit-box-shadow: 0 1px 0 rgba(219, 219, 219, 0.3);\n          box-shadow: 0 1px 0 rgba(219, 219, 219, 0.3);\n}\n\n.ame .hero .tabs ul {\n  border-bottom: none;\n}\n\n.ame .hero.is-white {\n  background-color: white;\n  color: #0a0a0a;\n}\n\n.ame .hero.is-white a:not(.button), .ame .hero.is-white strong {\n  color: inherit;\n}\n\n.ame .hero.is-white .title {\n  color: #0a0a0a;\n}\n\n.ame .hero.is-white .subtitle {\n  color: rgba(10, 10, 10, 0.9);\n}\n\n.ame .hero.is-white .subtitle a:not(.button), .ame .hero.is-white .subtitle strong {\n  color: #0a0a0a;\n}\n\n.ame .hero.is-white .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(10, 10, 10, 0.2);\n          box-shadow: 0 1px 0 rgba(10, 10, 10, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-white .nav-menu {\n    background-color: white;\n  }\n}\n\n.ame .hero.is-white a.nav-item, .ame .hero.is-white .nav-item a:not(.button) {\n  color: rgba(10, 10, 10, 0.7);\n}\n\n.ame .hero.is-white a.nav-item:hover, .ame .hero.is-white a.nav-item.is-active, .ame .hero.is-white .nav-item a:not(.button):hover, .ame .hero.is-white .nav-item a:not(.button).is-active {\n  color: #0a0a0a;\n}\n\n.ame .hero.is-white .tabs a {\n  color: #0a0a0a;\n  opacity: 0.9;\n}\n\n.ame .hero.is-white .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-white .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-white .tabs.is-boxed a, .ame .hero.is-white .tabs.is-toggle a {\n  color: #0a0a0a;\n}\n\n.ame .hero.is-white .tabs.is-boxed a:hover, .ame .hero.is-white .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-white .tabs.is-boxed li.is-active a, .ame .hero.is-white .tabs.is-boxed li.is-active a:hover, .ame .hero.is-white .tabs.is-toggle li.is-active a, .ame .hero.is-white .tabs.is-toggle li.is-active a:hover {\n  background-color: #0a0a0a;\n  border-color: #0a0a0a;\n  color: white;\n}\n\n.ame .hero.is-white.is-bold {\n  background-image: linear-gradient(141deg, #e6e6e6 0%, white 71%, white 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-white.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, #e6e6e6 0%, white 71%, white 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-white .nav-toggle span {\n    background-color: #0a0a0a;\n  }\n  .ame .hero.is-white .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-white .nav-toggle.is-active span {\n    background-color: #0a0a0a;\n  }\n  .ame .hero.is-white .nav-menu .nav-item {\n    border-top-color: rgba(10, 10, 10, 0.2);\n  }\n}\n\n.ame .hero.is-black {\n  background-color: #0a0a0a;\n  color: white;\n}\n\n.ame .hero.is-black a:not(.button), .ame .hero.is-black strong {\n  color: inherit;\n}\n\n.ame .hero.is-black .title {\n  color: white;\n}\n\n.ame .hero.is-black .subtitle {\n  color: rgba(255, 255, 255, 0.9);\n}\n\n.ame .hero.is-black .subtitle a:not(.button), .ame .hero.is-black .subtitle strong {\n  color: white;\n}\n\n.ame .hero.is-black .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n          box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-black .nav-menu {\n    background-color: #0a0a0a;\n  }\n}\n\n.ame .hero.is-black a.nav-item, .ame .hero.is-black .nav-item a:not(.button) {\n  color: rgba(255, 255, 255, 0.7);\n}\n\n.ame .hero.is-black a.nav-item:hover, .ame .hero.is-black a.nav-item.is-active, .ame .hero.is-black .nav-item a:not(.button):hover, .ame .hero.is-black .nav-item a:not(.button).is-active {\n  color: white;\n}\n\n.ame .hero.is-black .tabs a {\n  color: white;\n  opacity: 0.9;\n}\n\n.ame .hero.is-black .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-black .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-black .tabs.is-boxed a, .ame .hero.is-black .tabs.is-toggle a {\n  color: white;\n}\n\n.ame .hero.is-black .tabs.is-boxed a:hover, .ame .hero.is-black .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-black .tabs.is-boxed li.is-active a, .ame .hero.is-black .tabs.is-boxed li.is-active a:hover, .ame .hero.is-black .tabs.is-toggle li.is-active a, .ame .hero.is-black .tabs.is-toggle li.is-active a:hover {\n  background-color: white;\n  border-color: white;\n  color: #0a0a0a;\n}\n\n.ame .hero.is-black.is-bold {\n  background-image: linear-gradient(141deg, black 0%, #0a0a0a 71%, #181616 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-black.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, black 0%, #0a0a0a 71%, #181616 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-black .nav-toggle span {\n    background-color: white;\n  }\n  .ame .hero.is-black .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-black .nav-toggle.is-active span {\n    background-color: white;\n  }\n  .ame .hero.is-black .nav-menu .nav-item {\n    border-top-color: rgba(255, 255, 255, 0.2);\n  }\n}\n\n.ame .hero.is-light {\n  background-color: whitesmoke;\n  color: #363636;\n}\n\n.ame .hero.is-light a:not(.button), .ame .hero.is-light strong {\n  color: inherit;\n}\n\n.ame .hero.is-light .title {\n  color: #363636;\n}\n\n.ame .hero.is-light .subtitle {\n  color: rgba(54, 54, 54, 0.9);\n}\n\n.ame .hero.is-light .subtitle a:not(.button), .ame .hero.is-light .subtitle strong {\n  color: #363636;\n}\n\n.ame .hero.is-light .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(54, 54, 54, 0.2);\n          box-shadow: 0 1px 0 rgba(54, 54, 54, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-light .nav-menu {\n    background-color: whitesmoke;\n  }\n}\n\n.ame .hero.is-light a.nav-item, .ame .hero.is-light .nav-item a:not(.button) {\n  color: rgba(54, 54, 54, 0.7);\n}\n\n.ame .hero.is-light a.nav-item:hover, .ame .hero.is-light a.nav-item.is-active, .ame .hero.is-light .nav-item a:not(.button):hover, .ame .hero.is-light .nav-item a:not(.button).is-active {\n  color: #363636;\n}\n\n.ame .hero.is-light .tabs a {\n  color: #363636;\n  opacity: 0.9;\n}\n\n.ame .hero.is-light .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-light .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-light .tabs.is-boxed a, .ame .hero.is-light .tabs.is-toggle a {\n  color: #363636;\n}\n\n.ame .hero.is-light .tabs.is-boxed a:hover, .ame .hero.is-light .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-light .tabs.is-boxed li.is-active a, .ame .hero.is-light .tabs.is-boxed li.is-active a:hover, .ame .hero.is-light .tabs.is-toggle li.is-active a, .ame .hero.is-light .tabs.is-toggle li.is-active a:hover {\n  background-color: #363636;\n  border-color: #363636;\n  color: whitesmoke;\n}\n\n.ame .hero.is-light.is-bold {\n  background-image: linear-gradient(141deg, #dfd8d9 0%, whitesmoke 71%, white 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-light.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, #dfd8d9 0%, whitesmoke 71%, white 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-light .nav-toggle span {\n    background-color: #363636;\n  }\n  .ame .hero.is-light .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-light .nav-toggle.is-active span {\n    background-color: #363636;\n  }\n  .ame .hero.is-light .nav-menu .nav-item {\n    border-top-color: rgba(54, 54, 54, 0.2);\n  }\n}\n\n.ame .hero.is-dark {\n  background-color: #363636;\n  color: whitesmoke;\n}\n\n.ame .hero.is-dark a:not(.button), .ame .hero.is-dark strong {\n  color: inherit;\n}\n\n.ame .hero.is-dark .title {\n  color: whitesmoke;\n}\n\n.ame .hero.is-dark .subtitle {\n  color: rgba(245, 245, 245, 0.9);\n}\n\n.ame .hero.is-dark .subtitle a:not(.button), .ame .hero.is-dark .subtitle strong {\n  color: whitesmoke;\n}\n\n.ame .hero.is-dark .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(245, 245, 245, 0.2);\n          box-shadow: 0 1px 0 rgba(245, 245, 245, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-dark .nav-menu {\n    background-color: #363636;\n  }\n}\n\n.ame .hero.is-dark a.nav-item, .ame .hero.is-dark .nav-item a:not(.button) {\n  color: rgba(245, 245, 245, 0.7);\n}\n\n.ame .hero.is-dark a.nav-item:hover, .ame .hero.is-dark a.nav-item.is-active, .ame .hero.is-dark .nav-item a:not(.button):hover, .ame .hero.is-dark .nav-item a:not(.button).is-active {\n  color: whitesmoke;\n}\n\n.ame .hero.is-dark .tabs a {\n  color: whitesmoke;\n  opacity: 0.9;\n}\n\n.ame .hero.is-dark .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-dark .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-dark .tabs.is-boxed a, .ame .hero.is-dark .tabs.is-toggle a {\n  color: whitesmoke;\n}\n\n.ame .hero.is-dark .tabs.is-boxed a:hover, .ame .hero.is-dark .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-dark .tabs.is-boxed li.is-active a, .ame .hero.is-dark .tabs.is-boxed li.is-active a:hover, .ame .hero.is-dark .tabs.is-toggle li.is-active a, .ame .hero.is-dark .tabs.is-toggle li.is-active a:hover {\n  background-color: whitesmoke;\n  border-color: whitesmoke;\n  color: #363636;\n}\n\n.ame .hero.is-dark.is-bold {\n  background-image: linear-gradient(141deg, #1f191a 0%, #363636 71%, #46403f 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-dark.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, #1f191a 0%, #363636 71%, #46403f 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-dark .nav-toggle span {\n    background-color: whitesmoke;\n  }\n  .ame .hero.is-dark .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-dark .nav-toggle.is-active span {\n    background-color: whitesmoke;\n  }\n  .ame .hero.is-dark .nav-menu .nav-item {\n    border-top-color: rgba(245, 245, 245, 0.2);\n  }\n}\n\n.ame .hero.is-primary {\n  background-color: #00d1b2;\n  color: #fff;\n}\n\n.ame .hero.is-primary a:not(.button), .ame .hero.is-primary strong {\n  color: inherit;\n}\n\n.ame .hero.is-primary .title {\n  color: #fff;\n}\n\n.ame .hero.is-primary .subtitle {\n  color: rgba(255, 255, 255, 0.9);\n}\n\n.ame .hero.is-primary .subtitle a:not(.button), .ame .hero.is-primary .subtitle strong {\n  color: #fff;\n}\n\n.ame .hero.is-primary .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n          box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-primary .nav-menu {\n    background-color: #00d1b2;\n  }\n}\n\n.ame .hero.is-primary a.nav-item, .ame .hero.is-primary .nav-item a:not(.button) {\n  color: rgba(255, 255, 255, 0.7);\n}\n\n.ame .hero.is-primary a.nav-item:hover, .ame .hero.is-primary a.nav-item.is-active, .ame .hero.is-primary .nav-item a:not(.button):hover, .ame .hero.is-primary .nav-item a:not(.button).is-active {\n  color: #fff;\n}\n\n.ame .hero.is-primary .tabs a {\n  color: #fff;\n  opacity: 0.9;\n}\n\n.ame .hero.is-primary .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-primary .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-primary .tabs.is-boxed a, .ame .hero.is-primary .tabs.is-toggle a {\n  color: #fff;\n}\n\n.ame .hero.is-primary .tabs.is-boxed a:hover, .ame .hero.is-primary .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-primary .tabs.is-boxed li.is-active a, .ame .hero.is-primary .tabs.is-boxed li.is-active a:hover, .ame .hero.is-primary .tabs.is-toggle li.is-active a, .ame .hero.is-primary .tabs.is-toggle li.is-active a:hover {\n  background-color: #fff;\n  border-color: #fff;\n  color: #00d1b2;\n}\n\n.ame .hero.is-primary.is-bold {\n  background-image: linear-gradient(141deg, #009e6c 0%, #00d1b2 71%, #00e7eb 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-primary.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, #009e6c 0%, #00d1b2 71%, #00e7eb 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-primary .nav-toggle span {\n    background-color: #fff;\n  }\n  .ame .hero.is-primary .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-primary .nav-toggle.is-active span {\n    background-color: #fff;\n  }\n  .ame .hero.is-primary .nav-menu .nav-item {\n    border-top-color: rgba(255, 255, 255, 0.2);\n  }\n}\n\n.ame .hero.is-info {\n  background-color: #3273dc;\n  color: #fff;\n}\n\n.ame .hero.is-info a:not(.button), .ame .hero.is-info strong {\n  color: inherit;\n}\n\n.ame .hero.is-info .title {\n  color: #fff;\n}\n\n.ame .hero.is-info .subtitle {\n  color: rgba(255, 255, 255, 0.9);\n}\n\n.ame .hero.is-info .subtitle a:not(.button), .ame .hero.is-info .subtitle strong {\n  color: #fff;\n}\n\n.ame .hero.is-info .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n          box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-info .nav-menu {\n    background-color: #3273dc;\n  }\n}\n\n.ame .hero.is-info a.nav-item, .ame .hero.is-info .nav-item a:not(.button) {\n  color: rgba(255, 255, 255, 0.7);\n}\n\n.ame .hero.is-info a.nav-item:hover, .ame .hero.is-info a.nav-item.is-active, .ame .hero.is-info .nav-item a:not(.button):hover, .ame .hero.is-info .nav-item a:not(.button).is-active {\n  color: #fff;\n}\n\n.ame .hero.is-info .tabs a {\n  color: #fff;\n  opacity: 0.9;\n}\n\n.ame .hero.is-info .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-info .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-info .tabs.is-boxed a, .ame .hero.is-info .tabs.is-toggle a {\n  color: #fff;\n}\n\n.ame .hero.is-info .tabs.is-boxed a:hover, .ame .hero.is-info .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-info .tabs.is-boxed li.is-active a, .ame .hero.is-info .tabs.is-boxed li.is-active a:hover, .ame .hero.is-info .tabs.is-toggle li.is-active a, .ame .hero.is-info .tabs.is-toggle li.is-active a:hover {\n  background-color: #fff;\n  border-color: #fff;\n  color: #3273dc;\n}\n\n.ame .hero.is-info.is-bold {\n  background-image: linear-gradient(141deg, #1577c6 0%, #3273dc 71%, #4366e5 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-info.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, #1577c6 0%, #3273dc 71%, #4366e5 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-info .nav-toggle span {\n    background-color: #fff;\n  }\n  .ame .hero.is-info .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-info .nav-toggle.is-active span {\n    background-color: #fff;\n  }\n  .ame .hero.is-info .nav-menu .nav-item {\n    border-top-color: rgba(255, 255, 255, 0.2);\n  }\n}\n\n.ame .hero.is-success {\n  background-color: #23d160;\n  color: #fff;\n}\n\n.ame .hero.is-success a:not(.button), .ame .hero.is-success strong {\n  color: inherit;\n}\n\n.ame .hero.is-success .title {\n  color: #fff;\n}\n\n.ame .hero.is-success .subtitle {\n  color: rgba(255, 255, 255, 0.9);\n}\n\n.ame .hero.is-success .subtitle a:not(.button), .ame .hero.is-success .subtitle strong {\n  color: #fff;\n}\n\n.ame .hero.is-success .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n          box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-success .nav-menu {\n    background-color: #23d160;\n  }\n}\n\n.ame .hero.is-success a.nav-item, .ame .hero.is-success .nav-item a:not(.button) {\n  color: rgba(255, 255, 255, 0.7);\n}\n\n.ame .hero.is-success a.nav-item:hover, .ame .hero.is-success a.nav-item.is-active, .ame .hero.is-success .nav-item a:not(.button):hover, .ame .hero.is-success .nav-item a:not(.button).is-active {\n  color: #fff;\n}\n\n.ame .hero.is-success .tabs a {\n  color: #fff;\n  opacity: 0.9;\n}\n\n.ame .hero.is-success .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-success .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-success .tabs.is-boxed a, .ame .hero.is-success .tabs.is-toggle a {\n  color: #fff;\n}\n\n.ame .hero.is-success .tabs.is-boxed a:hover, .ame .hero.is-success .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-success .tabs.is-boxed li.is-active a, .ame .hero.is-success .tabs.is-boxed li.is-active a:hover, .ame .hero.is-success .tabs.is-toggle li.is-active a, .ame .hero.is-success .tabs.is-toggle li.is-active a:hover {\n  background-color: #fff;\n  border-color: #fff;\n  color: #23d160;\n}\n\n.ame .hero.is-success.is-bold {\n  background-image: linear-gradient(141deg, #12af2f 0%, #23d160 71%, #2ce28a 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-success.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, #12af2f 0%, #23d160 71%, #2ce28a 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-success .nav-toggle span {\n    background-color: #fff;\n  }\n  .ame .hero.is-success .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-success .nav-toggle.is-active span {\n    background-color: #fff;\n  }\n  .ame .hero.is-success .nav-menu .nav-item {\n    border-top-color: rgba(255, 255, 255, 0.2);\n  }\n}\n\n.ame .hero.is-warning {\n  background-color: #ffdd57;\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .hero.is-warning a:not(.button), .ame .hero.is-warning strong {\n  color: inherit;\n}\n\n.ame .hero.is-warning .title {\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .hero.is-warning .subtitle {\n  color: rgba(0, 0, 0, 0.9);\n}\n\n.ame .hero.is-warning .subtitle a:not(.button), .ame .hero.is-warning .subtitle strong {\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .hero.is-warning .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(0, 0, 0, 0.2);\n          box-shadow: 0 1px 0 rgba(0, 0, 0, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-warning .nav-menu {\n    background-color: #ffdd57;\n  }\n}\n\n.ame .hero.is-warning a.nav-item, .ame .hero.is-warning .nav-item a:not(.button) {\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .hero.is-warning a.nav-item:hover, .ame .hero.is-warning a.nav-item.is-active, .ame .hero.is-warning .nav-item a:not(.button):hover, .ame .hero.is-warning .nav-item a:not(.button).is-active {\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .hero.is-warning .tabs a {\n  color: rgba(0, 0, 0, 0.7);\n  opacity: 0.9;\n}\n\n.ame .hero.is-warning .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-warning .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-warning .tabs.is-boxed a, .ame .hero.is-warning .tabs.is-toggle a {\n  color: rgba(0, 0, 0, 0.7);\n}\n\n.ame .hero.is-warning .tabs.is-boxed a:hover, .ame .hero.is-warning .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-warning .tabs.is-boxed li.is-active a, .ame .hero.is-warning .tabs.is-boxed li.is-active a:hover, .ame .hero.is-warning .tabs.is-toggle li.is-active a, .ame .hero.is-warning .tabs.is-toggle li.is-active a:hover {\n  background-color: rgba(0, 0, 0, 0.7);\n  border-color: rgba(0, 0, 0, 0.7);\n  color: #ffdd57;\n}\n\n.ame .hero.is-warning.is-bold {\n  background-image: linear-gradient(141deg, #ffaf24 0%, #ffdd57 71%, #fffa70 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-warning.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, #ffaf24 0%, #ffdd57 71%, #fffa70 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-warning .nav-toggle span {\n    background-color: rgba(0, 0, 0, 0.7);\n  }\n  .ame .hero.is-warning .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-warning .nav-toggle.is-active span {\n    background-color: rgba(0, 0, 0, 0.7);\n  }\n  .ame .hero.is-warning .nav-menu .nav-item {\n    border-top-color: rgba(0, 0, 0, 0.2);\n  }\n}\n\n.ame .hero.is-danger {\n  background-color: #ff3860;\n  color: #fff;\n}\n\n.ame .hero.is-danger a:not(.button), .ame .hero.is-danger strong {\n  color: inherit;\n}\n\n.ame .hero.is-danger .title {\n  color: #fff;\n}\n\n.ame .hero.is-danger .subtitle {\n  color: rgba(255, 255, 255, 0.9);\n}\n\n.ame .hero.is-danger .subtitle a:not(.button), .ame .hero.is-danger .subtitle strong {\n  color: #fff;\n}\n\n.ame .hero.is-danger .nav {\n  -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n          box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-danger .nav-menu {\n    background-color: #ff3860;\n  }\n}\n\n.ame .hero.is-danger a.nav-item, .ame .hero.is-danger .nav-item a:not(.button) {\n  color: rgba(255, 255, 255, 0.7);\n}\n\n.ame .hero.is-danger a.nav-item:hover, .ame .hero.is-danger a.nav-item.is-active, .ame .hero.is-danger .nav-item a:not(.button):hover, .ame .hero.is-danger .nav-item a:not(.button).is-active {\n  color: #fff;\n}\n\n.ame .hero.is-danger .tabs a {\n  color: #fff;\n  opacity: 0.9;\n}\n\n.ame .hero.is-danger .tabs a:hover {\n  opacity: 1;\n}\n\n.ame .hero.is-danger .tabs li.is-active a {\n  opacity: 1;\n}\n\n.ame .hero.is-danger .tabs.is-boxed a, .ame .hero.is-danger .tabs.is-toggle a {\n  color: #fff;\n}\n\n.ame .hero.is-danger .tabs.is-boxed a:hover, .ame .hero.is-danger .tabs.is-toggle a:hover {\n  background-color: rgba(10, 10, 10, 0.1);\n}\n\n.ame .hero.is-danger .tabs.is-boxed li.is-active a, .ame .hero.is-danger .tabs.is-boxed li.is-active a:hover, .ame .hero.is-danger .tabs.is-toggle li.is-active a, .ame .hero.is-danger .tabs.is-toggle li.is-active a:hover {\n  background-color: #fff;\n  border-color: #fff;\n  color: #ff3860;\n}\n\n.ame .hero.is-danger.is-bold {\n  background-image: linear-gradient(141deg, #ff0561 0%, #ff3860 71%, #ff5257 100%);\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-danger.is-bold .nav-menu {\n    background-image: linear-gradient(141deg, #ff0561 0%, #ff3860 71%, #ff5257 100%);\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .ame .hero.is-danger .nav-toggle span {\n    background-color: #fff;\n  }\n  .ame .hero.is-danger .nav-toggle:hover {\n    background-color: rgba(10, 10, 10, 0.1);\n  }\n  .ame .hero.is-danger .nav-toggle.is-active span {\n    background-color: #fff;\n  }\n  .ame .hero.is-danger .nav-menu .nav-item {\n    border-top-color: rgba(255, 255, 255, 0.2);\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame ..hero.is-medium .hero-body {\n    padding-bottom: 9rem;\n    padding-top: 9rem;\n  }\n}\n\n@media screen and (min-width: 769px), print {\n  .ame ..hero.is-large .hero-body {\n    padding-bottom: 18rem;\n    padding-top: 18rem;\n  }\n}\n\n.ame ..hero.is-halfheight .hero-body, .ame . .hero.is-fullheight .hero-body {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.ame ..hero.is-halfheight .hero-body > .container, .ame . .hero.is-fullheight .hero-body > .container {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  -ms-flex-negative: 1;\n      flex-shrink: 1;\n}\n\n.ame .hero.is-halfheight {\n  min-height: 50vh;\n}\n\n.ame .hero.is-fullheight {\n  min-height: 100vh;\n}\n\n.ame .section {\n  background-color: white;\n  padding: 3rem 1.5rem;\n}\n\n@media screen and (min-width: 1000px) {\n  .ame .section.is-medium {\n    padding: 9rem 1.5rem;\n  }\n  .ame .section.is-large {\n    padding: 18rem 1.5rem;\n  }\n}\n\n.ame .footer {\n  background-color: whitesmoke;\n  padding: 3rem 1.5rem 6rem;\n}", ""]);

// exports

    /***/
  }),
  /* 12 */
  /***/ (function (module, exports, __webpack_require__) {

    exports = module.exports = __webpack_require__(3)(undefined);
// imports


// module
    exports.push([module.i, ".ame .icon-toolbar {\n    width: 24px;\n    height: 24px;\n    background-size: 24px 24px;\n    background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAAnFBMVEUAAAAAAAAAAAAAAAAAAABEREBEREBEREACAgFEREAJCQgAAAABAQFEREABAQFEREABAQFEREAEBAREREAAAAAAAAACAgIQEA8FBQUDAwMLCwoBAQENDQwAAAAAAAAFBQUAAAAFBQQBAQEMDAwFBQUAAAAAAAANDQwEBAMCAgIEBAQEBAQCAgICAgEAAAACAgIDAwMEBAMEBAQBAQHJ273sAAAALnRSTlMAPsTFOwQFAcUKRck8Cb8NyQLMD783PEPDxj/FRYecyInCxEg+B/gJ+D/JyMDAn8mrygAAASFJREFUKFNNUe1CgzAMvFWgItba0U1RNzd18zsb8P7vZpKCkD8k4XK9SwCNhaEhLjLMw+RFbjWzl2Om1am8qhi6qK7d2U39m7bzt6iAKizrluLYD23dhqxaAevlnSeQcCBS9PctJYSvfSCZsDmMbaTiNhEzBsUHVyDrH7gyIx6SBl/mQO+4yozgW/ZhVoIQZb0d+LvEqJPSE3FsPOE5Hp+UEdhsmXfgV/zzkGEXiaQfRSgj2OFmcph0kkzyd7ubvys/RgVRXrc8LaxMtR8mQezDutq/YNQ5ODwVQF76kOmdJoev5zegEOc4HBOekmLHY+leh2PiIWXc64aV//0jOSRMXiU+v7rvta7mfyMatimdNSLIumZ2c5v/9Hq4SH3/m/p/mo0hx2rKplgAAAAASUVORK5CYII=') no-repeat center center\n}\n\n.ame .table tr.notification:hover {\n    background-color: inherit !important;\n}\n\n.ame automate-content {\n    flex-grow: 1;\n    flex-shrink: 1;\n}\n\n.ame .hero-body {\n    max-height: 88vh;\n    display: flex;\n}\n\n.ame .hero-body > .container {\n    overflow: auto;\n}\n\n.ame .container {\n    /*min-width: 0 !important;*/\n    /*width: auto !important;*/\n}\n\n/*.sidebar {*/\n/*width: 256px;*/\n/*background-color: #fff;*/\n/*position: fixed !important;*/\n/*z-index: 2;*/\n/*overflow: hidden;*/\n/*top: 32px;*/\n/*bottom: 32px;*/\n/*}*/\n\n/*.bar-block .dropdown-hover, .bar-block .dropdown-click {*/\n/*width: 100%*/\n/*}*/\n\n/*.bar-block .dropdown-hover .dropdown-content, .bar-block .dropdown-click .dropdown-content {*/\n/*min-width: 100%*/\n/*}*/\n\n/*.bar-block .bar-item {*/\n/*width: 100%;*/\n/*display: block;*/\n/*padding: 8px 0px;*/\n/*border: none;*/\n/*outline: none;*/\n/*white-space: normal;*/\n/*float: none*/\n/*}*/\n\n/*.button {*/\n/*cursor: pointer;*/\n/*text-align: center;*/\n/*}*/\n\n/*.bar-block.center .bar-item {*/\n/*text-align: center*/\n/*}*/\n\n/*.block {*/\n/*display: block;*/\n/*width: 100%*/\n/*}*/\n\n/*.card, .card-2 {*/\n/*box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12)*/\n/*}*/\n\n/*.animate-right {*/\n/*position: relative;*/\n/*animation: animateright 0.4s*/\n/*}*/\n\n/*@keyframes animateright {*/\n/*from {*/\n/*right: -300px;*/\n/*opacity: 0*/\n/*}*/\n/*to {*/\n/*right: 0;*/\n/*opacity: 1*/\n/*}*/\n/*}*/\n\n/*.sidebar-container, .sidebar-top, .sidebar-bottom {*/\n/*right: 5px;*/\n/*left: 5px;*/\n/*}*/\n\n/*.sidebar-container {*/\n/*overflow-y: auto;*/\n/*position: absolute;*/\n/*top: 56px;*/\n/*bottom: 56px;*/\n/*}*/\n\n/*.sidebar-top {*/\n/*position: absolute;*/\n/*top: 6px;*/\n/*}*/\n\n/*.sidebar-bottom {*/\n/*position: absolute;*/\n/*bottom: 6px;*/\n/*}*/\n\n/*.sidebar-wrapper>table{*/\n/*width: 100%;*/\n/*}*/\n/*.close {*/\n/*cursor: pointer;*/\n/*padding: 0 4px;*/\n/*margin: 0 5px;*/\n/*border: 1px solid black;*/\n/*border-radius: 4px;*/\n/*float: right;*/\n/*}*/", ""]);

// exports

    /***/
  }),
  /* 13 */
  /***/ (function (module, exports, __webpack_require__) {


    var riot = __webpack_require__(0);
    riot.tag2('automate-content', '<div class="hero-body"> <div class="container"> <table class="table is-narrow"> <tbody> <tr each="{ctrl.data.PARKING_LOT}" class="notification {is-warning:diff.manual, is-success:fixed, is-danger:failed}"> <td><label class="checkbox"><input type="checkbox" checked="{selected}" onclick="{toggleFix}"></label> </td> <td><a onclick="{selectVenue}" href="{getHref(old.attributes.id)}">#</a></td> <td>{old.attributes.name}</td> <td>{typeof diff.name !== \'undefined\' ? (diff.name === \'\' ? \'⁗⁗\' : diff.name) : \'Attrs\'}</td> </tr> </tbody> </table> </div> </div>', '', '', function (opts) {
      this.ctrl = this.opts.ctrl;

      this.getHref = (id) => {
        const l = window.location;
        return l.protocol + '//' + l.host + l.pathname + '?venues=' + id;
      };

      this.toggleFix = (e) => {
        e.item.selected = !e.item.selected;
      };
    });

    /***/
  }),
  /* 14 */
  /***/ (function (module, exports, __webpack_require__) {


    var riot = __webpack_require__(0);
    riot.tag2('automate-footer', '<div class="hero-foot"> <footer class="is-fullwidth"> <div class="container" style="width: auto;"> <div class="columns"> <div class="column"> <a class="button is-info is-fullwidth" onclick="{scan}">Scan</a> </div> <div class="column"> <a class="button is-success is-fullwidth" onclick="{fix}">Fix</a> </div> </div> </div> </footer> </div>', '', '', function (opts) {
      const ctrl = this.opts.ctrl;
      this.scan = async (e) => {
        e.preventDefault();
        const data = await ctrl.scanner.scan();

        let merged;
        if (data.length > 1) {
          merged = ctrl.deepmerge.all(data);
        } else {
          merged = data[0];
        }
        const parkings = ctrl.filter.filter(merged, 'venues', 'PARKING_LOT');
        const parkingsToFix = ctrl.rules.getRules('PARKING_LOT')(parkings);
        ctrl.data.PARKING_LOT = parkingsToFix.map((it) => {
          it.selected = !it.diff.manual;
          it.selectVenue = ctrl.scanner.selector('venues', it.old.attributes.id);
          return it;
        });
        this.opts.parent.update();
      };

      this.fix = async (e) => {
        e.preventDefault();
        this.opts.parent.update();
        const toFix = ctrl.data.PARKING_LOT.filter((it) => {
          return it.selected
        });
        ctrl.fixer.fix(toFix, ctrl.rules.getRules('PARKING_LOT'));
        this.opts.parent.update();
      }
    });

    /***/
  }),
  /* 15 */
  /***/ (function (module, exports, __webpack_require__) {


    var riot = __webpack_require__(0);
    riot.tag2('automate-header', '<div class="hero-head"> <header class="nav"> <div class="container is-fluid"> <div class="nav-left nav-item" style="flex-basis: auto;"> <h4 class="title is-4">{this.opts.ctrl.title}</h4> </div> <div class="nav-right nav-item"> <button class="delete is-small is-danger" onclick="{close}"></button> </div> </div> </header> </div>', '', '', function (opts) {
      this.title = this.opts.ctrl.title;
      this.close = (e) => {
        e.preventDefault();
        this.opts.ctrl.ui.closeToolbar();
      }
    });

    /***/
  }),
  /* 16 */
  /***/ (function (module, exports) {

    /**
     * When source maps are enabled, `style-loader` uses a link element with a data-uri to
     * embed the css on the page. This breaks all relative urls because now they are relative to a
     * bundle instead of the current page.
     *
     * One solution is to only use full urls, but that may be impossible.
     *
     * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
     *
     * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
     *
     */

    module.exports = function (css) {
      // get current location
      var location = typeof window !== "undefined" && window.location;

      if (!location) {
        throw new Error("fixUrls requires window.location");
      }

      // blank or null?
      if (!css || typeof css !== "string") {
        return css;
      }

      var baseUrl = location.protocol + "//" + location.host;
      var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

      // convert each url(...)
      /*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
      var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function (fullMatch, origUrl) {
        // strip quotes (if they exist)
        var unquotedOrigUrl = origUrl
          .trim()
          .replace(/^"(.*)"$/, function (o, $1) {
            return $1;
          })
          .replace(/^'(.*)'$/, function (o, $1) {
            return $1;
          });

        // already a full url? no change
        if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(unquotedOrigUrl)) {
          return fullMatch;
        }

        // convert the url to a full url
        var newUrl;

        if (unquotedOrigUrl.indexOf("//") === 0) {
          //TODO: should we add protocol?
          newUrl = unquotedOrigUrl;
        } else if (unquotedOrigUrl.indexOf("/") === 0) {
          // path should be relative to the base url
          newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
        } else {
          // path should be relative to current directory
          newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
        }

        // send back the fixed url(...)
        return "url(" + JSON.stringify(newUrl) + ")";
      });

      // send back the fixed css
      return fixedCss;
    };

    /***/
  }),
  /* 17 */
  /***/ (function (module, exports, __webpack_require__) {

    "use strict";

    var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try { step(generator.next(value)); } catch (e) { reject(e); }
        }

        function rejected(value) {
          try { step(generator["throw"](value)); } catch (e) { reject(e); }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    Object.defineProperty(exports, "__esModule", {value: true});
    __webpack_require__(8);
    const deepmerge_1 = __webpack_require__(5);
    const riot_1 = __webpack_require__(0);
    __webpack_require__(1);
    __webpack_require__(9);
    __webpack_require__(6);
    __webpack_require__(7);
    __webpack_require__(2);
    const version = __webpack_require__(10).version;
    window.wmeAutoMate = window.wmeAutoMate || {};
// var unsafeWindow = unsafeWindow || {} as UnsafeWindow;
    const verbose = true;
    const utils = {};
    const ctrl = {};
    utils.log = {
      info: (...args) => {
        if (!verbose) {
            return;
        }
        const a = ['%c WMEAutoMate (I): ', 'color: #10bc01; font-weight: bold'].concat(args);
        console.info.apply(console, a);
      },
      error: (...args) => {
        const a = ['%c WMEAutoMate (E): ', 'color: #e52f02; font-weight: bold'].concat(args);
        console.error.apply(console, a);
      }
    };
    utils.timeout = (ms) => {
      return new Promise(resolve => setTimeout(resolve, ms));
    };
    utils.getWazeAPI = () => __awaiter(this, void 0, void 0, function* () {
      const api = window.Waze /*|| unsafeWindow.Waze*/;
      let ready = Boolean(api &&
        api.map &&
        api.model &&
        api.model.countries &&
        api.model.countries.top);
      utils.log.info('Waze API ready:', ready);
      while (!ready) {
        yield utils.timeout(100);
        ready = Boolean(api &&
            api.map &&
            api.model &&
            api.model.countries &&
            api.model.countries.top);
        utils.log.info('Waze API ready:', ready);
      }
      return api;
    });
    utils.updateObject = eval('require("Waze/Action/UpdateObject")');

    class Scanner {
      constructor(api) {
        this.api = api;
        this.map = api.map;
      }

      splitExtent(extent, zoom) {
        const result = [];
        const ratio = this.api.map.getResolution() / this.api.map.getResolutionForZoom(zoom);
        const dx = extent.getWidth() / ratio;
        const dy = extent.getHeight() / ratio;
        let x, y;
        for (x = extent.left; x < extent.right; x += dx) {
            for (y = extent.bottom; y < extent.top; y += dy) {
              const bounds = new OpenLayers.Bounds();
              bounds.extend(new OpenLayers.LonLat(x, y));
              bounds.extend(new OpenLayers.LonLat(x + dx, y + dy));
              result.push(bounds);
            }
        }
        return result;
      }

      getData(bbox) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = new URL([window.location.protocol,
              '//', window.location.host,
              this.api.Config.paths.features].join(''));
            const params = {
              bbox,
              language: I18n.locale,
              venueFilter: '3,1,3',
              venueLevel: this.api.Config.venues.zoomToSize[this.api.map.getZoom() || 5],
            };
            Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
            try {
              const resp = yield fetch(url.toString(), {
                method: 'GET',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Cache': 'no-cache'
                },
                credentials: 'include'
              });
              if (resp.status === 200) {
                return resp.json();
              }
              else {

              }
            }
            catch (ex) {
              utils.log.error(ex);

            }
        });
      }

      scan() {
        return __awaiter(this, void 0, void 0, function* () {
            const zoom = this.api.map.getZoom();
            const boundsArray = this.splitExtent(this.api.map.calculateBounds(), zoom);
            utils.log.info('scaning area:', boundsArray);
            const requests = [];
            boundsArray.forEach((bounds) => {
              const peace = bounds.transform(this.api.map.getProjectionObject(), this.api.map.displayProjection);
              requests.push(this.getData(peace.toBBOX()));
            });
            return yield Promise.all(requests);
        });
      }

      selector(collection, objectId) {
        return (e) => {
          e.preventDefault();
          // const zoom = this.api.map.getZoom();
          const obj = this.api.model[collection].objects[objectId];
          if (!obj) {
            return true;
          }
          // const objCenter = obj.geometry.getBounds().getCenterLonLat().transform(this.api.map.displayProjection, this.api.map.getProjectionObject());
          this.api.selectionManager.select([obj]);
          // this.api.map.setCenter(objCenter, zoom);
          return false;
        };
      }
    }

    class Filter {
      constructor(api) {
        this.api = api;
        this.keys = ['am9obmF0YW5kb2U=', 'Rm9yb3hvbg=='];
      }

      filter(data, type, category) {
        if (!data[type]) {
          return [];
        }
        const objects = data[type].objects;
        if (this.keys.indexOf(btoa(this.api.loginManager.user.userName.toLowerCase())) !== -1) {
          return [];
        }
        return objects.filter((it) => {
          return it.categories.indexOf(category) !== -1
                && it.lockRank <= this.api.loginManager.user.rank + 1;
        });
      }
    }

    class Fixer {
      constructor(api) {
        this.api = api;
      }

      fix(objects) {
        utils.log.info('Fixing items:', objects);
        objects.forEach((fix) => {
          try {
                this.api.model.actionManager.add(new utils.updateObject(fix.old, fix.diff));
            fix.fixed = true;
          }
          catch (ex) {
                utils.log.error('Fixing error', ex, fix);
            fix.failed = true;
          }
        });
      }
    }

    class Rules {
      constructor(api) {
        this.api = api;
      }

      isBad(parking) {
        const name = typeof parking.name !== 'undefined' ? parking.name.toLowerCase() : '';
        return name.indexOf('гараж') !== -1;
      }

      isPaid(parking) {
        const name = typeof parking.name !== 'undefined' ? parking.name.toLowerCase() : '';
        let attrs;
        if (parking.categoryAttributes && parking.categoryAttributes.PARKING_LOT) {
          attrs = parking.categoryAttributes.PARKING_LOT;
        }
        if (name.indexOf('$') !== -1 ||
          name.indexOf('₴') !== -1 ||
          name.indexOf('платн') !== -1) {
          return true;
        }
        return !!(attrs && (attrs.paymentType && attrs.paymentType.indexOf('CASH') !== -1 ||
          attrs.costType && ['FREE', 'UNKNOWN'].indexOf(attrs.costType) === -1));
      }

      hasPoi(parking) {
        if (!parking.name) {
          return;
        }
        const poiMatchers = [
          new RegExp('^(?:\\[*[pPрР]{1}\\]*|(?:(?:гостьова|гостьовий|платна|платний|підземний|підземна)\\s)?(?:parking|парковка|паркінг))(?:[\\s\\(\\),\\[\\]$₴\\-]+|\\s)((?:[\\w\\s\\-1-9А-ЯЇЄІҐа-яєїіґʼ.]{2,}|[1-9\\-,\\s]){1,})[\\s\\(\\),\\[\\]\\₴$\\-]*$', 'i'),
          new RegExp('^((?:[\\w1-9А-ЯЇЄІҐа-яєїіґʼ.]{2,}(?:\\s*[1-9\\-,\\s])?)+)(?=(?:(?:[\\s\\(\\),\\[\\]$₴\\-]+)(?:(?:гостьова|гостьовий|платна|платний|підземний|підземна)\\s)?(?:parking|парковка|паркінг))(?:[\\s\\(\\),\\[\\]$₴\\-]*)$)', 'i'),
        ];
        let poiName;
        for (let i = 0; i < poiMatchers.length; i++) {
          const matcher = poiMatchers[i];
          const match = parking.name.match(matcher);
          if (match && match[1]) {
            poiName = match[1].trim();
            if (poiName.indexOf('-') === poiName.length - 1) {
              poiName = poiName.substr(0, poiName.length - 2).trim();
            }
            if (poiName) {
              break;
            }
          }
        }
        return poiName;
      }

      getRules(category) {
        const rules = {
          PARKING_LOT: (items) => {
            const fixedItems = [];
            items.forEach((item) => {
              const fixed = {
                categoryAttributes: {
                  PARKING_LOT: {}
                }
              };
              let changed = false;
              const isPaid = this.isPaid(item);
              const poiName = this.hasPoi(item);
              const isBad = this.isBad(item);
              let newName;
              if (isBad) {
                fixed.manual = true;
                changed = true;
              }
              else if (isPaid) {
                // paid
                newName = 'Р (₴)';
                if (poiName && item.description !== poiName) {
                  // poi paid
                  fixed.description = poiName;
                  changed = true;
                }
                if (!item.categoryAttributes) {
                  fixed.categoryAttributes.PARKING_LOT.costType = 'MODERATE';
                  fixed.categoryAttributes.PARKING_LOT.parkingType = 'PUBLIC';
                  fixed.categoryAttributes.PARKING_LOT.paymentType = ['CASH'];
                  fixed.categoryAttributes.PARKING_LOT.lotType = ['STREET_LEVEL'];
                  changed = true;
                }
                else {
                  const attrs = item.categoryAttributes.PARKING_LOT;
                  if (!attrs.costType || attrs.costType === 'FREE') {
                    fixed.categoryAttributes.PARKING_LOT.costType = 'MODERATE';
                    changed = true;
                  }
                  if (!attrs.parkingType) {
                    fixed.categoryAttributes.PARKING_LOT.parkingType = 'PUBLIC';
                    changed = true;
                  }
                  if (!attrs.lotType || attrs.lotType.length === 0) {
                    fixed.categoryAttributes.PARKING_LOT.lotType = ['STREET_LEVEL'];
                    changed = true;
                  }
                  if (!attrs.paymentType || attrs.paymentType.length === 0) {
                    fixed.categoryAttributes.PARKING_LOT.paymentType = ['CASH'];
                    changed = true;
                  }
                }
              }
              else {
                // free
                if (poiName) {
                  // poi parking
                  newName = 'Р (' + poiName + ')';
                }
                else {
                  // non-poi parking
                  newName = '';
                }
                if (!item.categoryAttributes) {
                  fixed.categoryAttributes.PARKING_LOT.costType = 'FREE';
                  fixed.categoryAttributes.PARKING_LOT.parkingType = 'PUBLIC';
                  fixed.categoryAttributes.PARKING_LOT.lotType = ['STREET_LEVEL'];
                  changed = true;
                }
                else {
                  const attrs = item.categoryAttributes.PARKING_LOT;
                  if (!attrs.costType) {
                    fixed.categoryAttributes.PARKING_LOT.costType = 'FREE';
                    changed = true;
                  }
                  if (!attrs.parkingType) {
                    fixed.categoryAttributes.PARKING_LOT.parkingType = 'PUBLIC';
                    changed = true;
                  }
                  if (!attrs.lotType || attrs.lotType.length === 0) {
                    fixed.categoryAttributes.PARKING_LOT.lotType = ['STREET_LEVEL'];
                    changed = true;
                  }
                }
              }
              if (!item.name || item.name !== newName) {
                changed = true;
                fixed.name = newName || '';
              }
              if (changed) {
                if (Object.keys(fixed.categoryAttributes.PARKING_LOT).length === 0) {
                  delete fixed.categoryAttributes;
                }
                fixedItems.push({
                  old: this.api.model.venues.objects[item.id] ? this.api.model.venues.objects[item.id] : {
                    attributes: item,
                    model: this.api.model,
                    type: 'venue',
                    fake: true,
                    _originalResidential: false
                  },
                  diff: fixed
                });
              }
            });
            return fixedItems;
          }
        };
        return rules[category];
      }
    }

    let inited = false;
    const init = () => {
      if (inited) {
        return;
      }
      const panel = window.document.querySelector('#edit-buttons>div');
      const editor = window.document.querySelector('.edit-area');
      const toolbar = __webpack_require__(2);
      const button = __webpack_require__(1);
      panel.insertAdjacentHTML('beforeend', button);
      editor.insertAdjacentHTML('beforeend', toolbar);
      inited = true;
    };
    const openToolbar = () => {
      document.getElementById('wmeAutoMateToolbar').style.display = 'block';
    };
    const closeToolbar = () => {
      document.getElementById('wmeAutoMateToolbar').style.display = 'none';
    };
    const toggleToolbar = () => {
      const state = document.getElementById('wmeAutoMateToolbar').style.display;
      if (state === 'block') {
        closeToolbar();
      }
      else {
        openToolbar();
      }
    };
    ctrl.ui = {
      toggleToolbar,
      openToolbar,
      closeToolbar
    };
    ctrl.data = {};
    ctrl.deepmerge = deepmerge_1.default;
    ctrl.title = 'WME AutoMate v ' + version;
    let startTimeout;
    const start = () => __awaiter(this, void 0, void 0, function* () {
      if (startTimeout) {
        clearTimeout(startTimeout);
      }
      const api = yield utils.getWazeAPI();
      init();
      ctrl.scanner = new Scanner(api);
      ctrl.filter = new Filter(api);
      ctrl.fixer = new Fixer(api);
      ctrl.rules = new Rules(api);
      requestAnimationFrame(() => {
        riot_1.default.mount('automate-toolbar', {
          controller: ctrl
        });
        riot_1.default.mount('automate-button', {
          controller: {ui: {toggleToolbar}}
        });
      });
    });
    startTimeout = setTimeout(start, 5000);
    window.addEventListener('load', start);

    /***/
  })
  /******/]);
// ==UserScript==
// @name WME AutoMate
// @author m03geek
// @description This script automates correction of various names
// @match     https://world.waze.com/editor/*
// @match     https://*.waze.com/editor/*
// @match     https://*.waze.com/*/editor/*
// @match     https://world.waze.com/map-editor/*
// @match     https://world.waze.com/beta_editor/*
// @match     https://www.waze.com/map-editor/*
// @require   file:///home/m03geek/dev/github/wme-automate/dist/wme-automate.user.js
// @grant     none
// @include   https://editor-beta.waze.com/*
// @include   https://*.waze.com/editor/editor/*
// @include   https://*.waze.com/*/editor/*
// @version   1.0.0
// ==/UserScript==
